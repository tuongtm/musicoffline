//
//  SettingViewController.swift
//  Music Player
//
//  Created by NhonGa on 10/08/2018.
//  Copyright © 2018 NhonGa. All rights reserved.
//

import UIKit
import StoreKit
import MessageUI
import GoogleMobileAds

enum RegisteredPurchase: String {
    case removeads
    case hutmau
    case proyearly
}

let appID : String = "1571430586"
let mailGroup : [String] = ["ducnguyentien1977@gmail.com"]

struct StoreReviewHelper {
    static func incrementAppOpenedCount() { // called from appdelegate didfinishLaunchingWithOptions:
        guard var appOpenCount = UserDefaults.standard.value(forKey: defaultsKeys.APP_OPENED_COUNT) as? Int else {
            UserDefaults.standard.set(1, forKey: defaultsKeys.APP_OPENED_COUNT)
            return
        }
        appOpenCount += 1
        UserDefaults.standard.set(appOpenCount, forKey: defaultsKeys.APP_OPENED_COUNT)
    }
    static func checkAndAskForReview() { // call this whenever appropriate
        // this will not be shown everytime. Apple has some internal logic on how to show this.
        guard let appOpenCount = UserDefaults.standard.value(forKey: defaultsKeys.APP_OPENED_COUNT) as? Int else {
            UserDefaults.standard.set(1, forKey: defaultsKeys.APP_OPENED_COUNT)
            return
        }
        
        switch appOpenCount {
        case 10,50:
            requestReview()
        case _ where appOpenCount%100 == 0 :
            requestReview()
        default:
            print("App run count is : \(appOpenCount)")
            break;
        }
        
    }
    static func requestReview() {
        if #available(iOS 10.3, *) {
            SKStoreReviewController.requestReview()
        } else {
            // Fallback on earlier versions
            // Try any other 3rd party or manual method here.
            rateApp(appId: appID)
        }
    }
    
    static func rateApp(appId: String) {
        //itms-apps:itunes.apple.com/us/app/apple-store/id\(YOURAPPID)?mt=8&action=write-review"
        openUrl("itms-apps://itunes.apple.com/app/id" + appId + "?mt=8&action=write-review")
    }
}
class SettingViewController: UIViewController,UITableViewDelegate, UITableViewDataSource, UINavigationBarDelegate,MFMailComposeViewControllerDelegate {
    static var shared : SettingViewController? = nil
    let appBundleId : String =  Bundle.main.bundleIdentifier!
    @IBOutlet weak var tableView: UITableView!
    struct Setting {
        var title : String
        var image : String
    }
    var Settings1 = [
            Setting(title: "Drop Box", image: "logout"),
            Setting(title: "Google Drive", image: "logout"),
            Setting(title: "One Drive", image: "logout"),
    ]
    var Settings2 = [
            Setting(title: "No Ads", image: "setting_iconunlock"),
            Setting(title: "Rate app", image: "setting_rateme"),
            Setting(title: "Feedback", image: "setting_feedback"),
            Setting(title: "Privacy Policy", image: "setting_policy"),
            Setting(title: "Terms Of Use", image: "setting_terrm"),
//            Setting(title: "Restore", image: "setting_restore"),
//            Setting(title: "More Apps", image: "setting_moreapp")
    ]
    
    fileprivate let headerSectionTableView = ["Accounts", "Support" ]
    
    let headerSectionIconImage = ["setting_iconaccountmanager", "setting_iconsupport" ]
    
    @IBOutlet weak var bannerView: GADBannerView!
    @IBOutlet weak var heightConstraintBannerView: NSLayoutConstraint!
    
    @IBAction func btnCloseAd(_ sender: UIButton) {

    }
    
    var i : Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        
        SettingViewController.shared = self
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        //        self.tableView.contentInset = UIEdgeInsets(top: -50, left: 0, bottom: -20, right: 0)
        
        bannerView = createAndLoadBanner(bannerView, self, heightConstraintBannerView)
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        self.tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if section == 0
        {
            return Settings1.count
        }
        else
        {
            return Settings2.count
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectRow = indexPath.row
        let selectSection = indexPath.section
        
        if selectSection == 0
        {
            TITLEWEBVIEWREQUEST = Settings1[selectRow].title
        }
        else if selectSection == 1
        {
            TITLEWEBVIEWREQUEST = Settings2[selectRow].title
            switch selectRow {
            case 0:
                URLWEBVIEWREQUEST = "no Ads"
                performSegue(withIdentifier: "segue", sender: self)
                break
            case 1:
                StoreReviewHelper.rateApp(appId: appID)
                break
            case 2:
                sendEmail()
                break
            case 3:
                URLWEBVIEWREQUEST = "policy"
                performSegue(withIdentifier: "segue", sender: self)
                break
//            case 3:
//                URLWEBVIEWREQUEST = "terms"
//                performSegue(withIdentifier: "segue", sender: self)
//                break
//            case 5:
//                //Restore
//                break
            default:
//                openUrl("itms-apps://apps.apple.com/us/developer/duc-nguyen-tien/id1571430586")
                URLWEBVIEWREQUEST = "terms"
                performSegue(withIdentifier: "segue", sender: self)
                break
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let row = indexPath.row
        let selectSection = indexPath.section
        
        if(selectSection == 0)
        {
            let cell = Bundle.main.loadNibNamed("SettingCell1", owner: self, options: nil)?.first as! SettingTableViewCell1
            
            let setting = Settings1[row]
            cell.title?.text = setting.title
            
            let headline = cloudMusicOptions[row]
            if headline.isLogin
            {
                tableView.rowHeight = 70
                cell.account?.text = headline.title1
                cell.btnLogOut.isHidden = false
                cell.btnLogOut.tag = (row)
                cell.btnLogOut.addTarget(self, action: #selector(tabLogOut), for: .touchUpInside)
                cell.account?.text = cloudMusicOptions[row].title2
            }
            else
            {
                tableView.rowHeight = 50
                cell.btnLogOut.isHidden = true
                cell.account?.text = ""
            }
            // Configure the cell...
            
            return cell
        }
        else
        {
            let cell = Bundle.main.loadNibNamed("SettingCell", owner: self, options: nil)?.first as! SettingTableViewCell
            if row == 0
            {
                let storage = UserDefaults.standard
                if storage.string(forKey: defaultsKeys.APP_REMOVE_ADS) != nil
                {
                    cell.isUserInteractionEnabled = false
                    tableView.rowHeight = 0
                }
                else
                {
                    tableView.rowHeight = 50
                }
            }
            else
            {
                tableView.rowHeight = 50
            }
            
            let setting = Settings2[row]
            
            cell.title?.text = setting.title
            cell.icon?.image = UIImage(named: setting.image)
            return cell
        }
        
        // Configure the cell...
    }
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return headerSectionTableView.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        guard let customHeaderView = CustomImportHeader.loadViewFromNib()
            else { return nil }
        customHeaderView.titleLabel.text = headerSectionTableView[section]
        customHeaderView.titleLabel.textColor = UIColor.init(red: 248, green: 173, blue: 55)
        customHeaderView.titleLabel.font = UIFont(name:"SegoeUI-Bold", size: 15)
        customHeaderView.iconIamge.image = UIImage(named: headerSectionIconImage[section])
        customHeaderView.backgroundColor = UIColor(hexString: "#F9E2D6")
        
        return customHeaderView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        return 30
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat
    {
        return 0
    }
    
    @objc func tabLogOut(_ sender : UIButton)
    {
        let row = sender.tag
        HomeViewController.shared!.LogOut(row)
        self.tableView.reloadData()
        
    }
    
    func sendEmail() {
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients(mailGroup)
            let nameApp = Bundle.main.infoDictionary![kCFBundleNameKey as String] as! String
            mail.setSubject("[FEEDBACK] \(nameApp)")
            mail.setMessageBody("<p>You're so awesome!</p>", isHTML: true)
            
            self.present(mail, animated: true, completion: nil)
        } else {
            showMailError()
            // show failure alert
        }
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }
    
    func showMailError() {
        let sendMailErrorAlert = UIAlertController(title: "Could not send email", message: "Your device could not send email", preferredStyle: .alert)
        let dismiss = UIAlertAction(title: "Ok", style: .default, handler: nil)
        sendMailErrorAlert.addAction(dismiss)
        self.present(sendMailErrorAlert, animated: true, completion: nil)
    }
    
    @available(iOS 10.0, *)
    @objc func tapped() {
        i += 1
        print("Running \(i)")
        
        switch i {
        case 1:
            let generator = UINotificationFeedbackGenerator()
            generator.notificationOccurred(.error)
            
        case 2:
            let generator = UINotificationFeedbackGenerator()
            generator.notificationOccurred(.success)
            
        case 3:
            let generator = UINotificationFeedbackGenerator()
            generator.notificationOccurred(.warning)
            
        case 4:
            let generator = UIImpactFeedbackGenerator(style: .light)
            generator.impactOccurred()
            
        case 5:
            let generator = UIImpactFeedbackGenerator(style: .medium)
            generator.impactOccurred()
            
        case 6:
            let generator = UIImpactFeedbackGenerator(style: .heavy)
            generator.impactOccurred()
            
        default:
            let generator = UISelectionFeedbackGenerator()
            generator.selectionChanged()
            i = 0
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
