//
//  WebViewController.swift
//  Music Player
//
//  Created by NhonGa on 01/10/2018.
//  Copyright © 2018 NhonGa. All rights reserved.
//

import UIKit

var URLWEBVIEWREQUEST : String = ""
var TITLEWEBVIEWREQUEST : String = ""

class WebViewController: UIViewController {

    @IBOutlet weak var txTitle: UILabel!
    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var terms: UITextView!
    @IBOutlet weak var policy: UITextView!
    @IBOutlet weak var noAdsView: UIView!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        self.title = TITLEWEBVIEWREQUEST
        if txTitle != nil
        {
            txTitle.text = TITLEWEBVIEWREQUEST
        }
        if URLWEBVIEWREQUEST == "policy"
        {
            policy.setContentOffset(.zero, animated: false)
            policy.isHidden = false
            terms.isHidden = true
            noAdsView.isHidden = true
        }
        else if URLWEBVIEWREQUEST == "terms"
        {
            terms.setContentOffset(.zero, animated: false)
            policy.isHidden = true
            terms.isHidden = false
            noAdsView.isHidden = true
        }
        else if URLWEBVIEWREQUEST == "no Ads"
        {
            policy.isHidden = true
            terms.isHidden = true
            noAdsView.isHidden = false
        }
        
        // Do any additional setup after loading the view.
    }

    @IBAction func backIAP(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func actionRestore(_ sender: Any) {
        IAPHelper.shared().restoreProduct { [weak self] (success, error) in
            guard let strongSelf = self else {
              return
            }
            if let err = error {
              
              GlobalParameter.shared().showErrorHUD(title:"Error", subTitle: err.localizedDescription, inView: strongSelf.view)
            }
            else if success {
              GlobalParameter.shared().showSuccessHUD(title:"Successful", subTitle: "Purchased Successful", inView: strongSelf.view)
            }
            else {
              GlobalParameter.shared().hideHUD()
            }
          }
    }
    
    
    @IBAction func actionBuy(_ sender: Any) {
        IAPHelper.shared().buyProduct(IAPPremiumLifetime) { [weak self] (success, error) in
                  guard let strongSelf = self else {
                    return
                  }
                  if let err = error {
                    
                    GlobalParameter.shared().showErrorHUD(title:"Error", subTitle: err.localizedDescription, inView: strongSelf.view)
                  }
                  else if success {
                    GlobalParameter.shared().showSuccessHUD(title:"Successful", subTitle: "Purchased Successful", inView: strongSelf.view)
                  }
                  else {
                    GlobalParameter.shared().hideHUD()
                  }
                }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func termShow(_ sender: Any) {
        let url = URL (string: "https://ducnt.theappshowcase.com/musicoffline-term-of-use/")
        UIApplication.shared.open(url!)
    }
    @IBAction func policyShow(_ sender: Any) {
        let url = URL (string: "https://ducnt.theappshowcase.com/musicoffline-privacy-policy/")
        UIApplication.shared.open(url!)
    }
}
