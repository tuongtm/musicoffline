//
//  IAPHelper.swift
//  SafeNote
//
//  Created by Tuan Trinh on 4/9/19.
//  Copyright © 2019 POLYMATH, LLC. All rights reserved.
//

import UIKit
import StoreKit
import SwiftyStoreKit

let IAPPremiumLifetime = "imessagefullstickers"
let IAPPremiumYearly = "DropnotesPremiumYearly2"
let IAPPremiumMonthly = "DropnotesPremiumMonthly"

let shared_secret_key = "3e19af22df7b4cbab0f1d4bf1d97277b"
let app_id: UInt = 1571430586

let SubscriptionChangeNotification = "SubscriptionChangeNotification"
let receiptKey = "ReceiptInfo"
let receiptKey_productId = "ProductId"
let receiptKey_experiedDate = "ExperiedDate"

class IAPHelper: NSObject {
    
    var products = [SKProduct]()
    var subscribeOptions = [SubscribeOption]()
    
    static let sharedInstance = IAPHelper()
    override init() {
        
    }
    
    class func shared() -> IAPHelper {
        return sharedInstance
    }
    
    var isPremium: Bool {
        
        guard let receptInfo = UserDefaults.standard.object(forKey: receiptKey) as? [String: Any], let _ = receptInfo[receiptKey_productId] as? String else {
            return false
        }
        
        if let experiedDate = receptInfo[receiptKey_experiedDate] as? Date, experiedDate.isGreaterThan(Date()) {
            return true
        }
        else {
            return false
        }
    }
    
    func requestSubscribeOptions(completion: ((_ products: [SubscribeOption]?) -> Void)?) -> Void {
        if self.subscribeOptions.count > 0 {
            if let completion = completion {
                completion(self.subscribeOptions)
            }
        }
        else {
            SwiftyStoreKit.retrieveProductsInfo([IAPPremiumLifetime]) { [weak self] result in
                guard let strongSelf = self, result.retrievedProducts.count > 0 else {
                    return
                }
                
                var subscribeOptions = [SubscribeOption]()
                for product in result.retrievedProducts {
                    let numberFormatter = NumberFormatter.init()
                    numberFormatter.formatterBehavior = NumberFormatter.Behavior.behavior10_4
                    numberFormatter.numberStyle = NumberFormatter.Style.currency
                    numberFormatter.locale = product.priceLocale
                    
                    var name: String? = nil
                    var title: String? = nil
                    var price: String? = nil
                    var pricePeriod: String? = nil
                    var introductoryPrice: String? = nil
                    
                    if product.productIdentifier == IAPPremiumLifetime {
                        name = "Full Sticker Pack"
                    }
                    
                    if #available(iOS 11.2, *) {
                        if product.productIdentifier == IAPPremiumLifetime {
                            title = "Full Sticker Pack"
                            price = numberFormatter.string(from: product.price)
                        }
//                        else if let subscriptionPeriod = product.subscriptionPeriod {
//                            let numberOfUnits = subscriptionPeriod.numberOfUnits
//                            let princePerUnits = product.price.dividing(by: NSDecimalNumber.init(value: subscriptionPeriod.numberOfUnits), withBehavior: nil)
//                            price = numberFormatter.string(from: princePerUnits) ?? ""
//
//                            if numberOfUnits > 0 {
//                                if subscriptionPeriod.unit == SKProduct.PeriodUnit.year {
//                                    title = String.init(format: "%d %@", numberOfUnits, (numberOfUnits > 1) ? "years".localized:"year".localized)
//                                    pricePeriod = "year".localized
//                                }
//                                else if subscriptionPeriod.unit == SKProduct.PeriodUnit.month {
//                                    title = String.init(format: "%d %@", numberOfUnits, (numberOfUnits > 1) ? "months".localized:"month".localized)
//                                    pricePeriod = "month".localized
//                                }
//                                else if subscriptionPeriod.unit == SKProduct.PeriodUnit.week {
//                                    title = String.init(format: "%d %@", numberOfUnits, (numberOfUnits > 1) ? "weeks".localized:"week".localized)
//                                    pricePeriod = "week".localized
//                                }
//                                else if subscriptionPeriod.unit == SKProduct.PeriodUnit.day {
//                                    title = String.init(format: "%d %@", numberOfUnits, (numberOfUnits > 1) ? "days".localized:"day".localized)
//                                    pricePeriod = "day".localized
//                                }
//                            }
//                        }
                        
//                        if let introductory = product.introductoryPrice {
//                            let numberOfUnits = introductory.subscriptionPeriod.numberOfUnits
//                            if numberOfUnits > 0 {
//                                if introductory.subscriptionPeriod.unit == SKProduct.PeriodUnit.year {
//                                    introductoryPrice = String.init(format: "%d %@", numberOfUnits, (numberOfUnits > 1) ? "years".localized:"year".localized)
//                                }
//                                else if introductory.subscriptionPeriod.unit == SKProduct.PeriodUnit.month {
//                                    introductoryPrice = String.init(format: "%d %@", numberOfUnits, (numberOfUnits > 1) ? "months".localized:"month".localized)
//                                }
//                                else if introductory.subscriptionPeriod.unit == SKProduct.PeriodUnit.week {
//                                    introductoryPrice = String.init(format: "%d %@", numberOfUnits, (numberOfUnits > 1) ? "weeks".localized:"week".localized)
//                                }
//                                else if introductory.subscriptionPeriod.unit == SKProduct.PeriodUnit.day {
//                                    introductoryPrice = String.init(format: "%d %@", numberOfUnits, (numberOfUnits > 1) ? "days".localized:"day".localized)
//                                }
//                            }
//                        }
                    }
                    else {
                        if product.productIdentifier == IAPPremiumLifetime {
                            title = "Full Sticker Pack"
                            price = numberFormatter.string(from: product.price)
                        }
//                        else if product.productIdentifier == DropnotesPremiumYearly {
//                            title = String.init(format: "1 %@", "year".localized)
//                            price = numberFormatter.string(from: product.price)
//                            pricePeriod = "year".localized
//                        }
//                        else if product.productIdentifier == DropnotesPremiumMonthly {
//                            title = String.init(format: "1 %@", "month".localized)
//                            price = numberFormatter.string(from: product.price)
//                            pricePeriod = "month".localized
//                        }
                    }
                    
                    let option = SubscribeOption()
                    option.identifier = product.productIdentifier
                    option.name = name
                    option.title = title
                    option.price = price
                    option.pricePeriod = pricePeriod
                    option.introductoryPrice = introductoryPrice
                    subscribeOptions.append(option)
                }
                
                strongSelf.products = Array(result.retrievedProducts)
                strongSelf.subscribeOptions = subscribeOptions
                
                if let completion = completion {
                    completion(subscribeOptions)
                }
            }
        }
    }
    
    func getSubscribeOption(productId: String) -> SubscribeOption? {
        for subscribeOption in self.subscribeOptions {
            if subscribeOption.identifier == productId {
                return subscribeOption
            }
        }
        return nil
    }
    
    func buyProduct(_ productId: String, completion: ((_ success: Bool, _ error: Error?) -> Void)?) -> Void {
        SwiftyStoreKit.purchaseProduct(productId) { [weak self] (result) in
            if case .success(let purchase) = result {
                if purchase.needsFinishTransaction {
                    SwiftyStoreKit.finishTransaction(purchase.transaction)
                }
                
                if let strongSelf = self {
                    let newDate = Calendar.current.date(byAdding: .year, value: 10, to: Date())
                    strongSelf.saveReceiptInfo(purchase.productId, newDate)
                    if let completion = completion {
                        completion(true, nil)
                    }
//                    strongSelf.checkSubscription(productId, completion: completion)
                }
            }
            else if case .error(let error) = result {
                if error.code == SKError.paymentCancelled {
                    if let completion = completion {
                        completion(false, nil)
                    }
                }
                else {
                    if let completion = completion {
                        completion(false, error)
                    }
                }
            }
            else {
                if let completion = completion {
                    completion(false, nil)
                }
            }
        }
    }
    
    func restoreProduct(completion: ((_ success: Bool, _ error: Error?) -> Void)?) -> Void {
        SwiftyStoreKit.restorePurchases { [weak self] (result) in
            if let purchase = result.restoredPurchases.last {
                if let strongSelf = self {
                    strongSelf.checkSubscription(purchase.productId, completion: completion)
                }
            }
            else {
                let error = result.restoreFailedPurchases.last?.0
                if let error = error, error.code == SKError.paymentCancelled {
                    if let completion = completion {
                        completion(false, nil)
                    }
                }
                else {
                    if let completion = completion {
                        completion(false, error)
                    }
                }
            }
        }
    }
    
    func checkSubscription() -> Void {
        guard let receptInfo = UserDefaults.standard.object(forKey: receiptKey) as? [String: Any], let productId = receptInfo[receiptKey_productId] as? String else {
            return
        }
        
        if let experiedDate = receptInfo[receiptKey_experiedDate] as? Date, experiedDate.isGreaterThan(Date()) {
            self.checkSubscription(productId, completion: nil)
        }
        else {
            //self.saveReceiptInfo(nil, nil)
        }
    }
    
    private func checkSubscription(_ productId: String, completion: ((_ success: Bool, _ error: Error?) -> Void)?) -> Void {
        self.checkSubscription(productId, true) { [weak self] (success, errror) in
            if !success {
                if let strongSelf = self {
                    strongSelf.checkSubscription(productId, false, completion: completion)
                }
            }
            else {
                if let completion = completion {
                    completion(success, errror)
                }
            }
        }
    }
    
    private func checkSubscription(_ productId: String, _ onProduct: Bool, completion: ((_ success: Bool, _ error: Error?) -> Void)?) -> Void {
        let appleValidator = AppleReceiptValidator(service: (onProduct ? .production:.sandbox), sharedSecret: shared_secret_key)
        SwiftyStoreKit.verifyReceipt(using: appleValidator) { [weak self] (result) in
            if case .success(let receipt) = result {
                var expiriedDate: Date? = nil
                
                let purchaseResult = SwiftyStoreKit.verifySubscription(ofType: .autoRenewable, productId: productId, inReceipt: receipt)
                switch purchaseResult {
                case .purchased(let expiryDate, _):
                    expiriedDate = expiryDate
                    Utils.debugLog(value: "Product is valid until " + expiryDate.string(withFormat: "HH:mm, DD/mm/yyy"))
                case .expired(let expiryDate, _):
                    Utils.debugLog(value: "Product is expired since " + expiryDate.string(withFormat: "HH:mm, DD/mm/yyy"))
                case .notPurchased:
                    Utils.debugLog(value: "This product has never been purchased")
                }
                
                if let strongSelf = self {
                    strongSelf.saveReceiptInfo(productId, expiriedDate)
                }
                
                if let completion = completion {
                    completion(expiriedDate != nil, nil)
                }
            }
            else if case .error(let error) = result {
                if let completion = completion {
                    completion(false, error)
                }
            }
        }
    }
    
    private func saveReceiptInfo(_ productId: String?, _ expiriedDate: Date?) -> Void {
        let userDefault = UserDefaults.standard
        if let productId = productId, let expiriedDate = expiriedDate {
            userDefault.set([receiptKey_productId:productId, receiptKey_experiedDate:expiriedDate], forKey: receiptKey)
        }
        else {
            userDefault.removeObject(forKey: receiptKey)
        }
        userDefault.synchronize()
    }
    
}

class SubscribeOption: NSObject {
    
    var identifier: String!
    var name: String?
    var title: String?
    var price: String?
    var pricePeriod: String?
    var introductoryPrice: String?
    
}
