//
//  AudioSessionHelper.h
//  Tubidy
//
//  Created by NhonGa on 17/10/2018.
//  Copyright © 2018 ThreeWolves. All rights reserved.
//

#ifndef AudioSessionHelper_h
#define AudioSessionHelper_h
#import <AVFoundation/AVFoundation.h>

@interface AudioSessionHelper: NSObject
+ (BOOL) setAudioSessionWithError:(NSError **) error;
@end

#endif /* AudioSessionHelper_h */
