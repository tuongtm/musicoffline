//
//  AudioSessionHelper.m
//  Tubidy
//
//  Created by NhonGa on 17/10/2018.
//  Copyright © 2018 ThreeWolves. All rights reserved.
//

#import "AudioSessionHelper.h"
#import <Foundation/Foundation.h>

@implementation AudioSessionHelper: NSObject

+ (BOOL) setAudioSessionWithError:(NSError **) error {
    BOOL success = [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:error];
    if (!success && error) {
        return false;
    } else {
        return true;
    }
}
@end
