//
//  WelcomeAppViewController.swift
//  Offline Music
//
//  Created by NhonGa on 13/05/2019.
//  Copyright © 2019 Free App & Game. All rights reserved.
//

import UIKit

class WelcomeAppViewController: UIViewController, UITextViewDelegate  {
    @IBOutlet weak var btnContinue: UIButton!
    let linkUrl = "https://sites.google.com/site/cloudmusicpolicy/"
    
    @IBOutlet weak var txViewPolicy: UITextView!
    override func viewDidLoad()
    {
        super.viewDidLoad()
        btnContinue.layer.cornerRadius = 4
        self.txViewPolicy.delegate = self
        // Do any additional setup after loading the view.
        txViewPolicy.hyperLink(originalText: "PRIVACY POLICY NOTICE\n\nYour privacy is our concern. We intend to provide ensure user privacy. Please learn more about how we process the data that can be obtained about you or your device. By continuing you confirm that you accept our Privacy Policy.", hyperLink: "Privacy Policy", urlString: linkUrl)
    }
    
    @available(iOS, deprecated: 10.0)
    func textView(_ textView: UITextView, shouldInteractWith url: URL, in characterRange: NSRange) -> Bool {
        if (url.absoluteString == linkUrl)
        {
            performSegue(withIdentifier: "segue", sender: self)
        }
        return false
    }
    
    //For iOS 10
    @available(iOS 10.0, *)
    func textView(_ textView: UITextView, shouldInteractWith url: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        if (url.absoluteString == linkUrl)
        {
            performSegue(withIdentifier: "segue", sender: self)
        }
        return false
        
    }
    
    func textView(_ textView: UITextView, shouldInteractWith textAttachment: NSTextAttachment, in characterRange: NSRange) -> Bool {
        print("xxxx: aaaaa")
        return true
    }
    
    @available(iOS 10.0, *)
    func textView(_ textView: UITextView, shouldInteractWith textAttachment: NSTextAttachment, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        print("xxxx: bbbb")
        return true
    }
    
//    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange) -> Bool
//    {
//
//    }

    @IBAction func dismissWelcomeScreen(_ sender: Any)
    {
        
    }
    
}
