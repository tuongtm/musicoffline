//
//  FeatureTableViewCell.swift
//  Music Player
//
//  Created by NhonGa on 30/07/2018.
//  Copyright © 2018 NhonGa. All rights reserved.
//

import UIKit

class FeatureTableViewCell: UITableViewCell {

    @IBOutlet weak var rowView: UIView!
    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var txStatus: UILabel!
    @IBOutlet weak var txTitle: UILabel!
    @IBOutlet weak var btnLogOut: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        rowView.layer.cornerRadius = 18;
        rowView.layer.masksToBounds = true;
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
