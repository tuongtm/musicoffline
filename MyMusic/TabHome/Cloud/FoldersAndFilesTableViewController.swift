//
//  FoldersAndFilesTableViewController.swift
//  Music Player
//
//  Created by NhonGa on 03/08/2018.
//  Copyright © 2018 NhonGa. All rights reserved.
//

import UIKit
import GoogleAPIClientForREST
import SwiftyDropbox
import CoreGraphics
import OneDriveSDK
import GoogleMobileAds

class FoldersAndFilesTableViewController: UIViewController,UITableViewDelegate, UITableViewDataSource, UINavigationBarDelegate {
    static var shared : FoldersAndFilesTableViewController? = nil
    var fileListAll : [Any]!
    var optionMenu : UIAlertController? = nil
    var indexFile : IndexPath = []
    var serviceTicket : GTLRServiceTicket? = nil
    var cellTableView : UITableViewCell!
    var currentItem : Any!
    
    @IBOutlet weak var bgViewIndicator: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var viewIndicator: UIView!
    
    @IBOutlet weak var bannerView: GADBannerView!
    @IBOutlet weak var heightConstraintBannerView: NSLayoutConstraint!
    
    @IBAction func btnCloseAd(_ sender: UIButton) {

    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        FoldersAndFilesTableViewController.shared = self
//        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Cancel", style: .done, target: self, action: #selector(dismissSetting))
//        navigationItem.rightBarButtonItem?.tintColor = UIColor.white
        
        navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        
        bgViewIndicator.layer.cornerRadius = 18;
        bgViewIndicator.layer.masksToBounds = true;
        optionMenu = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        if let optionMenuSheet = optionMenu{
            let downloadAction = UIAlertAction(title: "Download", style: .default, handler: {
                (alert: UIAlertAction!) -> Void in
                self.downloadHandler()
            })
            let cancelAction = UIAlertAction(title: "Cancel", style: .destructive, handler: {
                (alert: UIAlertAction!) -> Void in
            })
            optionMenuSheet.addAction(downloadAction)
            optionMenuSheet.addAction(cancelAction)
        }
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.separatorColor = UIColor.clear

        switch myIndex {
        case 1:
            if isFirstToVC
            {
                self.title = "Google Drive"
            }
            
            listFileFromGoogleDrive()
            break
        case 2:
            if isFirstToVC
            {
                self.title = "Drop Box"
            }
            listFileFromDropBox()
            break
        case 3:
            if isFirstToVC
            {
                self.title = "One Drive"
            }
            listFileFromOneDrive()
            break
        default:
            
            break
        }
        isFirstToVC = false
        bannerView = createAndLoadBanner(bannerView, self, heightConstraintBannerView)
    }
    
    @objc func dismissSetting()
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        print("aaa didappear haha")
        self.tableView.reloadData()
    }
    
    func collectionView(with item: Any?) -> FoldersAndFilesTableViewController? {
        let newController = storyboard?.instantiateViewController(withIdentifier: "FoldersAndFilesTableViewController") as? FoldersAndFilesTableViewController
        switch myIndex {
            case 1:
                newController?.title = (item as! GTLRDrive_File).name
                break
            case 2:
                newController?.title = (item as! Files.Metadata).name
                break
            case 3:
                newController?.title = (item as! ODItem).name
                break
            default:
                
                break
        }
        
        newController?.currentItem = item
        return newController
    }
    
    
    func listFileFromOneDrive() {
        let itemId = currentItem != nil ? (currentItem as! ODItem).id : "root"
        let childrenRequest: ODChildrenCollectionRequest? = clientsOneDrive?.drive().items(itemId).children().request()
        childrenRequest?.getWithCompletion({ response, nextRequest, error in
            if error == nil {
                if (response?.value) != nil  {
                    var arrNewEntry : [ODItem] = [ODItem]()
                    for entry in response?.value as! [ODItem]{
                        if(entry.folder != nil){
                            arrNewEntry.append(entry)
                        }else{
                            if(entry.file.mimeType == "audio/mp3" ||
                                entry.file.mimeType == "audio/flac" ||
                                entry.file.mimeType == "audio/m4a" ||
                                entry.file.mimeType == "audio/wav" ||
                                entry.file.mimeType == "video/mp4"){
                                arrNewEntry.append(entry)
                            }
                        }
                    }
                    self.fileListAll = arrNewEntry
                    DispatchQueue.main.sync {
                        self.tableView?.reloadData()
                        self.viewIndicator.isHidden = true
                    }
                }
                if nextRequest != nil {
                    
                }
            } else {
                
            }
        })
    }
    
    func listFileFromDropBox(){
        let path : String = currentItem != nil ? (currentItem as! Files.Metadata).pathDisplay! : ""
        viewIndicator.isHidden = false
        if let client = DropboxClientsManager.authorizedClient {
            client.files.listFolder(path: path).response { response, error in
                if let result = response {
                    var arrNewEntry : [Files.Metadata] = [Files.Metadata]()
                    for entry in result.entries{
                        switch entry {
                            case let file as Files.FileMetadata:
                                if file.name.hasSuffix(".flac") || file.name.hasSuffix(".wav") ||
                                    file.name.hasSuffix(".m4a") || file.name.hasSuffix(".mp3")
                                    || file.name.hasSuffix(".mp4"){
                                    arrNewEntry.append(entry)
                                }
                                break
                            case _ as Files.FolderMetadata:
                                arrNewEntry.append(entry)
                                break
                            default:
                                break
                        }
                    }
                    self.fileListAll = arrNewEntry
                    self.tableView?.reloadData()
                    self.viewIndicator.isHidden = true
                }
            }
        }
    }
    
    @objc func reloadData(sender: UIBarButtonItem) {
       tableView.reloadData()
    }
    
    func listFileFromGoogleDrive(){
        let excuteQuery = currentItem != nil ? (currentItem as! GTLRDrive_File).identifier! : "root"
        let queryString : String = "(mimeType = 'application/vnd.google-apps.folder' or mimeType = 'audio/mp3' or mimeType = 'audio/flac' or mimeType = 'audio/m4a' or mimeType = 'audio/wav' or mimeType = 'video/mp4') and '\(excuteQuery)' in parents and trashed=false"
        viewIndicator.isHidden = false
        let query = GTLRDriveQuery_FilesList.query()
        query.pageSize = 1000
        query.q = queryString
        query.fields = "files(id,name,mimeType,modifiedTime,createdTime,fileExtension,size,parents,kind),nextPageToken"
        service.executeQuery(query, completionHandler: { (ticket, files, error) -> Void in
            if let filesList : GTLRDrive_FileList = files as? GTLRDrive_FileList {
                self.fileListAll = filesList.files
                self.tableView?.reloadData()
                self.viewIndicator.isHidden = true
            }
            
        })
    }

    // MARK: - Table view data source
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let len = (self.fileListAll != nil) ? self.fileListAll.count : 0
        return len
    }
    
    fileprivate func clickCommon(_ name: String){
        let checkAudioExist = checkExistFileAudio(name)
        if checkAudioExist{
            
        }else{
//            optionMenu?.popoverPresentationController?.sourceView = self.view
//            optionMenu?.popoverPresentationController?.sourceRect = CGRect(x:self.view.bounds.width / 2.0, y:self.view.bounds.height / 2.0, width: 1.0,height: 1.0)
            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.pad){
                if let currentPopoverpresentioncontroller = self.optionMenu!.popoverPresentationController{
                    currentPopoverpresentioncontroller.sourceView = self.view
                    currentPopoverpresentioncontroller.sourceRect = self.view.bounds
                    currentPopoverpresentioncontroller.permittedArrowDirections = UIPopoverArrowDirection(rawValue: 0)
                    self.present(self.optionMenu!, animated: true, completion: nil)
                }
            }else{
                self.present(self.optionMenu!, animated: true, completion: nil)
            }
        }
    }
    
    fileprivate func clickGgDr(_ indexPath: IndexPath) {
        let selectedDriveFile = self.fileListAll[indexPath.row] as! GTLRDrive_File
        
        if selectedDriveFile.mimeType == "application/vnd.google-apps.folder"{
            self.navigationController?.pushViewController(self.collectionView(with: selectedDriveFile)!, animated: true)
        }else{
            self.clickCommon(selectedDriveFile.name!)
        }
    }
    
    fileprivate func clickDropBox(_ indexPath: IndexPath) {
        let selectedFileDropBox = self.fileListAll[indexFile.row] as! Files.Metadata
        switch selectedFileDropBox {
        case _ as Files.FileMetadata:
            self.clickCommon(selectedFileDropBox.name)
            break
        case _ as Files.FolderMetadata:
            self.navigationController?.pushViewController(self.collectionView(with: selectedFileDropBox)!, animated: true)
            break
        default:
            break
        }
    }
    
    fileprivate func clickOneDrive(_ indexPath: IndexPath) {
        let selectedFileOneDrive = self.fileListAll[indexPath.row] as! ODItem
        if((selectedFileOneDrive.folder) != nil){
            self.navigationController?.pushViewController(self.collectionView(with: selectedFileOneDrive)!, animated: true)
        }else{
            self.clickCommon(selectedFileOneDrive.name!)
        }
    }
    
     func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        indexFile = indexPath
        switch myIndex {
        case 1:
            clickGgDr(indexPath)
            break
        case 2:
           clickDropBox(indexPath)
            break
        case 3:
            clickOneDrive(indexPath)
            break
        default:
            break
        }
    }
    
    func sizeFileConvertToString(sizeFile: Int64) -> String {
        let bcf = ByteCountFormatter()
        bcf.allowedUnits = [.useMB]
        bcf.countStyle = .file
        let sizeString = bcf.string(fromByteCount: sizeFile)
        return sizeString
    }
    
    func dateFileConvertToString(date: Date) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let dateString = formatter.string(from: date)
        return dateString
    }
    
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellFull", for: indexPath) as! FoldersAndFilesTableViewCell
        var isHaveSize = false
        var isHaveDate = false
        cell.progessDownload.isHidden = true
        cell.txDownload.isHidden = true
        cell.statusView.isHidden = true
        cell.indicatorDownload.isHidden = true
        switch myIndex
        {
        case 1:
            let selectedFile = self.fileListAll[indexPath.row] as! GTLRDrive_File
            cell.nameItem?.text = selectedFile.name

            if selectedFile.mimeType == "application/vnd.google-apps.folder"
            {
                cell.iconFolder.isHidden = false
                cell.iconMusic.isHidden = true
                cell.iconVideo.isHidden = true
            }
            else
            {
                if selectedFile.mimeType == "video/mp4"
                {
                    cell.iconFolder.isHidden = true
                    cell.iconMusic.isHidden = true
                    cell.iconVideo.isHidden = false
                }
                else
                {
                    cell.iconFolder.isHidden = true
                    cell.iconMusic.isHidden = false
                    cell.iconVideo.isHidden = true
                }
                if let sizeFile = selectedFile.size {
                    isHaveSize = true
                    let sizeString = sizeFileConvertToString(sizeFile: Int64(truncating: sizeFile))
                    cell.sizeItem.text = sizeString
                }else{
                    isHaveSize = false
                }
                
                if let dateFile = selectedFile.createdTime {
                    let dateString = dateFileConvertToString(date: dateFile.date)
                    cell.dateItem.text = dateString
                    isHaveDate = true
                }else{
                    isHaveDate = false
                }
                cell.statusView.isHidden = false
                let checkExistFile = checkExistFileAudio (selectedFile.name!)
                if checkExistFile{
                    cell.statusView?.image = UIImage(named: "icon_tick")
                }else{
                    cell.statusView?.image = UIImage(named: "icon_download")
                }
            }
            break
        case 2:
            let selectedFileDropBox = self.fileListAll[indexPath.row] as! Files.Metadata
            cell.nameItem?.text = selectedFileDropBox.name
            switch selectedFileDropBox {
                case let fileDropBox as Files.FileMetadata:
                    isHaveSize = true
                    let sizeString = sizeFileConvertToString(sizeFile: Int64(fileDropBox.size))
                    cell.sizeItem.text = sizeString
                    
                    let dateString = dateFileConvertToString(date: fileDropBox.serverModified)
                    cell.dateItem.text = dateString
                    isHaveDate = true
                    if selectedFileDropBox.name.hasSuffix(".mp4")
                    {
                        cell.iconFolder.isHidden = true
                        cell.iconMusic.isHidden = true
                        cell.iconVideo.isHidden = false
                    }
                    else
                    {
                        cell.iconFolder.isHidden = true
                        cell.iconMusic.isHidden = false
                        cell.iconVideo.isHidden = true
                    }
                    cell.statusView.isHidden = false
                    let checkExistFile = checkExistFileAudio (selectedFileDropBox.name)
                    if checkExistFile
                    {
                        cell.statusView?.image = UIImage(named: "icon_tick")
                    }
                    else
                    {
                        cell.statusView?.image = UIImage(named: "icon_download")
                    }
                    
                    break
                case _ as Files.FolderMetadata:
                    cell.iconFolder.isHidden = false
                    cell.iconMusic.isHidden = true
                    cell.iconVideo.isHidden = true
                    break
                default:
                    break
            }
            
            break
        case 3:
            let selectedFileOneDrive = self.fileListAll[indexPath.row] as! ODItem
            cell.nameItem?.text = selectedFileOneDrive.name
            if((selectedFileOneDrive.folder) != nil)
            {
                cell.iconFolder.isHidden = false
                cell.iconMusic.isHidden = true
                cell.iconVideo.isHidden = true
            }
            else
            {
                if(selectedFileOneDrive.file.mimeType == "audio/mp3" ||
                    selectedFileOneDrive.file.mimeType == "audio/flac" ||
                    selectedFileOneDrive.file.mimeType == "audio/m4a" ||
                    selectedFileOneDrive.file.mimeType == "audio/wav" ||
                    selectedFileOneDrive.file.mimeType == "video/mp4"){
                    cell.statusView.isHidden = false
                    let checkExistFile = checkExistFileAudio (selectedFileOneDrive.name)
                    if checkExistFile{
                        cell.statusView?.image = UIImage(named: "icon_tick")
                    }else{
                        cell.statusView?.image = UIImage(named: "icon_download")
                    }
                    if selectedFileOneDrive.file.mimeType == "video/mp4"
                    {
                        cell.iconFolder.isHidden = true
                        cell.iconMusic.isHidden = true
                        cell.iconVideo.isHidden = false
                    }
                    else
                    {
                        cell.iconFolder.isHidden = true
                        cell.iconMusic.isHidden = false
                        cell.iconVideo.isHidden = true
                    }
                    isHaveSize = true
                    let sizeString = sizeFileConvertToString(sizeFile: Int64(selectedFileOneDrive.size))
                    cell.sizeItem.text = sizeString
                    
                    let dateString = dateFileConvertToString(date: selectedFileOneDrive.lastModifiedDateTime)
                    cell.dateItem.text = dateString
                    isHaveDate = true
                }
            }
            break
        default:
            break
        }
        
        if isHaveDate || isHaveSize{
            cell.row2.isHidden = false
        }else{
            cell.row2.isHidden = true
            
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    fileprivate func checkExistFileAudio(_ nameFile: String) -> Bool{
        let filePath = getSongPath(songName: nameFile)
        let fileManager = FileManager.default
        if fileManager.fileExists(atPath: filePath.path){
            return true
        }else{
            return false
        }
    }
    
    fileprivate func saveFileAudio(_ nameFile : String, _ dataObj: GTLRDataObject?) {
        do{
            let pathComponent = getSongPath(songName: nameFile)
            if let dataWrite = dataObj
            {
                try dataWrite.data.write(to: pathComponent)
            }
            let attrs = try FileManager.default.attributesOfItem(atPath: pathComponent.path) as NSDictionary
            
            let fileStep = try loadDataJson(NAME_LIST_JSON) as Data
            var vendorArray = try JSONDecoder().decode([DataListJson].self, from: fileStep) as [DataListJson]
            let dataNew : DataListJson = DataListJson.init(pathComponent.path, nameFile, "\(attrs.fileSize())", attrs.fileCreationDate()!.description)
            vendorArray.append(dataNew)
            try savePropertyList(NAME_LIST_JSON, vendorArray)
        } catch{
            print(error)
        }
    }

    func downloadHandler(){
        let cell = self.tableView.cellForRow(at: self.indexFile) as! FoldersAndFilesTableViewCell
        var fractionalProgress : Float = 0.0
        cell.progessDownload.isHidden = false
        cell.txDownload.isHidden = false
        cell.statusView.isHidden = true
        switch(myIndex){
        case 1:
            let selectedDriveFile = self.fileListAll[indexFile.row] as! GTLRDrive_File
            let fileName = selectedDriveFile.name!
            let query = GTLRDriveQuery_FilesGet.queryForMedia(withFileId: selectedDriveFile.identifier!)
            let fileSize = selectedDriveFile.size
            
            cell.txDownload.text = "0 MB / " +
                sizeFileConvertToString(sizeFile: fileSize as! Int64)
            
            serviceTicket =  service.executeQuery(query) { (ticket, file, error) in
                if let dataObj : GTLRDataObject = file as? GTLRDataObject {
                    cell.progessDownload.isHidden = true
                    cell.txDownload.isHidden = true
                    cell.statusView.isHidden = false
                    cell.statusView?.image = UIImage(named: "icon_tick")
                    self.saveFileAudio(fileName, dataObj)
                }
            }
            
            serviceTicket?.objectFetcher?.receivedProgressBlock = {(sizeByte, totalSizeByteExpect) in
                DispatchQueue.main.async {
                    fractionalProgress = Float(totalSizeByteExpect) / Float(truncating: fileSize!)
                    cell.progessDownload.setProgress(fractionalProgress, animated: true)
                    cell.txDownload.text = self.sizeFileConvertToString(sizeFile: totalSizeByteExpect) + " / " +
                        self.sizeFileConvertToString(sizeFile: fileSize as! Int64)
                }
            }
            let storage = UserDefaults.standard
            if storage.string(forKey: defaultsKeys.APP_REVIEW_GGDRIVE) == nil
            {
                StoreReviewHelper.requestReview()
                storage.set(1, forKey: defaultsKeys.APP_REVIEW_GGDRIVE)
            }
            break
        case 2:
            let selectedFileDropBox = self.fileListAll[indexFile.row] as! Files.Metadata
            let fileName = selectedFileDropBox.name
            let destination: (URL, HTTPURLResponse) -> URL = { temporaryURL, response in
                return getSongPath(songName: fileName)
            }
            switch selectedFileDropBox {
                case let fileDropBox as Files.FileMetadata:
                    cell.txDownload.text = "0 MB / " +
                        sizeFileConvertToString(sizeFile: Int64(fileDropBox.size))
                    break
                default:
                    break
            }
            files.download(path: selectedFileDropBox.pathDisplay!, overwrite: true, destination: destination)
                .progress{bytesReader in
                    DispatchQueue.main.async {
                        fractionalProgress = Float(bytesReader.fractionCompleted)
                        cell.progessDownload.setProgress(fractionalProgress, animated: true)
                        cell.txDownload.text = self.sizeFileConvertToString(sizeFile: bytesReader.completedUnitCount) + " / " +
                            self.sizeFileConvertToString(sizeFile: bytesReader.totalUnitCount)
                    }
                }
                .response {response, error in
                    cell.progessDownload.isHidden = true
                    cell.txDownload.isHidden = true
                    cell.statusView.isHidden = false
                    cell.statusView?.image = UIImage(named: "icon_tick")
                    self.saveFileAudio(fileName, nil)
                }
            
            let storage = UserDefaults.standard
            if storage.string(forKey: defaultsKeys.APP_REVIEW_DROPBOX) == nil
            {
                StoreReviewHelper.requestReview()
                storage.set(1, forKey: defaultsKeys.APP_REVIEW_DROPBOX)
            }
            break
        case 3:
            cell.progessDownload.isHidden = true
            cell.txDownload.isHidden = true
            cell.indicatorDownload.isHidden = false
            cell.indicatorDownload.startAnimating()
            let selectedFileOneDrive = self.fileListAll[indexFile.row] as! ODItem
            let fileName = selectedFileOneDrive.name
            //            let fileSize = selectedFileOneDrive.size
            var _: ODURLSessionDownloadTask? = clientsOneDrive?.drive().items(selectedFileOneDrive.id).contentRequest().download(completion: { filePath, response, error in
                let newPath = getSongPath(songName: fileName!)
                let fileManager = FileManager.default
                do{
                    try fileManager.moveItem(at: filePath!, to: newPath)
                }catch{
                    
                }
                self.saveFileAudio(fileName!, nil)
                DispatchQueue.main.sync {
                    cell.indicatorDownload.isHidden = true
                    cell.statusView.isHidden = false
                    cell.statusView?.image = UIImage(named: "icon_tick")
                }
            })
            let storage = UserDefaults.standard
            if storage.string(forKey: defaultsKeys.APP_REVIEW_ONEDRIVE) == nil
            {
                StoreReviewHelper.requestReview()
                storage.set(1, forKey: defaultsKeys.APP_REVIEW_ONEDRIVE)
            }
            break
        default :
            break
        }
    }
    


}
