//
//  FoldersAndFilesTableViewCell.swift
//  Music Player
//
//  Created by NhonGa on 03/08/2018.
//  Copyright © 2018 NhonGa. All rights reserved.
//

import UIKit

class FoldersAndFilesTableViewCell: UITableViewCell {

    @IBOutlet weak var iconMusic: UIImageView!
    @IBOutlet weak var iconFolder: UIImageView!
    @IBOutlet weak var iconVideo: UIImageView!
    @IBOutlet weak var nameItem: UILabel!
    @IBOutlet weak var txDownload: UILabel!
    @IBOutlet weak var progessDownload: UIProgressView!
    @IBOutlet weak var statusView: UIImageView!
    @IBOutlet weak var row2: UIStackView!
    @IBOutlet weak var dateItem: UILabel!
    @IBOutlet weak var sizeItem: UILabel!
    @IBOutlet weak var indicatorDownload: UIActivityIndicatorView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        progessDownload.transform = progessDownload.transform.scaledBy(x: 1, y: 5)
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
