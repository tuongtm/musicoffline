//
//  ViewController.swift
//  Music Player
//
//  Created by NhonGa on 26/07/2018.
//  Copyright © 2018 ThreeWolves. All rights reserved.
//

import UIKit
import GoogleMobileAds
import AVFoundation
import AVKit

class TutorialViewController: UIViewController, UIScrollViewDelegate, GADInterstitialDelegate {
    var pageViewController: UIPageViewController!
    
    //    var playerController = AVPlayerViewController()
    var player:AVPlayer?
    let avpController = AVPlayerViewController()
    
    
    @IBOutlet weak var videoView: UIView!
    
    @IBOutlet weak var imageBtnImage: UIImageView!
    @IBOutlet weak var videoBtnImage: UIImageView!
    @IBOutlet weak var btnView: UIView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var header: UIImageView!
    
    var images : [UIImageView] = [UIImageView]()
    var labelTextDes : [UILabel] = [UILabel]()
    
    @IBOutlet weak var labelTextIPAddress : UILabel!
    
    var minYBiggestImage : CGFloat = 0
    var maxYBiggestImage : CGFloat = 0
    var timer = Timer()
    var timerDelay = Timer()
    var isAutoSliding : Bool = true
    
    var mgr : SGWiFiUploadManager?
    
    @IBAction func showImageTutorial(_ sender: UIButton) {
        self.pageControl.isHidden = false
        self.scrollView.isHidden = false
        self.labelTextIPAddress.isHidden = false
        self.videoView.isHidden = true
        
        switchBtnIcon(x: imageBtnImage, y: videoBtnImage, a: "Group 1513", b: "tutorial_iconvideo" )
        
        avpController.player?.pause()
        
        //        self.labelTextIPAddress.bottomAnchor.constraint(equalTo: self.pageControl.topAnchor, constant: 30).isActive = true
        
    }
    
    @IBAction func showVideoTutorial(_ sender: UIButton) {
        self.videoView.isHidden = false
        self.pageControl.isHidden = true
        self.scrollView.isHidden = true
        
        switchBtnIcon(x: videoBtnImage, y: imageBtnImage,a: "Group 1514", b:"Group 1448")
        //        self.labelTextIPAddress.bottomAnchor.constraint(equalTo: self.pageControl.topAnchor, constant: 10).isActive = true
        
        playVideo()
    }
    
    func switchBtnIcon(x: UIImageView,y: UIImageView, a: String, b: String){
        
        x.image = UIImage(named: a)
        y.image = UIImage(named: b)
        x.frame = CGRect(x: 0, y: 0, width: self.btnView.frame.size.width, height: self.btnView.frame.size.height)
        
        y.frame = CGRect(x:self.btnView.frame.size.width/2 - 25, y: 0, width: 50, height: self.btnView.frame.size.height)
        y.contentMode = .center
    }
    
    func initVideo()
    {
        guard let path = Bundle.main.path(forResource: "tutorial", ofType: "mp4") else {
            debugPrint("tutorial.mp4 not found")
            return
        }
        
        player = AVPlayer(url: URL(fileURLWithPath: path))
        //        let avpController = AVPlayerViewController()
        avpController.player = player
//        avpController.showsPlaybackControls = false
        
        avpController.view.frame.size.height = videoView.frame.size.height
        
        avpController.view.frame.size.width = videoView.frame.size.width
        self.videoView.addSubview(avpController.view)
        self.addChild(avpController)
        
    }
    
    func loopVideo(videoPlayer: AVPlayer) {
        NotificationCenter.default.addObserver(forName: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: nil, queue: nil) { notification in
            videoPlayer.seek(to: CMTime.zero)
            videoPlayer.play()
        }
    }
    
    func playVideo(){
        // play video
        avpController.player?.play()
    }
    
    override func viewDidLayoutSubviews() {
        pageControl.transform = CGAffineTransform(scaleX: 2, y: 2)
        //        pageControl.subviews.forEach {
        //            $0.transform = CGAffineTransform(scaleX: 2, y: 2)
        //        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initVideo()
        self.title = "Import";
        self.videoView.isHidden = true
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Skip", style: .done, target: self, action: #selector(clickStart))
        navigationItem.rightBarButtonItem?.tintColor = UIColor.white
        
        mgr = SGWiFiUploadManager.shared()
        
        let scrollViewHeight:CGFloat = view.frame.height - header.frame.height
        let scrollViewWidth:CGFloat = view.frame.width
        self.scrollView.frame = CGRect(x:0, y: 0, width:scrollViewWidth, height:scrollViewHeight)
        
        let scrollViewWidthPage : CGFloat = scrollViewWidth * 3
        //2
        let slideWidth : CGFloat = 621 * 0.5
        let slideHeight : CGFloat = 495 * 0.5
        //3
        let offsetY : CGFloat = self.scrollView.frame.minY
        
        let slide1 = pageViewContent(0, offsetY, scrollViewWidth, scrollViewHeight, scrollViewWidthPage, "Hi User", "HD_icon1@3x", slideWidth, slideHeight, "1/ Use your PHONE and COMPUTER are connected to the same Wi-Fi Network.",0)
        let slide2 = pageViewContent(scrollViewWidth, offsetY, scrollViewWidth, scrollViewHeight, scrollViewWidthPage, "How To Play Music?", "HD_icon2@3x", slideWidth , slideHeight, "2/ Copy the IP ADDRESS below to your browser in COMPUTER.",0)
        let slide3 = pageViewContent(scrollViewWidth * 2, offsetY ,scrollViewWidth, scrollViewHeight, scrollViewWidthPage, "Enjoy The Songs", "HD_icon3@3x", slideWidth, slideHeight, "3/ Upload files you want to your device.\n", 0)
        
        self.scrollView.addSubview(slide1)
        self.scrollView.addSubview(slide2)
        self.scrollView.addSubview(slide3)
        //4
        self.scrollView.contentSize = CGSize(width: scrollViewWidthPage, height:scrollViewHeight)
        self.scrollView.delegate = self
        self.pageControl.currentPage = 0
        
        timer = Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(moveToNextPage), userInfo: nil, repeats: true)
        
        setupServer()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
    }
    
    @objc func moveToNextPage (){
        if(!isAutoSliding)
        {
            return
        }
        let pageWidth:CGFloat = self.scrollView.frame.width
        let maxWidth:CGFloat = pageWidth * 3
        let contentOffset:CGFloat = self.scrollView.contentOffset.x
        
        var slideToX = contentOffset + pageWidth
        
        if  contentOffset + pageWidth == maxWidth
        {
            slideToX = 0
        }
        self.scrollView.scrollRectToVisible(CGRect(x:slideToX, y:0, width:pageWidth, height:self.scrollView.frame.height), animated: true)
    }
    
    fileprivate func highlightTextDes(_ contentDes: String, _ highLightTexts : [String], _ isUnderline : Bool) -> NSAttributedString {
        let attributedString = NSMutableAttributedString(string: contentDes)
        var checkUnderline = isUnderline
        for highLightText in highLightTexts {
            let range = (contentDes as NSString).range(of: highLightText)
            attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont.boldSystemFont(ofSize: 17), range: range)
            if(checkUnderline)
            {
                attributedString.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: range)
                checkUnderline = false
            }
        }
        return attributedString
    }
    
    func pageViewContent(_ xFrame: CGFloat,_ yFrame : CGFloat,_ scrollViewWidth: CGFloat,_ scrollViewHeight: CGFloat,_ scrollViewWidthPage: CGFloat, _ contentTitle: String ,_ imageName: String,_ imageWidth: CGFloat,_ imageHeight: CGFloat,_ contentDes: String,_ numberLine: Int) -> UIView{
        
        let imgBg = UIView.init(frame: CGRect(x:xFrame, y:yFrame, width: scrollViewWidth, height: scrollViewHeight))
        //        imgBg.backgroundColor = UIColor.red
        let distanceY : CGFloat = imageHeight / 2;
        let imgOne = UIImageView(frame: CGRect(x: scrollViewWidth / 2 - imageWidth / 2, y: scrollViewHeight / 2 - distanceY, width: imageWidth, height: imageHeight))
        imgOne.image = UIImage(named: imageName)
        images.append(imgOne)
        imgBg.addSubview(imgOne)
        
        let textDes = UILabel.init(frame: CGRect(x : 0, y : scrollViewHeight / 2 - 533 - 100, width : scrollViewWidth, height: 50))
        
        textDes.textColor = UIColor(hexString: "#F8AD37")
        textDes.font = UIFont(name:"SegoeUI", size: 17.0)
        textDes.textAlignment = .center
        textDes.numberOfLines = numberLine
        labelTextDes.append(textDes)
        imgBg.addSubview(textDes)
        
        if(imageName == "HD_icon1@3x")
        {
            textDes.attributedText = highlightTextDes(contentDes, ["PHONE","COMPUTER"], false)
        }
        else if(imageName == "HD_icon2@3x")
        {
            minYBiggestImage = imgOne.frame.minY
            maxYBiggestImage = imgOne.frame.maxY
            textDes.attributedText = highlightTextDes(contentDes, ["IP ADDRESS","COMPUTER"], true)
            //            imgOne.setX(scrollViewWidth / 2 - imageWidth / 2 - 30)
            
            let imgArrow = UIImageView(frame: CGRect(x: imgOne.frame.minX + 25, y: imgOne.frame.minY - 120 * 0.6 / 2 - 30, width: 130 * 0.6, height: 120 * 0.6))
            imgArrow.image = UIImage(named: "arrow")
            imgBg.addSubview(imgArrow)
        }
        else if(imageName == "HD_icon3@3x")
        {
            for labeltextdes in labelTextDes
            {
                labeltextdes.setY(minYBiggestImage - 100)
            }
            labelTextIPAddress.frame = CGRect(x: 0, y: maxYBiggestImage + 80, width: scrollViewWidth, height: 100)
            
            pageControl.frame = CGRect(x: 0, y: labelTextIPAddress.frame.maxY - 10, width: scrollViewWidth, height: 50)
            
            textDes.attributedText = highlightTextDes(contentDes, ["Upload"], false)
            let textTitle = UILabel.init(frame: CGRect(x : 0, y : textDes.frame.maxY, width : scrollViewWidth, height: 50))
            //        textTitle.setAnchorPoint(CGPoint(x : 0.5, y : 0))
            textTitle.textColor = UIColor(hexString: "#F8AD37")
            textTitle.font = UIFont(name:"SegoeUI", size: 30.0)
            textTitle.text = contentTitle
            textTitle.textAlignment = .center
            imgBg.addSubview(textTitle)
        }
        
        return imgBg
    }
    
    func setupServer() {
        let success = mgr?.startHTTPServer(atPort: 8080)
        if (success)!{
            mgr?.setFileUploadStartCallback ({ fileName, savePath in
                print("Filesdfs \(fileName ?? "") Upload Start")
            })
            mgr?.setFileUploadProgressCallback({ fileName, savePath, progress in
                print("Filedfd \(fileName ?? "") on progress \(progress)")
            })
            mgr?.setFileUploadFinishCallback({ fileName, savePath in
                print("xxx Fileasa Upload Finish \(fileName ?? "") at \(savePath ?? "")")
                let newPath = getSongPath(songName: fileName!)
                let fileManager = FileManager.default
                do{
                    try fileManager.moveItem(atPath: savePath!, toPath: newPath.path)
                }catch{
                    
                }
                let storage = UserDefaults.standard
                if storage.string(forKey: defaultsKeys.APP_REVIEW_TRANSFER) == nil
                {
                    StoreReviewHelper.requestReview()
                    storage.set(1, forKey: defaultsKeys.APP_REVIEW_TRANSFER)
                }
            })
        }
        
        if (mgr?.httpServer.isRunning) != nil {
            if HYBIPHelper.deviceIPAdress() == nil {
                labelTextIPAddress.text = "Need to connect Wi-Fi"
                labelTextIPAddress.textColor = UIColor.red
                return
            }
            if let ip = mgr?.ip(){
                if let port = mgr?.port(){
                    let ip_port : String = "http://\(ip):\(port)"
                    labelTextIPAddress.attributedText = highlightTextDes("IP Address:\n" + ip_port, ["\(ip_port)"], false)
                    labelTextIPAddress.textColor = UIColor(hexString: "#E06D33")
                }
            }
            
        } else {
            labelTextIPAddress.text = "Error, Server Stopped"
            labelTextIPAddress.textColor = UIColor.red
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func clickStart(_ sender: Any)
    {
        NotificationCenter.default.addObserver(self, selector: #selector(ShowAdsFull), name: .CLOSE_TUTORIAL, object: nil)
        if(fullAds != nil)
        {
            fullAds.delegate = self
        }
        else
        {
            createAndLoadInterstitial()
        }
        
        showActivityIndicatoryCountDown(isRV : false, pos: .CLOSE_TUTORIAL){
            (str) in
            if str == "removeads"
            {
                self.closeTutorial()
            }
        }
    }
    
    @objc func ShowAdsFull()
    {
        DispatchQueue.main.async
            {
                if !showAdsInterstitial(self)
                {
                    self.closeTutorial()
                }
        }
        
    }
    
    fileprivate func closeTutorial() {
        NotificationCenter.default.removeObserver(self, name: .CLOSE_TUTORIAL, object: nil)
        if(isAutoSliding)
        {
            timer.invalidate()
        }
        mgr?.stopHTTPServer()
        if(fullAds != nil)
        {
            fullAds.delegate = CustomTabbarViewController.shared
        }
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "ShowAdsFull\(PositionAdsFull[0])"), object: nil)
        self.dismiss(animated: true, completion: nil)
    }
    
    func interstitialDidDismissScreen(_ ad: GADInterstitial) {
        createAndLoadInterstitial()
        closeTutorial()
        
    }
    func interstitialDidFail(toPresentScreen ad: GADInterstitial) {
        print("aaaa error: ")
    }
    
}
private typealias ScrollView = TutorialViewController
extension ScrollView
{
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let pageIndex = round(scrollView.contentOffset.x/view.frame.width)
        pageControl.currentPage = Int(pageIndex)
        
        let maximumHorizontalOffset: CGFloat = scrollView.contentSize.width - scrollView.frame.width
        let currentHorizontalOffset: CGFloat = scrollView.contentOffset.x
        
        // vertical
        let maximumVerticalOffset: CGFloat = scrollView.contentSize.height - scrollView.frame.height
        let currentVerticalOffset: CGFloat = scrollView.contentOffset.y
        
        let percentageHorizontalOffset: CGFloat = currentHorizontalOffset / maximumHorizontalOffset
        let percentageVerticalOffset: CGFloat = currentVerticalOffset / maximumVerticalOffset
        
        
        /*
         * below code changes the background color of view on paging the scrollview
         */
        //        self.scrollView(scrollView, didScrollToPercentageOffset: percentageHorizontalOffset)
        
        
        /*
         * below code scales the imageview on paging the scrollview
         */
        let percentOffset: CGPoint = CGPoint(x: percentageHorizontalOffset, y: percentageVerticalOffset)
        let percent1 : CGFloat = 0.5
        let percent2 : CGFloat = 1
        
        if(percentOffset.x > 0 && percentOffset.x <= percent1)
        {
            images[0].transform = CGAffineTransform(scaleX: (percent1-percentOffset.x)/percent1, y: (percent1 - percentOffset.x)/percent1)
            images[1].transform = CGAffineTransform(scaleX: percentOffset.x/percent1, y: percentOffset.x/percent1)
        } else if(percentOffset.x > percent1 && percentOffset.x <= percent2)
        {
            images[1].transform = CGAffineTransform(scaleX: (percent2-percentOffset.x)/percent1, y: (percent2-percentOffset.x)/percent1)
            images[2].transform = CGAffineTransform(scaleX: percentOffset.x/percent2, y: percentOffset.x/percent2)
        }
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        if(!isAutoSliding)
        {
            timerDelay.invalidate()
        }
        isAutoSliding = false
        timerDelay = Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(turnOnAutoSliding), userInfo: nil, repeats: false)
    }
    
    @objc func turnOnAutoSliding ()
    {
        isAutoSliding = true
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView){
        // Test the offset and calculate the current page after scrolling ends
        let pageWidth:CGFloat = scrollView.frame.width
        let currentPage:CGFloat = floor((scrollView.contentOffset.x-pageWidth/2)/pageWidth)+1
        // Change the indicator
        self.pageControl.currentPage = Int(currentPage);
        // Change the text accordingly
        if Int(currentPage) == 0{
            
        }else if Int(currentPage) == 1{
            
        }else{
            
        }
    }
    
}

