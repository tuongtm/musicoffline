//
//  LibraryViewController.swift
//  Offline Music
//
//  Created by NhonGa on 04/09/2019.
//  Copyright © 2019 Free App & Game. All rights reserved.
//

import UIKit
import MediaPlayer
import AVFoundation
import GoogleMobileAds

class LibraryViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet var tableView : UITableView?
    @IBOutlet weak var bannerView: GADBannerView!
    @IBOutlet weak var heightConstraintBannerView: NSLayoutConstraint!
    
    
    var songs : [SongInfo] = []
    var songQuery: SongQuery = SongQuery()
    var audio: AVAudioPlayer?
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.title = "Songs"
        tableView?.delegate = self
        tableView?.dataSource = self
        if #available(iOS 9.3, *) {
            MPMediaLibrary.requestAuthorization { (status) in
                if status == .authorized {
                    DispatchQueue.main.async {
                        self.songs = self.songQuery.getAllSong()
//                        self.albums = self.songQuery.get(songCategory: "Artist")
//                        self.albums.removeAll(where: { (albuminfo) -> Bool in
//                            return albuminfo.albumTitle == "Songs of Innocence"
//                        })
                        //                        self.tableView?.rowHeight = UITableView.automaticDimension
                        //                        self.tableView?.estimatedRowHeight = 80.0
                        self.tableView?.reloadData()
                    }
                } else {
                    self.displayMediaLibraryError()
                }
            }
        } else {
            // Fallback on earlier versions
        }
        bannerView = createAndLoadBanner(bannerView, self, heightConstraintBannerView)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    func displayMediaLibraryError() {
        
        var errorDes: String = "Unknown error"
        if #available(iOS 9.3, *) {
            switch MPMediaLibrary.authorizationStatus() {
            case .restricted:
                errorDes = "Media library access restricted by corporate or parental settings"
            case .denied:
                errorDes = "Media library access denied by user"
            default:
                errorDes = "Unknown error"
            }
        } else {
            // Fallback on earlier versions
        }
        
        let controller = UIAlertController(title: "Error", message: errorDes, preferredStyle: .alert)
        controller.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        controller.addAction(UIAlertAction(title: "Open Settings", style: .default, handler: { (action) in
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!, options: [:], completionHandler: nil)
            } else {
                // Fallback on earlier versions
            }
        }))
        present(controller, animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
        
    }
    
//    func numberOfSections(in tableView: UITableView) -> Int {
//        return albums.count
//    }
    
    func tableView( _ tableView: UITableView, numberOfRowsInSection section: Int ) -> Int  {
        
        return songs.count
    }
    
    func tableView( _ tableView: UITableView, cellForRowAt indexPath:IndexPath ) -> UITableViewCell {
        tableView.rowHeight = 80
        let cell = tableView.dequeueReusableCell(withIdentifier: "MusicPlayerCell",
                                                 for: indexPath) as! MusicPlayerCell
        cell.labelMusicTitle?.text = songs[indexPath.row].songTitle
        cell.labelMusicDescription?.text = songs[indexPath.row].artistName
        let songId: NSNumber = songs[indexPath.row].songId
        let item: MPMediaItem = songQuery.getItem( songId: songId )
        
        if let imageSound: MPMediaItemArtwork = item.value( forProperty: MPMediaItemPropertyArtwork ) as? MPMediaItemArtwork {
            cell.imageMusic?.image = imageSound.image(at: CGSize(width: cell.imageMusic.frame.size.width, height: cell.imageMusic.frame.size.height))
        }
        else
        {
            cell.imageMusic?.image = UIImage.init(named: "icon_music.png")
        }
        return cell
    }
    
//    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//        let vw = UIView.init(frame: CGRect.init(x: 0, y: 0, width: view.frame.width, height: 30))
//        vw.backgroundColor = UIColor(hexString: "#F9E2D6")
//
//        let textTitle = UILabel.init(frame: CGRect(x : 10, y : 0, width : view.frame.width - 10, height: vw.frame.height))
//        textTitle.textColor = UIColor(hexString: "#F8AD37")
//        textTitle.font = UIFont(name:"SegoeUI-Bold", size: 17)
//        textTitle.text = albums[section].albumTitle
//        vw.addSubview(textTitle)
//
//        return vw
//    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let songId: NSNumber = songs[indexPath.row].songId
        let item: MPMediaItem = songQuery.getItem( songId: songId )
        guard let url: URL = item.value( forProperty: MPMediaItemPropertyAssetURL ) as? URL
            else
        {
            showAlertDialog(title: "Import failed", subtitle: "Your file is failed to import due to an error.", cancelTitle: "Ok", cancelHandler: nil)
            return
        }
        //        do {
        //            audio = try AVAudioPlayer(contentsOf: url)
        //            guard let player = audio else { return }
        //
        //            player.prepareToPlay()
        //            player.play()
        //        } catch let error {
        //            print(error.localizedDescription)
        //        }
        //
        let songTitle = songs[indexPath.row].songTitle
        
        export(url: url, songTitle: songTitle) { (url, error) in
            self.hideActivityIndicatory()
            print("xxx ahihi \(error ?? "" as! Error)")
        }
    }
    
    func export(url: URL, songTitle : String, completionHandler: @escaping (_ fileURL: URL?, _ error: Error?) -> ()) {
        guard let exportSession = AVAssetExportSession(asset: AVURLAsset(url: url), presetName: AVAssetExportPresetAppleM4A) else {
            hideActivityIndicatory()
//            self.navigationController!.popViewController(animated: true)
            self.showAlertDialog(title: "Error", subtitle: "Importing errors \n Please try again!", cancelTitle: "OK", cancelHandler: nil)
            return
        }
        
        let outputURL = getSongPath(songName: "\(songTitle).m4a")
        if FileManager.default.fileExists(atPath: outputURL.path)
        {
            DispatchQueue.main.async{
//                    self.navigationController!.popViewController(animated: true)
                self.showAlertDialog(title: "Error", subtitle: "This file is existed!", cancelTitle: "OK", cancelHandler: nil)
                return
            }
        }
        
        showActivityIndicatoryTitle(title: "Importing")
        
        exportSession.outputFileType = .m4a
        exportSession.metadata = AVURLAsset(url: url).metadata
        //        exportSession.shouldOptimizeForNetworkUse = true
        
        
        /* Dont't forget to remove the existing url, or exportSession will throw error: can't save file */
        //        do {
        //            try FileManager.default.removeItem(at: outputURL)
        //        } catch let error as NSError {
        //            print(error.debugDescription)
        //        }
        
        exportSession.outputURL = outputURL
        exportSession.exportAsynchronously(completionHandler: {
            DispatchQueue.main.async
                {
                    self.hideActivityIndicatory()
                    if exportSession.status == .completed {
                        self.saveFileAudio("\(songTitle).m4a")
                        let storage = UserDefaults.standard
                        if storage.string(forKey: defaultsKeys.APP_REVIEW_LIBRARY) == nil
                        {
                            StoreReviewHelper.requestReview()
                            storage.set(1, forKey: defaultsKeys.APP_REVIEW_LIBRARY)
                        }
                        self.showAlertDialog(title: "Save successfully", subtitle: "", cancelTitle: "Ok", cancelHandler: nil)
                    } else {
                        print("AV export failed with error:- ", exportSession.error!.localizedDescription)
                    }
            }
        })
    }
    
    fileprivate func saveFileAudio(_ nameFile : String) {
        do{
            let pathComponent = getSongPath(songName: nameFile)
            let attrs = try FileManager.default.attributesOfItem(atPath: pathComponent.path) as NSDictionary
            
            let fileStep = try loadDataJson(NAME_LIST_JSON) as Data
            var vendorArray = try JSONDecoder().decode([DataListJson].self, from: fileStep) as [DataListJson]
            let dataNew : DataListJson = DataListJson.init(pathComponent.path, nameFile, "\(attrs.fileSize())", attrs.fileCreationDate()!.description)
            vendorArray.append(dataNew)
            try savePropertyList(NAME_LIST_JSON, vendorArray)
        } catch{
            print(error)
        }
    }
    
    fileprivate func setBadgeValueForTabMyTunes(_ countMyTunesDownload: Int) {
        if let tabItems = tabBarController?.tabBar.items {
            // In this case we want to modify the badge number of the third tab:
            let tabItem = tabItems[2]
            tabItem.badgeValue = "\(countMyTunesDownload)"
        }
    }
    
    func countMyTunes()
    {
        guard var countMyTunesDownload = UserDefaults.standard.value(forKey: defaultsKeys.APP_MYTUNES) as? Int else {
            UserDefaults.standard.set(1, forKey: defaultsKeys.APP_MYTUNES)
            setBadgeValueForTabMyTunes(1)
            return
        }
        countMyTunesDownload += 1
        UserDefaults.standard.set(countMyTunesDownload, forKey: defaultsKeys.APP_MYTUNES)
        setBadgeValueForTabMyTunes(countMyTunesDownload)
    }
}
