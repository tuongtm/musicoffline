//
//  CustomImportHeader.swift
//  Offline Music
//
//  Created by Hai on 8/13/19.
//  Copyright © 2019 Free App & Game. All rights reserved.
//

import UIKit

class CustomImportHeader: UIView {
    @IBOutlet public var titleLabel: UILabel!
    @IBOutlet weak var iconIamge: UIImageView!
    class func loadViewFromNib() -> CustomImportHeader? {
        let bundle = Bundle.main
        let nib = UINib(nibName: "CustomImportHeader", bundle: bundle)
        guard
            let view = nib.instantiate(withOwner: CustomImportHeader())
                .first as? CustomImportHeader
            else {
                return nil
        }
        return view
    }

}
