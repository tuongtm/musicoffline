//
//  HomeViewBannerTableViewCell.swift
//  Offline Music
//
//  Created by NhonGa on 13/07/2019.
//  Copyright © 2019 Free App & Game. All rights reserved.
//

import UIKit

class HomeViewBannerTableViewCell: UITableViewCell {
    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var txTitle: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
