//
//  HomeViewTableViewCell.swift
//  Music1
//
//  Created by NhonGa on 11/05/2019.
//  Copyright © 2019 ThreeWolves. All rights reserved.
//

import UIKit

struct BodyCollectionView {
    var title : String
    var image : String
    var isLogin : Bool
}

var BodyCollectionViewAll = [
    BodyCollectionView(title: "Paste URL", image: "import_iconpasteurl", isLogin: false),
    BodyCollectionView(title: "Browser", image: "import_iconbrowser", isLogin: false),
    BodyCollectionView(title: "Transfer Wifi", image: "import_iconwifitransfer", isLogin: false),
]

class HomeViewTableViewCell: UITableViewCell {
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var tableViewCellCloudMusic: UITableViewCell!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.collectionView.register(UINib(nibName: "HomeViewCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "HomeViewCollectionViewCell")
        collectionView.delegate = self
        collectionView.dataSource = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

extension HomeViewTableViewCell : UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return BodyCollectionViewAll.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let row = indexPath.row
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeViewCollectionViewCell", for: indexPath) as! HomeViewCollectionViewCell
        cell.imIconImport.image = UIImage.init(named: BodyCollectionViewAll[row].image)
        cell.lbNameImport.text = BodyCollectionViewAll[row].title
        cell.lbNameImport.font = UIFont(name: "SegoeUI", size: 15)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let selectRow = indexPath.row
        switch selectRow {
        case 0:
            print("Paste URL")
            HomeViewController.shared!.showPasteURL()
            break
        case 1:
            print("Browser")
            HomeViewController.shared!.showAlertDialog(title: "Comming soon!", subtitle: "", cancelTitle: "OK", cancelHandler: nil)
            break
        default:
            HomeViewController.shared!.showTutorial("Tutorial")
            break
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        let width : CGFloat = collectionView.frame.width
        let totalBody : CGFloat =  CGFloat(BodyCollectionViewAll.count)
        return CGSize(width:  width / totalBody - 10, height: 120)
    }
    
}
