//
//  SongQuery.swift
//  Offline Music
//
//  Created by NhonGa on 04/09/2019.
//  Copyright © 2019 Free App & Game. All rights reserved.
//

import Foundation
import MediaPlayer

struct SongInfo {
    
    var albumTitle: String
    var artistName: String
    var songTitle:  String
    
    var songId   :  NSNumber
}

struct AlbumInfo {
    
    var albumTitle: String
    var songs: [SongInfo]
}

class SongQuery {
    
    func getAllSong() -> [SongInfo]
    {
        var songs: [SongInfo] = []
        let albumsQuery: MPMediaQuery = MPMediaQuery.songs()
        guard let albumItems = albumsQuery.items as? [MPMediaItem]
            else
        {
            return songs
        }
        
        for song in albumItems {
            let songInfo: SongInfo = SongInfo(
                albumTitle: song.value(forProperty: MPMediaItemPropertyAlbumTitle ) as? String ?? "Unknow Album",
                artistName: song.value(forProperty: MPMediaItemPropertyArtist ) as? String ?? "Unknow Artist",
                songTitle:  song.value(forProperty: MPMediaItemPropertyTitle ) as? String ?? "Unknow Title",
                songId:     song.value(forProperty: MPMediaItemPropertyPersistentID ) as! NSNumber
            )
            songs.append( songInfo )
        }
        return songs
    }
    
    func get(songCategory: String) -> [AlbumInfo] {
        
        var albums: [AlbumInfo] = []
        let albumsQuery: MPMediaQuery
        if songCategory == "Artist" {
            albumsQuery = MPMediaQuery.artists()
            
        } else if songCategory == "Album" {
            albumsQuery = MPMediaQuery.albums()
            
        } else {
            albumsQuery = MPMediaQuery.albums()
        }
        
        
        // let albumsQuery: MPMediaQuery = MPMediaQuery.albums()
        let albumItems : [MPMediaItemCollection] = albumsQuery.collections! as [MPMediaItemCollection]
        //  var album: MPMediaItemCollection
        
        for album in albumItems {
           
            let albumItems: [MPMediaItem] = album.items as [MPMediaItem]
            // var song: MPMediaItem
            
            var songs: [SongInfo] = []
            
            var albumTitle: String = "Unknow Album"
            
            for song in albumItems {
                if songCategory == "Artist" {
                    albumTitle = song.value(forProperty: MPMediaItemPropertyArtist ) as? String ?? "Unknow Artist"
                } else if songCategory == "Album" {
                    albumTitle = song.value(forProperty: MPMediaItemPropertyAlbumTitle ) as? String ?? "Unknow Album"
                } else {
                    albumTitle = song.value(forProperty: MPMediaItemPropertyAlbumTitle ) as? String ?? "Unknow Album"
                }
                
                let songInfo: SongInfo = SongInfo(
                    albumTitle: song.value(forProperty: MPMediaItemPropertyAlbumTitle ) as? String ?? "Unknow Album",
                    artistName: song.value(forProperty: MPMediaItemPropertyArtist ) as? String ?? "Unknow Artist",
                    songTitle:  song.value(forProperty: MPMediaItemPropertyTitle ) as? String ?? "Unknow Title",
                    songId:     song.value(forProperty: MPMediaItemPropertyPersistentID ) as! NSNumber
                )
                songs.append( songInfo )
            }
            
            albumTitle = albumTitle == "" ? "Unknow Album" : albumTitle
            let albumInfo: AlbumInfo = AlbumInfo(
                albumTitle: albumTitle,
                songs: songs
            )
            
            albums.append( albumInfo )
        }
        
        return albums
        
    }
    
    func getItem( songId: NSNumber ) -> MPMediaItem {
        
        let property: MPMediaPropertyPredicate = MPMediaPropertyPredicate( value: songId, forProperty: MPMediaItemPropertyPersistentID )
        
        let query: MPMediaQuery = MPMediaQuery()
        query.addFilterPredicate( property )
        
        let items: [MPMediaItem] = query.items! as [MPMediaItem]
        
        return items[items.count - 1]
        
    }
    
}
