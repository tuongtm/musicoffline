//
//  HomeCloudMusicTableViewCell.swift
//  Offline Music
//
//  Created by Hai on 8/13/19.
//  Copyright © 2019 Free App & Game. All rights reserved.
//

import UIKit

class HomeCloudMusicTableViewCell: UITableViewCell {

    @IBOutlet weak var iconCloudImage: UIImageView!
    @IBOutlet weak var iconCloudTitle: UILabel!
    
    @IBOutlet weak var iconCloudSubTitle: UILabel!
    @IBOutlet weak var logIcon: UIImageView!
    @IBOutlet weak var logStatus: UILabel!
    @IBOutlet weak var btnLogOut: UIButton!
}
