//
//  MusicPlayerCell.swift
//  Offline Music
//
//  Created by NhonGa on 04/09/2019.
//  Copyright © 2019 Free App & Game. All rights reserved.
//

import UIKit

class MusicPlayerCell: UITableViewCell {

    @IBOutlet weak var imageMusic: UIImageView!
    @IBOutlet weak var labelMusicTitle: UILabel!
    @IBOutlet weak var labelMusicDescription: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
