//
//  HomeViewCollectionViewCell.swift
//  Offline Music
//
//  Created by NhonGa on 13/07/2019.
//  Copyright © 2019 Free App & Game. All rights reserved.
//

import UIKit

class HomeViewCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imIconImport: UIImageView!
    @IBOutlet weak var lbNameImport: UILabel!
}
