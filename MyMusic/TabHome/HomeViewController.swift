//
//  FirstViewController.swift
//  Tubidy
//
//  Created by NhonGa on 14/10/2018.
//  Copyright © 2018 ThreeWolves. All rights reserved.
//

import UIKit
import GoogleMobileAds
import GoogleSignIn
import GoogleAPIClientForREST
import SwiftyDropbox
import OneDriveSDK
import Alamofire
import MediaPlayer
import AVFoundation
import SwiftSoup
import WebKit

let service = GTLRDriveService()
let auth = DropboxClientsManager.authorizedClient!.auth!
let users = DropboxClientsManager.authorizedClient!.users!
let files = DropboxClientsManager.authorizedClient!.files!
let sharing = DropboxClientsManager.authorizedClient!.sharing!
var clientsOneDrive : ODClient? = nil
var isFirstToVC : Bool = false
var myIndex : Int = 0

struct CloudMusicOption
{
    var image1 : String
    var title1 : String
    var title2 : String
    var image2 : String
    var statusTitle : String
    var isLogin: Bool
}
struct ImportMusicOption {
    var title : String
    var image : String
    var isLogin : Bool
}
//struct BodyBanner
//{
//    var appId : String
//}

var cloudMusicOptions = [
    CloudMusicOption(image1: "import_icondropbox", title1: "DropBox", title2: "Tap to log in", image2: "import_iconlogin", statusTitle: "Log in", isLogin: false),
    CloudMusicOption(image1: "import_icongoogledrive", title1: "Google Drive", title2: "Tap to log in", image2: "import_iconlogin", statusTitle: "Log in", isLogin: false),
    CloudMusicOption(image1: "import_icononedrive", title1: "One Drive", title2: "Tap to log in", image2: "import_iconlogin", statusTitle: "Log in", isLogin: false)
]

var importMusicOptions = [
    CloudMusicOption(image1: "import_iconpasteurl", title1: "Paste URL", title2: "Download music from URL", image2: "", statusTitle: "", isLogin: false),
    CloudMusicOption(image1: "import_iconbrowser", title1: "Wi-fi Transfer", title2: "Import music from computer", image2: "", statusTitle: "", isLogin: false),
    CloudMusicOption(image1: "import_iconlibrary", title1: "Media Library", title2: "Sync songs from Library", image2: "", statusTitle: "", isLogin: false)
    ]

//let libraryOption = CloudMusicOption(image1: "import_iconlibrary", title1: "Media Library", title2: "Sync songs from iTunes", image2: "", statusTitle: "", isLogin: false)
class HomeViewController: UIViewController, MPMediaPickerControllerDelegate, WKNavigationDelegate {
    
    static var shared : HomeViewController? = nil
    @IBOutlet weak var bannerView: GADBannerView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var heightConstraintBannerView: NSLayoutConstraint!
    
    @IBOutlet weak var homeLb: UILabel!
    @IBOutlet weak var musicLb: UILabel!
    @IBAction func btnCloseAd(_ sender: UIButton) {

    }
    
    @IBOutlet weak var viewSub: UIView!
    
    fileprivate let headerSectionTableView =
        ["Import Music",
         "CLOUD MUSIC",
        ]
    
    let headerSectionIconImage =
        ["import_iconheaderimportmusic",
         "import_iconheadercloudmusic",
         "import_iconheadermedialibrary"]
    
    let libraryOption = CloudMusicOption(image1: "import_iconlibrary", title1: "Media Library", title2: "Sync songs from iTunes", image2: "", statusTitle: "", isLogin: false)
    
    fileprivate var theWebView : WKWebView! =  {
        let webConfiguration = WKWebViewConfiguration()
        let webView = WKWebView(frame: .zero, configuration: webConfiguration)
        return webView
    }()
    var urlString : String = ""
    //    var isLoadDone : Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.addSubview(theWebView)
        self.theWebView.navigationDelegate = self
        
        // Do any additional setup after loading the view, typically from a nib.
        HomeViewController.shared = self
        setupGoogleSignIn()
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorColor = UIColor.clear
        
        bannerView = createAndLoadBanner(bannerView, self, heightConstraintBannerView)
        //        GADRewardBasedVideoAd.sharedInstance().load(GADRequest(),
        //                                                    withAdUnitID: Constants.VIDEO_ID)
        
        do
        {
            let fileStep = try loadDataJson(NAME_PLAYLIST_JSON) as Data
            let vendorArray = try JSONDecoder().decode([DataPlayListJson].self, from: fileStep) as [DataPlayListJson]
            audioPlayList = vendorArray
            
            let fileStep1 = try loadDataJson(NAME_LIST_JSON) as Data
            let vendorArray1 = try JSONDecoder().decode([DataListJson].self, from: fileStep1) as [DataListJson]
            audioList = vendorArray1
            
        }
        catch
        {
            let jsonstr = "[]"
            do
            {
                try jsonstr.write(to: plistURL(NAME_LIST_JSON), atomically: false, encoding: .utf8)
                try jsonstr.write(to: plistURL(NAME_PLAYLIST_JSON), atomically: false, encoding: .utf8)
            }
            catch
            {
                print("xxx \(error)")
            }
        }
        
        UIDevice.current.setValue(UIInterfaceOrientation.portrait.rawValue, forKey: "orientation")
//        homeLb.textColor = UIColor.init(red: 0, green: 0, blue: 0)
//        homeLb.text = "IMPORT"
//        homeLb.font = UIFont(name:"SegoeUI-Bold", size: 25)
//        musicLb.text = "MUSIC"
//        musicLb.font = UIFont(name:"SegoeUI-Bold", size: 25)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        AppUtility.lockOrientation(.portrait)
        grabData()
        AppUtility.lockOrientation(.portrait)
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        //        AppUtility.lockOrientation(.all)
        do {
            //keep alive audio at background
            if #available(iOS 10.0, *) {
                try AVAudioSession.sharedInstance().setCategory(.playback, mode: .default, options: [])
            } else {
                // Fallback on earlier versions
                try AudioSessionHelper.setAudioSession()
            }
            try AVAudioSession.sharedInstance().setActive(true)
        } catch _ {
        }
    }
    
    @objc func grabData(){
        if let userDropBoxClient = DropboxClientsManager.authorizedClient?.users {
            userDropBoxClient.getCurrentAccount().response { response, error in
                if let result = response {
                    //Do Something
                    cloudMusicOptions[0].isLogin = true
                    cloudMusicOptions[0].title2 = result.email
                    self.tableView.reloadData()
                } else if let callError = error {
                    print("dropbox error : \(callError)")
                }
            }
        }
    }
    
    func clickIconGoogleDrive() {
        //print("xxx: login Gg")
        if !cloudMusicOptions[1].isLogin{
            GIDSignIn.sharedInstance().signIn()
        }else {
            performSegue(withIdentifier: "segue", sender: self)
        }
    }
    
    func clickIconDropBox() {
        if DropboxClientsManager.authorizedClient == nil{
            DropboxClientsManager.authorizeFromController(UIApplication.shared, controller: self, openURL: {(url: URL) -> Void in UIApplication.shared.openURL(url)})
        }else{
            performSegue(withIdentifier: "segue", sender: self)
        }
    }
    
    func clickIconOneDrive() {
        if clientsOneDrive == nil{
            ODClient.authenticatedClient(completion: {clients , errors in
                if errors == nil{
                    clientsOneDrive = clients
                    clients!.drive().request().getWithCompletion { (drive, error) -> Void in
                        if(error == nil){
                            cloudMusicOptions[2].title2 = (drive?.owner.user.displayName)!
                            cloudMusicOptions[2].isLogin = true
                            DispatchQueue.main.async {
                                // Do something
                                self.tableView.reloadData()
                            }
                        }
                    }
                }
            })
        }else{
            performSegue(withIdentifier: "segue", sender: self)
        }
    }
    
    
    func LogOut(_ index : Int){
        let row = index
        cloudMusicOptions[row].title2 = "Tap to log in"
        cloudMusicOptions[row].isLogin = false
        self.tableView.reloadData()
        switch row
        {
        case 1:
            GIDSignIn.sharedInstance().signOut()
            break
        case 0:
            DropboxClientsManager.unlinkClients()
            break
        case 2:
            clientsOneDrive?.signOut(completion: {(error) in
                clientsOneDrive = nil
            })
            break
        default:
            break
        }
    }
    
    private func setupGoogleSignIn() {
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().presentingViewController = self
        GIDSignIn.sharedInstance().scopes = [kGTLRAuthScopeDrive]
        //                                             kGTLRAuthScopeDriveAppdata,
        //                                             kGTLRAuthScopeDriveFile,
        //                                             kGTLRAuthScopeDriveMetadata,
        //                                             kGTLRAuthScopeDriveMetadataReadonly,
        //                                             kGTLRAuthScopeDrivePhotosReadonly,
        //                                             kGTLRAuthScopeDriveReadonly,
        //                                             kGTLRAuthScopeDriveScripts]
        GIDSignIn.sharedInstance().restorePreviousSignIn()
    }
    
    func mediaPickerDidCancel(mediaPicker: MPMediaPickerController) {
        print("xxx: Cancle")
    }
    
    func showLibrary()
    {
        if #available(iOS 9.3, *)
        {
            let mediaPicker = MPMediaPickerController(mediaTypes: .anyAudio)
            
            mediaPicker.delegate = self
            //        mediaPicker.allowsPickingMultipleItems = true
            self.present(mediaPicker, animated: true, completion:nil)
            
            if MPMediaLibrary.authorizationStatus() == MPMediaLibraryAuthorizationStatus.authorized
            {
                
            }
            else
            {
                let storage = UserDefaults.standard
                if storage.string(forKey: defaultsKeys.APP_FIRST_OPEN_LIBRARY) == nil
                {
                    storage.set(1, forKey: defaultsKeys.APP_FIRST_OPEN_LIBRARY)
                }
                else
                {
                    let urlSetting = URL(string: UIApplication.openSettingsURLString)!
                    if UIApplication.shared.canOpenURL(urlSetting)
                    {
                        UIApplication.shared.openURL(urlSetting)
                    }
                }
            }
        }
        else
        {
            let mediaPicker = MPMediaPickerController(mediaTypes: .anyAudio)
            
            mediaPicker.delegate = self
            //        mediaPicker.allowsPickingMultipleItems = true
            
            self.present(mediaPicker, animated: true, completion:nil)
        }
    }
    func mediaPicker(_ mediaPicker: MPMediaPickerController, didPickMediaItems mediaItemCollection: MPMediaItemCollection) {
        guard let mediaItem = mediaItemCollection.items.first else {  return  }
        guard let url = mediaItem.assetURL else { return }
        guard let songTitle = mediaItem.title else {  return }
        guard let exportSession = AVAssetExportSession(asset: AVURLAsset(url: url), presetName: AVAssetExportPresetAppleM4A) else {
            return
        }
        
        let outputURL = getSongPath(songName: "\(songTitle).m4a")
        if FileManager.default.fileExists(atPath: outputURL.path)
        {
            DispatchQueue.main.async
                {
                    self.dismiss(animated: true, completion: nil)
                    return
            }
        }
        
        mediaPicker.showActivityIndicatory()
        
        exportSession.outputFileType = .m4a
        exportSession.metadata = AVURLAsset(url: url).metadata
        //        exportSession.shouldOptimizeForNetworkUse = true
        
        
        /* Dont't forget to remove the existing url, or exportSession will throw error: can't save file */
        //        do {
        //            try FileManager.default.removeItem(at: outputURL)
        //        } catch let error as NSError {
        //            print(error.debugDescription)
        //        }
        
        exportSession.outputURL = outputURL
        exportSession.exportAsynchronously(completionHandler: {
            if exportSession.status == .completed {
                DispatchQueue.main.async
                    {
                        self.saveFileAudio("\(songTitle).m4a")
                        self.dismiss(animated: true, completion: nil)
                        let storage = UserDefaults.standard
                        if storage.string(forKey: defaultsKeys.APP_REVIEW_LIBRARY) == nil
                        {
                            StoreReviewHelper.requestReview()
                            storage.set(1, forKey: defaultsKeys.APP_REVIEW_LIBRARY)
                        }
                }
            } else {
                print("AV export failed with error:- ", exportSession.error!.localizedDescription)
            }
        })
        
        
        //        for mpMediaItem in mediaItemCollection.items
        //        {
        //            if let inputURL = mpMediaItem.assetURL {
        //                do {
        //
        //                } catch {
        //                    print("Unable to load data: \(error)")
        //                }
        //            }
        //        }
    }
    
    func mediaPickerDidCancel(_ mediaPicker: MPMediaPickerController) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func exportVideoAndAudio(url : String, typeFile : String, fileName : String, path : URL)
    {
       DispatchQueue.main.async {
            var typeAVMeidaTypeFile : AVMediaType = .audio
            var typeOutputTypeFile : AVFileType = .mp3
            if typeFile == "mp3"
            {
                typeAVMeidaTypeFile = .audio
                typeOutputTypeFile = .mp3
            }
            else
            {
                typeAVMeidaTypeFile = .video
                typeOutputTypeFile = .mp4
            }
            let videoAsset = AVAsset.init(url: URL.init(string: url)!) as! AVAsset
            print("xxx \(typeAVMeidaTypeFile) \(typeOutputTypeFile)")
            //                let clipVideoTrack = videoAsset.tracks(withMediaType: AVMediaType.video).first! as AVAssetTrack

            let composition = AVMutableComposition()
            let videoCompositions = composition.addMutableTrack(withMediaType: typeAVMeidaTypeFile, preferredTrackID: CMPersistentTrackID())
            do {
                try videoCompositions?.insertTimeRange(CMTimeRangeMake(start: CMTime.zero, duration: videoAsset.duration), of: (videoAsset.tracks(withMediaType: typeAVMeidaTypeFile).first)!, at: CMTime.zero)
            } catch {
                print("handle insert error")
                return
            }
            let videoDuration = videoAsset.duration
            let finalTimeScale:Int64 = (Int64(float_t(videoDuration.value) / 1.0))
            videoCompositions?.scaleTimeRange(CMTimeRangeMake(start: CMTime.zero, duration: videoDuration), toDuration: CMTimeMake(value: finalTimeScale, timescale: videoDuration.timescale))

            guard let exporter = AVAssetExportSession(asset: composition, presetName: AVAssetExportPresetHighestQuality) else { return }
            exporter.outputURL = path
            exporter.outputFileType = typeOutputTypeFile

            let downloadingView = self.showActivityIndicatoryTitle()
            downloadingView.0.text = "Downloading 0%"
            downloadingView.1.progress = 0

            let timerDownload = Timer.scheduledTimer(withTimeInterval: 0.1, repeats: true) { (timer) in
                downloadingView.1.progress = Float(exporter.progress)
                let progress = Int(exporter.progress * 100)
                downloadingView.0.text = "Downloading \(progress)%"
            }
            exporter.exportAsynchronously {
                switch exporter.status {
                case .completed:
                    print("xxx exported at \(path)")
                    timerDownload.invalidate()
                    DispatchQueue.main.async {
                        self.hideActivityIndicatory()
                        self.saveFileAudio(fileName)
                        self.hideActivityIndicatory()
                        self.showAlertDialog(title: "Save successfully", subtitle: "", cancelTitle: "Ok", cancelHandler: nil)
                        self.countMyTunes()
                    }
                    break
                case .failed:
                    print("failed \(exporter.error.debugDescription)")
                    self.downloadError()
                    timerDownload.invalidate()
                    break
                case .cancelled:
                    print("cancelled \(exporter.error.debugDescription)")
                    self.downloadError()
                    timerDownload.invalidate()
                    break
                default:
                    timerDownload.invalidate()
                    break
                }
            }
        }
    }
    
    fileprivate func saveFileAudio(_ nameFile : String) {
        do{
            let pathComponent = getSongPath(songName: nameFile)
            let attrs = try FileManager.default.attributesOfItem(atPath: pathComponent.path) as NSDictionary
            
            let fileStep = try loadDataJson(NAME_LIST_JSON) as Data
            var vendorArray = try JSONDecoder().decode([DataListJson].self, from: fileStep) as [DataListJson]
            let dataNew : DataListJson = DataListJson.init(pathComponent.path, nameFile, "\(attrs.fileSize())", attrs.fileCreationDate()!.description)
            vendorArray.append(dataNew)
            try savePropertyList(NAME_LIST_JSON, vendorArray)
        } catch{
            print("xxx error \(error)")
        }
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        print("xxx load done")
        //        isLoadDone = true
        clickDownload()
    }
    
    func clickDownload()
    {
        self.theWebView.evaluateJavaScript("document.documentElement.outerHTML.toString()", completionHandler: { (html: Any?, error: Error?) in
            DispatchQueue.main.async {
                guard let html = html as? String else
                {
                    print("Canot convert html STring")
                    self.downloadError()
                    return
                }
                
                self.requestDownload(htmlstring: html)
            }
        })
    }
    
    lazy var document: Document = Document.init("")
    
    func requestDownload(htmlstring : String){
        do
        {
            self.hideActivityIndicatory()
            document = try SwiftSoup.parse(htmlstring)
            let elements: Elements = try document.select("video")

            if elements.size() > 1
            {
                let alert = UIAlertController(title: "Select a file to download", message: "", preferredStyle: .alert)

                for element in elements
                {
                    let linkHref: String = String.init(htmlEncodedString: try element.attr("src"))!

                    if let videoUrl = URL(string: linkHref)
                    {
                        alert.addAction(UIAlertAction(title: "\(videoUrl.lastPathComponent)", style: .default, handler: { (str) in
                            
                            self.downloadFiles(url: linkHref, name: videoUrl.lastPathComponent, isLink: false)
                        }))
                    }
                }
                alert.addAction(UIAlertAction(title: "Cancel", style: .destructive, handler: {(str) in
                    self.hideActivityIndicatory()
                }))
                self.present(alert, animated: true, completion: nil)
            }
            else
            {
                guard let element: Element = elements.first()
                    else
                {
                    self.downloadError()
                    return
                }
                let linkHref: String = String.init(htmlEncodedString:try element.attr("src"))! // "http://example.com/"
                
                guard let videoUrl = URL(string: linkHref)
                    else
                {
                    print("xxx error updateVideo")
                    self.downloadError()
                    return
                }
  
                self.downloadFiles(url: linkHref, name: videoUrl.lastPathComponent, isLink: false)
            }


        }
        catch
        {
            print("xxx \(error)")
            self.downloadError()
        }
    }
    
    fileprivate func downloadFiles(url: String, name: String, isLink : Bool) {
        self.hideActivityIndicatory()
        let splitName = name.components(separatedBy: ".")
        showInputDialog(title: "File's name", subtitle: "", actionTitle: "Save", cancelTitle: "Cancel", text: splitName[0], inputPlaceholder: "", cancelHandler: nil) { (str) in
           
            guard let str = str,
                str != ""
                else
            {
                DispatchQueue.main.async {
                     print("xxx error str")
                    self.downloadError()
                }
                return
            }
            
            var typeFile = "mp3"
            if splitName.count <= 1
            {
               
                if self.urlString.contains("youtu")
                {
                    typeFile = "mp4"
                }
                else
                {
                    typeFile = "mp3"
                }
            }
            else
            {
                typeFile = splitName[1]
            }
            let fileName = "\(str).\(typeFile)"
            let path = getDocumentsDirectory().appendingPathComponent(fileName)
             
            if FileManager.default.fileExists(atPath: path.path)
            {
                DispatchQueue.main.async {
                    self.hideActivityIndicatory()
                    self.showAlertDialog(title: "Download failed", subtitle: "Your file is already existed", cancelTitle: "Ok", cancelHandler: nil)
                }
            }
            else
            {
                let downloadingView = self.showActivityIndicatoryTitle()
                downloadingView.0.text = "Downloading 0%"
                downloadingView.1.progress = 0
                Alamofire.request(url).downloadProgress(closure: { (progress) in
                    downloadingView.1.progress = Float(progress.fractionCompleted)
                    let progress = Int(progress.fractionCompleted * 100)
                    downloadingView.0.text = "Downloading \(progress)%"
                }).responseData{ (response) in
                    guard let data = response.result.value else{
                        
                        self.downloadError()
                        return
                    }
                    DispatchQueue.main.async {
                        do {
                            try data.write(to: path)
                            if isLink
                            {
                                let bombSoundEffect : AVAudioPlayer = try AVAudioPlayer(contentsOf: path)
                                bombSoundEffect.play()
                                bombSoundEffect.stop()
                            }
                            
                            self.hideActivityIndicatory()
                            self.saveFileAudio(fileName)
                            self.hideActivityIndicatory()
                            self.showAlertDialog(title: "Save successfully", subtitle: "", cancelTitle: "Ok", cancelHandler: nil)
                            self.countMyTunes()
                            
                        } catch {
                            if FileManager.default.fileExists(atPath: path.path)
                            {
                                try? FileManager.default.removeItem(atPath: path.path)
                            }
                            self.downloadError()
                            print("xxx \(error)")
                        }
                    }
                    
                }
            }
        }
    }
    
    func downloadError()
    {
        DispatchQueue.main.async {
            self.hideActivityIndicatory()
            self.showConfirmDialog(title: "Download failed", subtitle: "Your file is failed to download due to an error.", actionTitle: "Cancel", cancelTitle: "Try again!", cancelHandler: { (str) in
                self.showPasteURL()
            }, actionHandler: nil)
        }
    }
    
    func showPasteURL() -> Void {
        showInputDialog(title: "Paste URL", subtitle: "", actionTitle: "Download", cancelTitle: "Cancel", text: UIPasteboard.general.string ?? "", inputPlaceholder: "Only URLs end in .mp3 and .mp4", cancelHandler: nil) { (str) in
            
            DispatchQueue.global(qos: .background).async {
                DispatchQueue.main.async
                    {
                        self.showActivityIndicatoryTitle(title: "Downloading")
                        if let str = str,
                            str.contains("http"),
                            let url = URL(string: str)
                        {
                            if str.contains(".mp4") || str.contains(".mp3") || str.contains(".m4r")
                            {
                                self.downloadFiles(url: str, name: url.lastPathComponent, isLink: true)
                            }
                            else
                            {
                                
                                //                                self.theWebView.frame = self.view.frame
                                let request = URLRequest(url: url)
                                self.theWebView.load(request)
                                self.urlString = str
                            }
                        }
                        else
                        {
                            self.hideActivityIndicatory()
                            self.showAlertDialog(title: "URLs not allowed", subtitle: "Your URLs is not allowed. Please find a URL ends in .mp3 and .mp4 and try again!", cancelTitle: "OK", cancelHandler: nil)
                        }
                }
                
                
            }
        }
    }
    
    fileprivate func setBadgeValueForTabMyTunes(_ countMyTunesDownload: Int) {
        if let tabItems = tabBarController?.tabBar.items {
            // In this case we want to modify the badge number of the third tab:
            let tabItem = tabItems[2]
            tabItem.badgeValue = "\(countMyTunesDownload)"
        }
    }
    
    func countMyTunes()
    {
        guard var countMyTunesDownload = UserDefaults.standard.value(forKey: defaultsKeys.APP_MYTUNES) as? Int else {
            UserDefaults.standard.set(1, forKey: defaultsKeys.APP_MYTUNES)
            setBadgeValueForTabMyTunes(1)
            return
        }
        countMyTunesDownload += 1
        UserDefaults.standard.set(countMyTunesDownload, forKey: defaultsKeys.APP_MYTUNES)
        setBadgeValueForTabMyTunes(countMyTunesDownload)
    }
}

extension HomeViewController : UITableViewDelegate,UITableViewDataSource
{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return headerSectionTableView.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 3
        }
        else if section == 1{
            return 3
        }
        else {
            return 0
        }
        /*
        if section == 0 {
            return 2
        } else if section == 1 {
            return 3
        } else if section == 2 {
            return 1
        } else {
            return 3
        }
         */
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let row = indexPath.row
        let section = indexPath.section
        
        switch (section) {
        case 0:
//            let cell = Bundle.main.loadNibNamed("HomeViewTableViewCell", owner: self, options: nil)?.first as! HomeViewTableViewCell
            let cell = Bundle.main.loadNibNamed("HomeViewTableViewCell", owner: self, options: nil)?.last as! HomeCloudMusicTableViewCell
            cell.iconCloudTitle.text = importMusicOptions[row].title1
            cell.iconCloudTitle.font = UIFont(name: "SegoeUI", size: 15)
            cell.iconCloudImage.image = UIImage.init(named: importMusicOptions[row].image1)
            cell.iconCloudSubTitle.text = importMusicOptions[row].title2
            cell.logStatus.text = importMusicOptions[row].statusTitle
            cell.logStatus.isHidden = true
            cell.logIcon.isHidden = true
            cell.selectionStyle = .none
            return cell
        case 1:
            let cell = Bundle.main.loadNibNamed("HomeViewTableViewCell", owner: self, options: nil)?.last as! HomeCloudMusicTableViewCell
            cell.iconCloudTitle.text = cloudMusicOptions[row].title1
            cell.iconCloudTitle.font = UIFont(name: "SegoeUI", size: 15)
            cell.iconCloudImage.image = UIImage.init(named: cloudMusicOptions[row].image1)
            cell.iconCloudSubTitle.text = cloudMusicOptions[row].title2
            //            cell.logIcon.image = UIImage.init(named: cloudMusicOptions[row].image2)
            //            cell.logStatus.text = cloudMusicOptions[row].statusTitle
            //            cell.icon.layer.cornerRadius = 20
            
            // Check color of text base on log status
            //cell.logStatus.textColor = UIColor(hexString: "#F8AD37")
            
            if cloudMusicOptions[row].isLogin == true
            {
                cell.logStatus.isHidden = false
                cell.logIcon.isHidden = false
                //                cell.logStatus.text = "Log Out"
                //                cell.logStatus.textColor = UIColor(hexString: "#E5E5E5")
                //                cell.logIcon.image = UIImage(named: "import_iconlogout")
                cell.btnLogOut.tag = (row)
                cell.btnLogOut.addTarget(self, action: #selector(tabLogOut), for: .touchUpInside)
            }
            else
            {
                cell.logStatus.isHidden = true
                cell.logIcon.isHidden = true
            }
            
            cell.selectionStyle = .none
            
            return cell
        /*
        case 2:
            let cell = Bundle.main.loadNibNamed("HomeViewTableViewCell", owner: self, options: nil)?.last as! HomeCloudMusicTableViewCell
            cell.iconCloudTitle.text = libraryOption.title1
            cell.iconCloudImage.image = UIImage.init(named: libraryOption.image1)
            cell.iconCloudSubTitle.text = libraryOption.title2
            cell.logStatus.text = libraryOption.statusTitle
            cell.logStatus.isHidden = true
            cell.logIcon.isHidden = true
            cell.selectionStyle = .none
            
            return cell
        */
        default:
            let cell = Bundle.main.loadNibNamed("HomeViewTableViewCell", owner: self, options: nil)?.last as! HomeCloudMusicTableViewCell
            cell.logStatus.isHidden = true
            cell.logIcon.isHidden = true
            cell.selectionStyle = .none
            return cell
        }
    }
    
    @objc func tabLogOut(_ sender : UIButton)
    {
        let row = sender.tag
        LogOut(row)
        self.tableView.reloadData()
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let section = indexPath.section
        
        let row = indexPath.row
        
        switch section {
        case 0:
            if row == 0 {
                showPasteURL()
            }
            else if row == 1 {
                showTutorial("Tutorial")
            }
            else if row == 2 {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let tutvc = storyboard.instantiateViewController(withIdentifier: "LibraryViewController")
                self.navigationController?.pushViewController(tutvc, animated: true)
            }
            break
        case 1:
            if row == 0 {
                clickIconDropBox()
                isFirstToVC = true
                myIndex = 2
                
            }
            else if row == 1 {
                clickIconGoogleDrive()
                isFirstToVC = true
                myIndex = 1
                
            }
            else {
                clickIconOneDrive()
                isFirstToVC = true
                myIndex = 3
                
            }
            break
        /*
        case 2:
            //            showLibrary()
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let tutvc = storyboard.instantiateViewController(withIdentifier: "LibraryViewController")
            self.navigationController?.pushViewController(tutvc, animated: true)
            break
        */
        default:
            break
        }
        //        else
        //        {
        //            //
        //            openUrl("itms-apps://itunes.apple.com/app/id\(BodyBannerAll[row].appId)")
        //        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90


        /*
        let section = indexPath.section
        switch section {
        case 0:
            return 90
        case 1:
            return 80
        case 2:
            return 80
        default:
            return 70
        }
        */
    }
/*

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        guard let customHeaderView = CustomImportHeader.loadViewFromNib()
            else { return nil }
//        if section == 0 {
//            customHeaderView.isHidden = true
//        }
        if section  == 1 {
            customHeaderView.titleLabel.text = headerSectionTableView[section]
            customHeaderView.titleLabel.isHidden = true
            customHeaderView.titleLabel.textColor = UIColor.init(red: 0, green: 0, blue: 0)

            customHeaderView.titleLabel.font = UIFont(name:"SegoeUI-Bold", size: 25)

            //        customHeaderView.iconIamge.image = UIImage(named: headerSectionIconImage[section])

            customHeaderView.backgroundColor = UIColor(hexString: "#FFFFFF")
        }
        return customHeaderView
    }
 */
}


extension HomeViewController: GIDSignInDelegate {
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if let _ = error {
            service.authorizer = nil
        } else {
            service.authorizer = user.authentication.fetcherAuthorizer()
            //            let userId = user.userID                  // For client-side use only!
            //            let idToken = user.authentication.idToken // Safe to send to the server
            //            let fullName = user.profile.name
            //            let givenName = user.profile.givenName
            //            let familyName = user.profile.familyName
            
            // ...
            //            print("table View Controller : \(email)")
            if let email = user.profile.email {
                cloudMusicOptions[1].title2 = email
                cloudMusicOptions[1].isLogin = true
                // Do something
                self.tableView.reloadData()
            }
        }
    }
}
