//
//  GlobalParameter.swift
//  SafeNote
//
//  Created by Tuan Trinh on 1/28/19.
//  Copyright © 2019 POLYMATH, LLC. All rights reserved.
//

import UIKit
import MBProgressHUD


let info_disableLimitControl = "DISABLE_LIMIT_CONTROL"
let info_disableRestoreData = "DISABLE_RESTORE_DATA"
let info_disableFirebaseFunction = "DISABLE_FIREBASE_FUNCTION"
let info_disableDebug = "DISABLE_DEBUG"


class GlobalParameter: NSObject {
    
    var navigationTitleFont: UIFont = UIFont.boldSystemFont(ofSize: 17.0)
    var navigationItemFont: UIFont = UIFont.systemFont(ofSize: 15.0)
    
    var buttonSelectedColor: UIColor = UIColor.white
    var buttonColor: UIColor = UIColor.white
    
    var folderBackgroundColor: UIColor = UIColor.white
    var folderTintColor: UIColor = UIColor.white
    
    var pinFolderBackgroundColor: UIColor = UIColor.white
    var pinFolderTintColor: UIColor = UIColor.white
    
    var coverColor: UIColor = UIColor.black.withAlphaComponent(0.4)
    
    var reminderNoteColor: UIColor = UIColor.white
    var reminderPassedNoteColor: UIColor = UIColor.white
    
    var animationTime = 0.25
    var animationOptions: UIView.AnimationOptions = UIView.AnimationOptions(rawValue: 7)
    
    var sessionTime: TimeInterval = 15.0*60.0
    
    var disableLimitControl: Bool {
        if let value = Bundle.main.object(forInfoDictionaryKey: info_disableLimitControl) as? Bool {
            return value
        }
        return false
    }
    
    var disableRestoreData: Bool {
        if let value = Bundle.main.object(forInfoDictionaryKey: info_disableRestoreData) as? Bool {
            return value
        }
        return false
    }
    
    var disableFirebaseFunction: Bool {
        if let value = Bundle.main.object(forInfoDictionaryKey: info_disableFirebaseFunction) as? Bool {
            return value
        }
        return false
    }
    
    var disableDebug: Bool {
        if let value = Bundle.main.object(forInfoDictionaryKey: info_disableDebug) as? Bool {
            return value
        }
        return false
    }
    
    var currentHUD: MBProgressHUD? = nil
    
    static let sharedInstance = GlobalParameter()
    override init() {
        
    }
    
    class func shared() -> GlobalParameter {
        return sharedInstance
    }
    
}

private typealias UI = GlobalParameter
extension UI {

    class func font(_ size: CGFloat) -> UIFont {
        return GlobalParameter.shared().font(size, .regular)
    }
    
    class func fontWithBold(_ size: CGFloat) -> UIFont {
        return GlobalParameter.shared().font(size, .bold)
    }
    
    class func fontWithSemiBold(_ size: CGFloat) -> UIFont {
        return GlobalParameter.shared().font(size, .semibold)
    }
    
    class func fontWithMedium(_ size: CGFloat) -> UIFont {
        return GlobalParameter.shared().font(size, .medium)
    }
    
    class func font(_ size: CGFloat, _ weight: UIFont.Weight) -> UIFont {
        return GlobalParameter.shared().font(size, weight)
    }
    
    private func font(_ size: CGFloat, _ weight: UIFont.Weight) -> UIFont {
        return UIFont.systemFont(ofSize: size, weight: weight)
    }
    
    class func animation(withBlock animationBlock: @escaping (() -> Void), completion: (() -> Void)?) -> Void {
        return GlobalParameter.shared().animation(withBlock: animationBlock, completion: completion)
    }
    
    class func animation(time: Double, withBlock animationBlock: @escaping (() -> Void), completion: (() -> Void)?) -> Void {
        return GlobalParameter.shared().animation(time: time, withBlock: animationBlock, completion: completion)
    }
    
    private func animation(withBlock animationBlock: @escaping (() -> Void), completion: (() -> Void)?) -> Void {
        self.animation(time: self.animationTime, withBlock: animationBlock, completion: completion)
    }
    
    private func animation(time: Double, withBlock animationBlock: @escaping (() -> Void), completion: (() -> Void)?) -> Void {
        UIView.animate(withDuration: time, delay: 0.0, options: self.animationOptions, animations: animationBlock) { (finished) in
            if let completion = completion {
                completion()
            }
        }
    }
    
    class func attributedString(withContent content: String, fontSize: CGFloat, icon: String?, color: UIColor) -> NSAttributedString {
        let font = GlobalParameter.font(fontSize.rounded())
        let iconSize = CGSize.init(width: (font.lineHeight*0.8).rounded(), height: (font.lineHeight*0.8).rounded())
        
        let attributedString = NSMutableAttributedString.init()
        
        if let icon = icon {
            let iconAttachment = NSTextAttachment()
//            iconAttachment.image = UIImage(named: icon)?.colored(with: color, size: iconSize)
            iconAttachment.bounds = CGRect.init(origin: CGPoint.init(x: 0.0, y: (font.capHeight - iconSize.height).rounded()/2.0), size: iconSize)
            
            attributedString.append(NSAttributedString(attachment: iconAttachment))
            attributedString.append(NSAttributedString.init(string: "  "))
        }
        
        attributedString.append(NSAttributedString.init(string: String.init(format: "%@", content), attributes: [.foregroundColor:color, .font:font]))
        return attributedString
    }
    
    class func mediumAttributedString(withContent content: String, fontSize: CGFloat, icon: String?, color: UIColor) -> NSAttributedString {
        let font = GlobalParameter.fontWithMedium(fontSize.rounded())
        let iconSize = CGSize.init(width: (font.lineHeight*0.8).rounded(), height: (font.lineHeight*0.8).rounded())
        
        let attributedString = NSMutableAttributedString.init()
        
        if let icon = icon {
            let iconAttachment = NSTextAttachment()
//            iconAttachment.image = UIImage(named: icon)?.colored(with: color, size: iconSize)
            iconAttachment.bounds = CGRect.init(origin: CGPoint.init(x: 0.0, y: (font.capHeight - iconSize.height).rounded()/2.0), size: iconSize)
            
            attributedString.append(NSAttributedString(attachment: iconAttachment))
            attributedString.append(NSAttributedString.init(string: "  "))
        }
        
        attributedString.append(NSAttributedString.init(string: String.init(format: "%@", content), attributes: [.foregroundColor:color, .font:font]))
        return attributedString
    }
}

private typealias HUD = GlobalParameter
extension HUD {
    
    func hideHUD() -> Void {
        guard let hud = self.currentHUD else {
            return
        }
        
        Utils.dispatch_main_async {
            hud.hide(animated: true)
            self.currentHUD = nil
            
//            UIApplication.shared.isIdleTimerDisabled = true
        }
    }
    
    func showProgressHUD(inView view: UIView?) -> Void {
        self.showProgressHUD(title: nil, subTitle: nil, inView: view)
    }
    
    func showProgressHUD(title: String?, subTitle: String?, inView view: UIView?) -> Void {
        self.hideHUD()
        guard let view = view else { return }
        
        Utils.dispatch_main_async {
            let progressHUD = MBProgressHUD.showAdded(to: view, animated: true)
            progressHUD.mode = MBProgressHUDMode.indeterminate
            progressHUD.removeFromSuperViewOnHide = true
            
            progressHUD.label.text = title
            progressHUD.label.textColor = self.buttonSelectedColor
            
            progressHUD.detailsLabel.text = subTitle
            progressHUD.detailsLabel.textColor = self.buttonSelectedColor
            
//            UIApplication.shared.isIdleTimerDisabled = false
            self.currentHUD = progressHUD
        }
    }

    func showErrorHUD(inView view: UIView?) -> Void {
        self.showErrorHUD(title: "Error", subTitle: "error_retry".localized, inView: view)
    }
    
    func showErrorHUD(subTitle: String?, inView view: UIView?) -> Void {
        self.showErrorHUD(title: "Error", subTitle: subTitle, inView: view)
    }
    
    func showErrorHUD(title: String?, subTitle: String?, inView view: UIView?) -> Void {
        self.showHUD(title: title, subTitle: subTitle, icon: "icon-error", inView: view)
    }
    
    func showSuccessHUD(subTitle: String?, inView view: UIView?) -> Void {
        self.showSuccessHUD(title: "Error", subTitle: subTitle, inView: view)
    }
    
    func showSuccessHUD(title: String?, subTitle: String?, inView view: UIView?) -> Void {
        self.showHUD(title: title, subTitle: subTitle, icon: "icon-success", inView: view)
    }
    
    func showHUD(title: String?, subTitle: String?, icon: String?, inView view: UIView?) -> Void {
        self.hideHUD()
        guard let view = view else { return }
        
        Utils.dispatch_main_async {
            let hud = MBProgressHUD.init()
            hud.removeFromSuperViewOnHide = true
            hud.mode = MBProgressHUDMode.customView
            
            hud.label.text = title
            hud.detailsLabel.text = subTitle
            
            if let icon = icon {
                hud.customView = UIImageView.init(image: UIImage.init(named: icon))
            }
            
            view.addSubview(hud)
            
            hud.show(animated: true)
            hud.hide(animated: true, afterDelay: 2.0)
        }
    }
    
}
