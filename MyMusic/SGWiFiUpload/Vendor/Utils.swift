//
//  Utils.swift
//  SafeNote
//
//  Created by Tuan Trinh on 1/28/19.
//  Copyright © 2019 POLYMATH, LLC. All rights reserved.
//

import UIKit
import Reachability
import AudioToolbox

class Utils: NSObject {
    
    var reachability: Reachability?
    
    static let sharedInstance = Utils()
    override init() {
        
    }
    
    class func shared() -> Utils {
        return sharedInstance
    }
    
    deinit {
        self.stopNotifier()
    }
}

private typealias NetworUtils = Utils
extension NetworUtils {
    
    class func setupNetwork() -> Void {
        Utils.shared().setupReachability("google.com")
    }
    
    class var reachable: Bool {
        return (Utils.shared().reachability?.connection != Reachability.Connection.unavailable)
    }
    
    func stopNotifier() -> Void {
        self.reachability?.stopNotifier()
        self.reachability = nil
    }
    
    func setupReachability(_ hostName: String?) {
        let reachability: Reachability?
        
        if let hostName = hostName {
            reachability = try! Reachability(hostname: hostName)
        }
        else {
            reachability = try! Reachability()
        }
        
        if reachability == nil {
            return
        }
        
        reachability!.whenReachable = { reachability in
            print(reachability.connection)
        }
        
        reachability!.whenUnreachable = { reachability in
            print(reachability.connection)
        }
        
        do {
            try reachability!.startNotifier()
        } catch {
            Utils.errorLog(value: error)
        }
        
        self.reachability = reachability
    }
    
}

private typealias LogUtils = Utils
extension LogUtils {
    
    class func initLogger() -> Void {
    }
    
    class func debugInfo(value: Any) {
        print("INFO: " + "\(value)")
    }
    
    class func debugLog(value: Any) {
        print("LOG: " + "\(value)")
    }
    
    class func warningLog(value: Any) {
        print("WARNING: " + "\(value)")
    }
    
    class func errorLog(value: Any) {
        print("ERROR: " + "\(value)")
    }
    
}

private typealias UIUtils = Utils
extension UIUtils {
    
    class func configApplicationAppearance() {
        let color = GlobalParameter.shared().buttonSelectedColor
        let highlightColor = color.withAlphaComponent(0.6)
        
        let navigationbarAppearance = UINavigationBar.appearance()
        navigationbarAppearance.tintColor = color
        navigationbarAppearance.barTintColor = UIColor.white
        navigationbarAppearance.isTranslucent = false
        navigationbarAppearance.isOpaque = false
        navigationbarAppearance.titleTextAttributes = [NSAttributedString.Key.font:GlobalParameter.shared().navigationTitleFont,
                                                       NSAttributedString.Key.foregroundColor:color]
        
        let barButtonItemAppearance = UIBarButtonItem.appearance()
        barButtonItemAppearance.tintColor = UIColor.black
        barButtonItemAppearance.setTitleTextAttributes([NSAttributedString.Key.font:GlobalParameter.shared().navigationItemFont,
                                                        NSAttributedString.Key.foregroundColor:UIColor.black], for: UIControl.State.normal)
        barButtonItemAppearance.setTitleTextAttributes([NSAttributedString.Key.font:GlobalParameter.shared().navigationItemFont,
                                                        NSAttributedString.Key.foregroundColor:highlightColor], for: UIControl.State.highlighted)
        barButtonItemAppearance.setTitleTextAttributes([NSAttributedString.Key.font:GlobalParameter.shared().navigationItemFont,
                                                        NSAttributedString.Key.foregroundColor:highlightColor], for: UIControl.State.disabled)
    }
    
    class func alertView(title: String?, message: String?) -> UIAlertController {
        let alertView = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let cancelAction = UIAlertAction.init(title: "action_cancel".localized, style: .cancel, handler: nil)
        alertView.addAction(cancelAction)
        
        return alertView
    }
    
    class func actionSheet(title: String?, message: String?) -> UIAlertController {
        let alertView = UIAlertController(title: title, message: message, preferredStyle: .actionSheet)
        
        let cancelAction = UIAlertAction.init(title: "action_cancel".localized, style: .cancel, handler: nil)
        alertView.addAction(cancelAction)
        
        return alertView
    }
    
    class var appName: String {
        if let value = Bundle.main.object(forInfoDictionaryKey: "CFBundleDisplayName") as? String {
            return value
        }
        return "Dropnotes"
    }
    
    class var appBuildVersion: String {
        if let value = Bundle.main.object(forInfoDictionaryKey: "CFBundleVersion") as? String {
            return value
        }
        return "1.0"
    }
    
    class var appVersion: String {
        if let value = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as? String {
            return value
        }
        return "1.0"
    }
    
}

private typealias StringUtils = Utils
extension StringUtils {
    
    class func getStringSize(string: String, font: UIFont) -> CGSize {
        return Utils.getStringSize(string: string, font: font, maxWidth: CGFloat.greatestFiniteMagnitude)
    }
    
    class func getStringSize(string: String, font: UIFont, maxWidth: CGFloat) -> CGSize {
        let size = NSString(string: string).boundingRect(with: CGSize.init(width: maxWidth, height: CGFloat.greatestFiniteMagnitude), options: NSStringDrawingOptions(rawValue: NSStringDrawingOptions.usesFontLeading.rawValue|NSStringDrawingOptions.usesLineFragmentOrigin.rawValue) , attributes:[NSAttributedString.Key.font:font] , context: nil).size
        return CGSize.init(width: ceil(size.width), height: ceil(size.height))
    }
    
}

private typealias TimeUtils = Utils
extension TimeUtils {
    
    class var timestamp: Int64 {
        return Int64(time(nil))
    }
    
    class var timestampDesc: String {
        return Date().string(withFormat: "yyyyMMddHHmmss")
    }
    
}

private typealias System = Utils
extension System {
    
    class func playHapticPeek() {
        AudioServicesPlaySystemSound(1519)
    }
    
    class func playHapticPop() {
        AudioServicesPlaySystemSound(1520)
    }
    
    class func playHapticCancelled() {
        AudioServicesPlaySystemSound(1520)
    }
    
    class func playHapticTryAgain() {
        AudioServicesPlaySystemSound(1102)
    }
    
    class func playHapticFail() {
        AudioServicesPlaySystemSound(1107)
    }
    
    class var hasTopNotch: Bool {
        if #available(iOS 11.0,  *) {
            //return UIApplication.shared.delegate?.window??.safeAreaInsets.top ?? 0.0 > 20.0
        }
        
        return false
    }
    
    class var isIpad: Bool {
        return (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.pad)
    }
    
    class func dispatch_main_sync(block: (() -> Void)?) -> Void {
        if Thread.isMainThread {
            if let block = block {
                block()
            }
        }
        else {
            DispatchQueue.main.sync {
                if let block = block {
                    block()
                }
            }
        }
    }
    
    class func dispatch_main_async(block: (() -> Void)?) -> Void {
        if Thread.isMainThread {
            if let block = block {
                block()
            }
        }
        else {
            DispatchQueue.main.async {
                if let block = block {
                    block()
                }
            }
        }
    }
    
    class func doActionAfter(_ time: Double, completion: @escaping (() -> Void)) -> Void {
        DispatchQueue.main.asyncAfter(deadline: .now() + time) {
            completion()
        }
    }
    
    class func openUrl(urlPath: String?) -> Void {
        Utils.openUrl(urlPath: urlPath, completion: nil)
    }
    
    class func openUrl(urlPath: String?, completion: ((Bool) -> Void)?) -> Void {
        guard let path = urlPath else { return }
        guard let url = URL(string: path) else { return }
        
        //UIApplication.shared.open(url, options: [:], completionHandler: completion)
    }
    
}

private typealias MathUtils = Utils
extension MathUtils {
    
    class func deg2rad(_ number: Double) -> Double {
        return number * .pi / 180
    }
    
    class func rad2deg(_ number: Double) -> Double {
        return number * 180 / .pi
    }
    
}

private typealias FileUtils = Utils
extension FileUtils {
    
    class func cachePath() -> String {
        let cacheDirectoryURL =  try! FileManager.default.url(for: .cachesDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
        return cacheDirectoryURL.path.stringByAppendingPathComponent("Cache")
    }
    
    class func documentPath() -> String {
        let documentDirectoryURL =  try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
        return documentDirectoryURL.path
    }
    
    class func libraryPath() -> String {
        let documentDirectoryURL =  try! FileManager.default.url(for: .libraryDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
        return documentDirectoryURL.path
    }
    
    class func documentFilePath(with fileName: String) -> String {
        let filePath = self.documentPath().stringByAppendingPathComponent(fileName)
        return filePath
    }
    
    
    class func fileSize(file: Any) -> Int64 {
        if let filePath = String.init(from: file) {
            do {
                let attributes = try FileManager.default.attributesOfItem(atPath: filePath)
                let fileSize = attributes[FileAttributeKey.size] as! Int64
                return fileSize
            } catch {
                Utils.errorLog(value: error)
                return -1
            }
        }
        else {
            return -1
        }
    }
    
}

extension UIScreen {
    
    var isSmallScreen: Bool {
        let width = nativeBounds.width/self.scale
        return (width <= 320.0)
    }
    
}

extension UIAlertAction {
    
    
}

extension UIView {
    
    func asImage() -> UIImage? {
        return self.asImage(true)
    }
    
    func asImage(_ afterScreenUpdates: Bool) -> UIImage? {
        UIGraphicsBeginImageContextWithOptions(self.bounds.size, self.isOpaque, 0.0)
        self.drawHierarchy(in: self.bounds, afterScreenUpdates: afterScreenUpdates)
        let snapshotImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return snapshotImage
    }
    
}

protocol PropertyStoring {
    
    associatedtype T
    func getAssociatedObject(_ key: UnsafeRawPointer!, defaultValue: T) -> T
    
}

extension PropertyStoring {
    
    func getAssociatedObject(_ key: UnsafeRawPointer!, defaultValue: T) -> T {
        guard let value = objc_getAssociatedObject(self, key) as? T else {
            return defaultValue
        }
        return value
    }
    
}

extension Encodable {
    
    var dictionary: [String: Any]? {
        guard let data = try? JSONEncoder().encode(self) else { return nil }
        return (try? JSONSerialization.jsonObject(with: data, options: .allowFragments)).flatMap { $0 as? [String: Any] }
    }
    
}

