//
//  Date+Additions.swift
//  SafeNote
//
//  Created by Trinh Nhat Tuan on 3/19/19.
//  Copyright © 2019 POLYMATH, LLC. All rights reserved.
//

import Foundation

extension Date {
    
    public init(string: String, formatter: String) {
        let dateFormatter = DateFormatter.init()
        dateFormatter.dateFormat = formatter
        
        if let date = dateFormatter.date(from: string) {
            self = date
        }
        else {
            self = Date()
        }
    }
    
    func string(withFormat formatter: String) -> String {
        let dateFormatter = DateFormatter.init()
        dateFormatter.dateFormat = formatter
        
        return dateFormatter.string(from: self)
    }
    
    func dateAt(hour: Int, minute: Int) -> Date {
        var dateComponents = NSCalendar.current.dateComponents([.year, .month, .day, .hour, .minute, .second], from: self)
        dateComponents.hour = hour
        dateComponents.minute = minute
        dateComponents.second = 0
        
        if let date = NSCalendar.current.date(from: dateComponents) {
            return date
        }
        return self
    }
    
    func isInSameWeek(date: Date) -> Bool {
        return Calendar.current.isDate(self, equalTo: date, toGranularity: .weekOfYear)
    }
    func isInSameMonth(date: Date) -> Bool {
        return Calendar.current.isDate(self, equalTo: date, toGranularity: .month)
    }
    func isInSameYear(date: Date) -> Bool {
        return Calendar.current.isDate(self, equalTo: date, toGranularity: .year)
    }
    func isInSameDay(date: Date) -> Bool {
        return Calendar.current.isDate(self, equalTo: date, toGranularity: .day)
    }
    
    func isEqualTo(_ date: Date) -> Bool {
        return self == date
    }
    func isGreaterThan(_ date: Date) -> Bool {
        return self > date
    }
    func isSmallerThan(_ date: Date) -> Bool {
        return self < date
    }
    
    var isInThisWeek: Bool {
        return isInSameWeek(date: Date())
    }
    var isInToday: Bool {
        return Calendar.current.isDateInToday(self)
    }
    var isInYesterday: Bool {
        return Calendar.current.isDateInYesterday(self)
    }
    var isInTomorrow: Bool {
        return Calendar.current.isDateInTomorrow(self)
    }
    
    var isInTheFuture: Bool {
        return Date() < self
    }
    var isInThePast: Bool {
        return self < Date()
    }
    
    var startOfDay: Date {
        return Calendar.current.startOfDay(for: self)
    }
    var endOfDay: Date {
        var components = DateComponents()
        components.day = 1
        components.second = -1
        return Calendar.current.date(byAdding: components, to: startOfDay)!
    }
    var startOfMonth: Date {
        let components = Calendar.current.dateComponents([.year, .month], from: startOfDay)
        return Calendar.current.date(from: components)!
    }
    var endOfMonth: Date {
        var components = DateComponents()
        components.month = 1
        components.second = -1
        return Calendar.current.date(byAdding: components, to: startOfMonth)!
    }
    
    func addingHour(_ hour: TimeInterval) -> Date {
        return self.addingMinute(hour * 60.0)
    }
    
    func addingMinute(_ minute: TimeInterval) -> Date {
        return self.addingTimeInterval(minute * 60)
    }
    
}
