//
//  String+Additions.swift
//  SafeNote
//
//  Created by Trinh Nhat Tuan on 3/19/19.
//  Copyright © 2019 POLYMATH, LLC. All rights reserved.
//

import UIKit
import CommonCrypto

extension String {
    
    init?(from file: Any) {
        if file is String {
            self = file as! String
        }
        else if file is URL {
            self = (file as! URL).path
        }
        else {
            return nil
        }
    }
    
    var lastPathComponent: String {
        return (self as NSString).lastPathComponent
    }
    
    func deletingPathExtension() -> String {
        return (self as NSString).deletingPathExtension
    }
    
    func deletingLastPathComponent() -> String {
        return (self as NSString).deletingLastPathComponent
    }
    
    func stringByAppendingPathComponent(_ component: String) -> String {
        return (self as NSString).appendingPathComponent(component)
    }
    
    func stringByAppendingPathExtension(_ fileExtension: String) -> String {
        if let path = (self as NSString).appendingPathExtension(fileExtension) {
            return path
        }
        else {
            return self + "." + fileExtension
        }
    }
    
    var withoutSpecialCharacters: String {
        return self.components(separatedBy: CharacterSet.symbols).joined(separator: "")
    }
    
    func trim() -> String {
        return self.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
    }
    
    func stripHTML() -> String {
        var plainText = self
        plainText = plainText.replacingOccurrences(of: "<[^>]+>", with: "", options: String.CompareOptions.regularExpression, range: nil)
        return plainText
    }
    
    func nsRange(from range: Range<Index>) -> NSRange {
        return NSRange(range, in: self)
    }
    
    func substring(from range: NSRange) -> String? {
        if let range = Range(range, in: self) {
            return String(self[range])
        }
        return nil
    }
    
    func linkDetector() -> [String] {
        let detector = try! NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue)
        let matches = detector.matches(in: self, options: [], range: NSRange(location: 0, length: self.utf16.count))
        
        var links = [String]()
        
        for match in matches {
            guard let range = Range(match.range, in: self) else { continue }
            let url = self[range]
            links.append(String(url))
        }
        
        return links
    }
    
    var CGFloatValue: CGFloat? {
        guard let doubleValue = Double(self) else {
            return nil
        }
        return CGFloat(doubleValue)
    }
    
    var MD5: String {
        let length = Int(CC_MD5_DIGEST_LENGTH)
        var digest = [UInt8](repeating: 0, count: length)
        
        if let d = self.data(using: String.Encoding.utf8) {
            _ = d.withUnsafeBytes { (body: UnsafePointer<UInt8>) in
                CC_MD5(body, CC_LONG(d.count), &digest)
            }
        }
        
        return (0..<length).reduce("") {
            $0 + String(format: "%02x", digest[$1])
        }
    }
    
    static func random(length: Int = 20) -> String {
        let base = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        var randomString: String = ""
        
        for _ in 0..<length {
            let randomValue = arc4random_uniform(UInt32(base.count))
            randomString += "\(base[base.index(base.startIndex, offsetBy: Int(randomValue))])"
        }
        return randomString
    }
    
    public init(timeFormatted time: Float) {
        let seconds = Int(time)%60
        let minutes = (Int(time)/60)%60
        let hours = Int(time)/3600
        
        if hours > 0 {
            self.init(format: "%02d:%02d:%02d", hours, minutes, seconds)
        }
        else {
            self.init(format: "%02d:%02d", minutes, seconds)
        }
    }
    
    func removeCharsFromEnd(count: Int) -> String {
        return (self as NSString).substring(to: self.count - count)
    }
    
    
    var localized: String {
        if let languageBundle = Language.shared().languageBundle {
            let string = self.localized(bundle: languageBundle, tableName: "Localizable")
            if string != self {
                return string
            }
        }
       return NSLocalizedString(self, comment: "")
    }
    
    func localized(bundle: Bundle, tableName: String) -> String {
        return bundle.localizedString(forKey: self, value: nil, table: tableName)
    }
}

class Language: NSObject {

    let languageFolder: String
    var languageBundle: Bundle? = nil
    
    static let sharedInstance = Language()
    override init() {
        self.languageFolder = Utils.libraryPath().stringByAppendingPathComponent("Localizable")
        super.init()
        
    }
    
    class func shared() -> Language {
        return sharedInstance
    }
    
}
