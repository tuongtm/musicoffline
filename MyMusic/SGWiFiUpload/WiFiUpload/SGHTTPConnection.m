//
//  SGWiFiUploadManager.m
//  SGWiFiUpload
//
//  Created by soulghost on 29/6/2016.
//  Copyright © 2016 soulghost. All rights reserved.
//

#import "SGHTTPConnection.h"
#import "HTTPMessage.h"
#import "HTTPDataResponse.h"
#import "DDNumber.h"
#import "HTTPLogging.h"

#import "MultipartFormDataParser.h"
#import "MultipartMessageHeaderField.h"
#import "HTTPDynamicFileResponse.h"
#import "HTTPFileResponse.h"
#import "SGConst.h"
#import <UIKit/UIKit.h>
#import "SGWiFiUploadManager.h"

// Log levels : off, error, warn, info, verbose
// Other flags: trace
static const int httpLogLevel = HTTP_LOG_LEVEL_VERBOSE; // | HTTP_LOG_FLAG_TRACE;

/**
 * All we have to do is override appropriate methods in HTTPConnection.
 **/

@interface SGHTTPConnection ()

@property (nonatomic, assign) UInt64 contentLength;
@property (nonatomic, assign) UInt64 currentLength;

@end

@implementation SGHTTPConnection

- (BOOL)supportsMethod:(NSString *)method atPath:(NSString *)path
{
	HTTPLogTrace();
	
	// Add support for POST
   
	if ([method isEqualToString:@"POST"])
	{
		if ([path isEqualToString:@"/upload.json"])
		{
			return YES;
		}
	}
	
	return [super supportsMethod:method atPath:path];
}

- (BOOL)expectsRequestBodyFromMethod:(NSString *)method atPath:(NSString *)path
{
	HTTPLogTrace();
	
	// Inform HTTP server that we expect a body to accompany a POST request
	
	if([method isEqualToString:@"POST"] && [path isEqualToString:@"/upload.json"]) {
        // here we need to make sure, boundary is set in header
        NSString* contentType = [request headerField:@"Content-Type"];
        NSUInteger paramsSeparator = [contentType rangeOfString:@";"].location;
        if( NSNotFound == paramsSeparator ) {
            return NO;
        }
        if( paramsSeparator >= contentType.length - 1 ) {
            return NO;
        }
        NSString* type = [contentType substringToIndex:paramsSeparator];
        if( ![type isEqualToString:@"multipart/form-data"] ) {
            // we expect multipart/form-data content type
            return NO;
        }

		// enumerate all params in content-type, and find boundary there
        NSArray* params = [[contentType substringFromIndex:paramsSeparator + 1] componentsSeparatedByString:@";"];
        for( NSString* param in params ) {
            paramsSeparator = [param rangeOfString:@"="].location;
            if( (NSNotFound == paramsSeparator) || paramsSeparator >= param.length - 1 ) {
                continue;
            }
            NSString* paramName = [param substringWithRange:NSMakeRange(1, paramsSeparator-1)];
            NSString* paramValue = [param substringFromIndex:paramsSeparator+1];
            
            if( [paramName isEqualToString: @"boundary"] ) {
                // let's separate the boundary from content-type, to make it more handy to handle
                [request setHeaderField:@"boundary" value:paramValue];
            }
        }
        // check if boundary specified
        if( nil == [request headerField:@"boundary"] )  {
            return NO;
        }
        return YES;
    }
	return [super expectsRequestBodyFromMethod:method atPath:path];
}

- (NSObject<HTTPResponse> *)httpResponseForMethod:(NSString *)method URI:(NSString *)path
{
	HTTPLogTrace();
    
    if ([method isEqualToString:@"GET"] && [path hasPrefix:@"/delete.json"])
    {
       NSString *fileName = [path componentsSeparatedByString:@"="][1];
       NSString *documents = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
       NSString *filePath = [documents stringByAppendingPathComponent:@"list.json"];
       
        NSError *error;
        NSData *jsonData = [NSData dataWithContentsOfFile:filePath];
        
        if (jsonData == nil) {
            NSLog(@"jsonData %@", jsonData);
            [jsonData writeToFile:filePath atomically:YES];
            // handle error ...
        }
        
        NSArray *array = [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:&error];
        NSMutableArray *jsonArray = [NSMutableArray arrayWithArray:array];
        if (jsonArray == nil) {
            // handle error ...
            NSLog(@"error");
        }else{
            for(id item in jsonArray) {
                if([item[@"name"] isEqual:fileName]) {
                    [jsonArray removeObject:item];
                    break;
                }
                NSLog(@"item : %@", item);
            }
            array = [jsonArray copy];
            jsonData = [NSJSONSerialization dataWithJSONObject:array options:NSJSONWritingPrettyPrinted error:&error];
            NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
            [[jsonString dataUsingEncoding:NSUTF8StringEncoding] writeToFile:filePath atomically:NO];
        }
    }
	
	if ([method isEqualToString:@"POST"] && [path isEqualToString:@"/upload.json"])
	{

		// this method will generate response with links to uploaded file
		NSMutableString* filesStr = [[NSMutableString alloc] init];
		for(NSString* filePath in uploadedFiles ) {
			//generate links
			[filesStr appendFormat:@"<a href=\"%@\"> %@ </a><br/>",filePath, [filePath lastPathComponent]];
		}
        
        NSString* templatePath = [[config documentRoot] stringByAppendingPathComponent:@"list.json"];
        NSDictionary* replacementDict = [NSDictionary dictionaryWithObject:filesStr forKey:@"list.json"];
        
        //xxxxxx
        NSString *documents = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        NSString *filePath = [documents stringByAppendingPathComponent:@"list.json"];
        uploadedFilesPath = [documents stringByAppendingPathComponent:uploadedFilesName];
        if(![uploadedFilesPath isEqualToString: @""])
        {
            NSError *error;
            NSData *jsonData = [NSData dataWithContentsOfFile:filePath];
            
            if (jsonData == nil) {
                 NSLog(@"jsonData %@", jsonData);
                 [jsonData writeToFile:filePath atomically:YES];
                // handle error ...
            }
            
            NSArray *array = [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:&error];
            NSMutableArray *jsonArray = [NSMutableArray arrayWithArray:array];
            if (jsonArray == nil) {
                // handle error ...
                NSLog(@"error");
            }else{
                NSDictionary *dic = @{ @"path" : uploadedFilesPath, @"name" : uploadedFilesName, @"size": uploadedFilesLength, @"createdate" : uploadedFileCreateDate};
                [jsonArray addObject:dic];
                array = [jsonArray copy];
    //            [dic setValue:[NSString stringWithFormat:@"%@", self.contentPath] forKey:@"path"];
    //            [dic setValue:[NSString stringWithFormat:@"%@", self.contentName] forKey:@""];
    //            [dic setValue:[NSString stringWithFormat:@"%llu", self.contentLength] forKey:@"size"];
                jsonData = [NSJSONSerialization dataWithJSONObject:array options:NSJSONWritingPrettyPrinted error:&error];
                NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
                [[jsonString dataUsingEncoding:NSUTF8StringEncoding] writeToFile:filePath atomically:NO];
//                if ([[NSFileManager defaultManager] isReadableFileAtPath:uploadedFilesPath])
//                    [[NSFileManager defaultManager] moveItemAtPath:uploadedFilesPath toPath:documents error:nil];
            }
            
        }
		
		// use dynamic file response to apply our links to response template
		return [[HTTPDynamicFileResponse alloc] initWithFilePath:templatePath forConnection:self separator:@"%" replacementDictionary:replacementDict];
	}
    
    
	if( [method isEqualToString:@"GET"] && [path hasPrefix:@"/list.json"] ) {
		// let download the uploaded files
        NSString *documents = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        NSString *filePath = [documents stringByAppendingPathComponent:@"list.json"];
        // The main act...
//        [[aString dataUsingEncoding:NSUTF8StringEncoding] writeToFile:fileAtPath atomically:NO];
//        NSLog(@"aaa: %@ " , filePath);
        return [[HTTPFileResponse alloc] initWithFilePath: filePath forConnection:self];
	}
	
	return [super httpResponseForMethod:method URI:path];
}

- (void)prepareForBodyWithSize:(UInt64)contentLength
{
	HTTPLogTrace();
    uploadedFilesName = @"";
    uploadedFilesPath = @"";
    uploadedFilesLength = @"";
    uploadedFileCreateDate = @"";
    self.contentLength = contentLength;
    self.currentLength = 0;
	// set up mime parser
    NSString* boundary = [request headerField:@"boundary"];
    parser = [[MultipartFormDataParser alloc] initWithBoundary:boundary formEncoding:NSUTF8StringEncoding];
    parser.delegate = self;

	uploadedFiles = [[NSMutableArray alloc] init];
}

- (void)processBodyData:(NSData *)postDataChunk
{
	HTTPLogTrace();
    // append data to the parser. It will invoke callbacks to let us handle
    // parsed data.
    [parser appendData:postDataChunk];
}


//-----------------------------------------------------------------
#pragma mark multipart form data parser delegate
- (void) processStartOfPartWithHeader:(MultipartMessageHeader*) header {
    
	// in this sample, we are not interested in parts, other then file parts.
	// check content disposition to find out filename

    MultipartMessageHeaderField* disposition = [header.fields objectForKey:@"Content-Disposition"];
	NSString* filename = [[disposition.params objectForKey:@"filename"] lastPathComponent];

    if ( (nil == filename) || [filename isEqualToString: @""] ) {
        // it's either not a file part, or
		// an empty form sent. we won't handle it.
		return;
	}
    // 这里用于发出文件开始上传的通知
    dispatch_async(dispatch_get_main_queue(), ^{
        [[NSNotificationCenter defaultCenter] postNotificationName:SGFileUploadDidStartNotification object:@{@"fileName" : filename ?: @"File"}];
    });
    // 这里用于设置文件的保存路径，先预存一个空文件，然后进行追加写内容
    NSString *uploadDirPath = [SGWiFiUploadManager sharedManager].savePath;
	BOOL isDir = YES;
	if (![[NSFileManager defaultManager]fileExistsAtPath:uploadDirPath isDirectory:&isDir ]) {
		[[NSFileManager defaultManager]createDirectoryAtPath:uploadDirPath withIntermediateDirectories:YES attributes:nil error:nil];
	}
	
    NSString* filePath = [uploadDirPath stringByAppendingPathComponent: filename];
    if( [[NSFileManager defaultManager] fileExistsAtPath:filePath] ) {
        storeFile = nil;
    }
    else {
		HTTPLogVerbose(@"Saving file to %@", filePath);
		if(![[NSFileManager defaultManager] createDirectoryAtPath:uploadDirPath withIntermediateDirectories:true attributes:nil error:nil]) {
			HTTPLogError(@"Could not create directory at path: %@", filePath);
		}
		if(![[NSFileManager defaultManager] createFileAtPath:filePath contents:nil attributes:nil]) {
			HTTPLogError(@"Could not create file at path: %@", filePath);
		}
		storeFile = [NSFileHandle fileHandleForWritingAtPath:filePath];
		[uploadedFiles addObject: [NSString stringWithFormat:@"/upload/%@", filename]];
        uploadedFilesPath = filePath;
        uploadedFilesName = filename;

        if ([[NSFileManager defaultManager] attributesOfItemAtPath:filePath error:nil] != nil) {
            NSDate *date = (NSDate*)[[[NSFileManager defaultManager] attributesOfItemAtPath:filePath error:nil] objectForKey: NSFileCreationDate];
            uploadedFileCreateDate = [date description];
        }
        else {
            NSLog(@"Not found");
        }
        uploadedFilesLength = [NSString stringWithFormat:@"%llu", self.contentLength];
    }
}


- (void) processContent:(NSData*) data WithHeader:(MultipartMessageHeader*) header 
{
	// here we just write the output from parser to the file.
    // 由于除去文件内容外，还有HTML内容和空文件通过此方法处理，因此需要过滤掉HTML和空文件内容
    if (!header.fields[@"Content-Disposition"]) {
        return;
    } else {
        MultipartMessageHeaderField *field = header.fields[@"Content-Disposition"];
        NSString *fileName = field.params[@"filename"];
        if (fileName.length == 0) return;
    }
    self.currentLength += data.length;
    CGFloat progress;
    if (self.contentLength == 0) {
        progress = 1.0f;
    } else {
        progress = (CGFloat)self.currentLength / self.contentLength;
    }
    dispatch_async(dispatch_get_main_queue(), ^{
       [[NSNotificationCenter defaultCenter] postNotificationName:SGFileUploadProgressNotification object:@{@"progress" : @(progress)}]; 
    });
	if (storeFile) {
		[storeFile writeData:data];
	}
}

- (void) processEndOfPartWithHeader:(MultipartMessageHeader*) header
{
    // as the file part is over, we close the file.
    // 由于除去文件内容外，还有HTML内容和空文件通过此方法处理，因此需要过滤掉HTML和空文件内容
    if (!header.fields[@"Content-Disposition"]) {
        return;
    } else {
        MultipartMessageHeaderField *field = header.fields[@"Content-Disposition"];
        NSString *fileName = field.params[@"filename"];
        if (fileName.length == 0) return;
    }
	[storeFile closeFile];
	storeFile = nil;
    dispatch_async(dispatch_get_main_queue(), ^{
        [[NSNotificationCenter defaultCenter] postNotificationName:SGFileUploadDidEndNotification object:nil];
    });
}

- (void) processPreambleData:(NSData*) data
{
    // if we are interested in preamble data, we could process it here
}

- (void) processEpilogueData:(NSData*) data 
{
    // if we are interested in epilogue data, we could process it here.
}

@end
