//
//  CustomTabbarViewController.swift
//  Tubidy
//
//  Created by NhonGa on 27/10/2018.
//  Copyright © 2018 ThreeWolves. All rights reserved.
//

import UIKit
import GoogleMobileAds

class CustomTabbarViewController: UITabBarController, GADInterstitialDelegate {
    static var shared : CustomTabbarViewController? = nil
    var countAds : Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        CustomTabbarViewController.shared = self
        // Do any additional setup after loading the view.
//        let gradient = CAGradientLayer()
////        bounds.size.height += UIApplication.shared.statusBarFrame.size.height
//        gradient.frame = tabBar.bounds
//        //            gradient.colors = [UIColor.red.cgColor, UIColor.blue.cgColor]
//        
//        let firstColor : UIColor = UIColor(red: 56/255, green: 39/255, blue: 180/255, alpha: 1.0)
//        let secondColor : UIColor = UIColor(red: 108/255, green: 24/255, blue: 164/255, alpha: 1.0)
//        gradient.colors = [firstColor.cgColor ,secondColor.cgColor]
//        
//        gradient.startPoint = CGPoint(x: 0, y: 0)
//        gradient.endPoint = CGPoint(x: 1, y: 0)
//        
//        if let image = getImageFrom(gradientLayer: gradient) {
//            tabBar.backgroundImage = image
//        }
        
        createAndLoadInterstitial()
    }
    
    func getImageFrom(gradientLayer:CAGradientLayer) -> UIImage? {
        var gradientImage:UIImage?
        UIGraphicsBeginImageContext(gradientLayer.frame.size)
        if let context = UIGraphicsGetCurrentContext() {
            gradientLayer.render(in: context)
            gradientImage = UIGraphicsGetImageFromCurrentImageContext()?.resizableImage(withCapInsets: UIEdgeInsets.zero, resizingMode: .stretch)
        }
        UIGraphicsEndImageContext()
        return gradientImage
    }
    
    func closeAdsFull()
    {
         NotificationCenter.default.removeObserver(self, name: .CLICK_TABBAR, object: nil)
    }
    
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        NotificationCenter.default.addObserver(self, selector: #selector(ShowAdsFull), name: .CLICK_TABBAR, object: nil)
        
        if(fullAds != nil)
        {
            fullAds.delegate = self
        }
        else
        {
            createAndLoadInterstitial()
        }
        
        countAds+=1
        if(countAds > 4){
            countAds = 0
            showActivityIndicatoryCountDown(isRV: false, pos: .CLICK_TABBAR) { (str) in
                if str == "removeads"
                {
                    closeAdsFull()
                }
            }
        }else{
            closeAdsFull()
        }
    }
    
    @objc func ShowAdsFull()
    {
        DispatchQueue.main.async
        {
            if !showAdsInterstitial(self)
            {
                self.closeAdsFull()
            }
        }
        
    }
    
    func interstitialDidDismissScreen(_ ad: GADInterstitial) {
        closeAdsFull()
        createAndLoadInterstitial()
    }
    
    override func tabBar(_ tabBar: UITabBar, willBeginCustomizing items: [UITabBarItem]) {
       
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
