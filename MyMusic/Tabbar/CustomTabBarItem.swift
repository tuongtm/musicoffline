//
//  CustomTabBarItem.swift
//  Music1
//
//  Created by NhonGa on 11/05/2019.
//  Copyright © 2019 ThreeWolves. All rights reserved.
//

import UIKit

class CustomTabBarItem: UITabBarItem {
    override func awakeFromNib() {
        if let image = image
        {
            self.image = image.withRenderingMode(.alwaysOriginal)
        }
        if let image = selectedImage
        {
            selectedImage = image.withRenderingMode(.alwaysOriginal)
        }
    }
}
