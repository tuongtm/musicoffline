//
//  AppDelegate.swift
//  Offline Music
//
//  Created by NhonGa on 12/05/2019.
//  Copyright © 2019 Free App & Game. All rights reserved.
//

import GoogleMobileAds
import UIKit
import GoogleSignIn
import SwiftyDropbox
import OneDriveSDK

let NAME_LIST_JSON = "list.json"
struct DataListJson : Codable {
    let path : String
    let name: String
    let size : String
    let createdate : String
    
    init(_ pathX: String, _ nameX: String, _ sizeX: String, _ createDateX: String) {
        path = pathX
        name = nameX
        size = sizeX
        createdate = createDateX
    }
}

let NAME_PLAYLIST_JSON = "playlist.json"
struct DataPlayListJson : Codable {
    var name : String
    var listSong : [DataListJson]
    
    init(_ nameX: String, _ listSongX: [DataListJson]) {
        name = nameX
        listSong = listSongX
    }
}

struct defaultsKeys {
    static let KEY_FIRST_OPEN_APP = "KEY_FIRST_OPEN_APP"
    static let APP_OPENED_COUNT = "APP_OPENED_COUNT"
    static let APP_REMOVE_ADS = "APP_REMOVE_ADS"
    static let APP_FIRST_OPEN_LIBRARY = "APP_FIRST_OPEN_LIBRARY"
    static let APP_REVIEW_TRANSFER = "APP_REVIEW_TRANSFER"
    static let APP_REVIEW_DROPBOX = "APP_REVIEW_DROPBOX"
    static let APP_REVIEW_GGDRIVE = "APP_REVIEW_GGDRIVE"
    static let APP_REVIEW_ONEDRIVE = "APP_REVIEW_ONEDRIVE"
    static let APP_REVIEW_LIBRARY = "APP_REVIEW_LIBRARY"
    static let APP_SLEEP_TIMER = "APP_SLEEP_TIMER"
    static let APP_MYTUNES = "APP_MYTUNES"
    static let APP_HOME_FIRST_SHOW_ADS_FULL = "APP_HOME_FIRST_SHOW_ADS_FULL"
    static let APP_RATING = "APP_RATING"
    static let APP_DOWNLOAD_MUSIC = "APP_DOWNLOAD_MUSIC"
}
struct Constants {
    static let BANNER_ID = "ca-app-pub-1407459637292918/9160837163"
    static let FULL_ID = "ca-app-pub-1407459637292918/9368720726"
    static let VIDEO_ID = "ca-app-pub-1407459637292918/2323040039"
    static let NATIVE_ID = "ca-app-pub-1407459637292918/6837193636"
    static let APP_ID = "ca-app-pub-1407459637292918~4592898586"

//    static let BANNER_ID = "ca-app-pub-3940256099942544/2934735716"
//    static let FULL_ID = "ca-app-pub-3940256099942544/4411468910"
//    static let VIDEO_ID = "ca-app-pub-3940256099942544/5224354917"
//    static let NATIVE_ID = "ca-app-pub-3940256099942544/3986624511"
//    static let APP_ID = "ca-app-pub-3940256099942544~1458002511"
}

var statusBarHeight : CGFloat = 0
var bottomPadding : CGFloat = 0

//Dropbox ID
let fullDropboxAppKey = "c18x53wp4ewyhew";
let fullDropboxAppSecret = "hjo5xpluuopvpgm";

//GoogleDrive ID
let clientIdGD = "684424540985-d9su4dv6v31at38impmbnrh0uqjd2mer.apps.googleusercontent.com"


//OneDrive ID
let odID = "de79eaeb-c475-47e4-9970-a170504a4f98"

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    let appBundleId : String = Bundle.main.bundleIdentifier!
    var window: UIWindow?
    let gcmMessageIDKey = "gcm.message_id"
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Use Firebase library to configure APIs.
        IAPHelper.shared().requestSubscribeOptions(completion: nil)
        GIDSignIn.sharedInstance().clientID = clientIdGD
        
        ODClient.setMicrosoftAccountAppId(odID , scopes: ["onedrive.readwrite,offline_access,wl.emails,wl.signin"])
        
        
        DropboxClientsManager.setupWithAppKey(fullDropboxAppKey)
        
        GADMobileAds.sharedInstance().start(completionHandler: nil)
        
        return true
    }
    
    var orientationLock = UIInterfaceOrientationMask.all

    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
            return self.orientationLock
    }
    
    fileprivate func checkTypeDevice() {
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 1136:
                print("iPhone 5 or 5S or 5C")
            case 1334:
                print("iPhone 6/6S/7/8")
            case 1920, 2208:
                print("iPhone 6+/6S+/7+/8+")
            case 2436:
                statusBarHeight = UIApplication.shared.statusBarFrame.height
                print("iPhone X, Xs")
            case 2688:
                statusBarHeight = UIApplication.shared.statusBarFrame.height
                print("iPhone Xs Max")
            case 1792:
                statusBarHeight = UIApplication.shared.statusBarFrame.height
                print("iPhone Xr")
            default:
                print("unknown")
            }
        }
    }
    
    func applicationDidReceiveMemoryWarning(_ application: UIApplication) {
        URLCache.shared.removeAllCachedResponses()
    }
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        return GIDSignIn.sharedInstance().handle(url)
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        if let authResult = DropboxClientsManager.handleRedirectURL(url) {
            switch authResult {
            case .success:
                print("Success! User is logged into DropboxClientsManager.")
            case .cancel:
                print("Authorization flow was manually canceled by user!")
            case .error(_, let description):
                print("Error: \(description)")
            }
        }
        
//        let sourceApplication = options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String
//        let annotation = options[UIApplication.OpenURLOptionsKey.annotation]
        return GIDSignIn.sharedInstance().handle(url)
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        self.window?.rootViewController?.beginAppearanceTransition(true, animated: false)
        self.window?.rootViewController?.endAppearanceTransition()
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        UserDefaults.standard.removeObject(forKey: defaultsKeys.APP_SLEEP_TIMER)
    }
}

struct AppUtility {

    static func lockOrientation(_ orientation: UIInterfaceOrientationMask) {

        if let delegate = UIApplication.shared.delegate as? AppDelegate {
            delegate.orientationLock = orientation
        }
    }

    /// OPTIONAL Added method to adjust lock and rotate to the desired orientation
    static func lockOrientation(_ orientation: UIInterfaceOrientationMask, andRotateTo rotateOrientation:UIInterfaceOrientation) {

        self.lockOrientation(orientation)

        UIDevice.current.setValue(rotateOrientation.rawValue, forKey: "orientation")
        UINavigationController.attemptRotationToDeviceOrientation()
    }

}
