//
//  AudioPlayerViewController.swift
//  Music Player
//
//  Created by NhonGa on 05/08/2018.
//  Copyright © 2018 NhonGa. All rights reserved.
//

import UIKit
import AVFoundation
import MediaPlayer
import GoogleMobileAds

extension MPVolumeView {
    var volumeSliderXX:UISlider {
        self.showsRouteButton = false
        self.showsVolumeSlider = false
        self.isHidden = true
        var slider = UISlider()
        for subview in self.subviews {
            if subview.isKind(of: UISlider.self){
                slider = subview as! UISlider
                slider.isContinuous = false
                (subview as! UISlider).value = AVAudioSession.sharedInstance().outputVolume
                return slider
            }
        }
        return slider
    }
    
    static func setVolume(_ volume: Float) {
        let volumeView = MPVolumeView()
        let slider = volumeView.subviews.first(where: { $0 is UISlider }) as? UISlider
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.01) {
            slider?.value = volume
        }
    }
}

var isRotation : Bool = false

extension UIView {
    private static let kRotationAnimationKey = "rotationanimationkey"
    func rotate(duration: Double = 8) {
        if isRotation{
            resumeAnimations()
        }else{
            if layer.animation(forKey: UIView.kRotationAnimationKey) == nil {
                let rotationAnimation = CABasicAnimation(keyPath: "transform.rotation")
                
                rotationAnimation.fromValue = 0.0
                rotationAnimation.toValue = Float.pi * 2.0
                rotationAnimation.duration = duration
                rotationAnimation.repeatCount = Float.infinity
                
                layer.add(rotationAnimation, forKey: UIView.kRotationAnimationKey)
            }
        }
    }
    
    func stopRotating() {
        if layer.animation(forKey: UIView.kRotationAnimationKey) != nil {
            layer.removeAnimation(forKey: UIView.kRotationAnimationKey)
        }
    }
    
    func pauseAnimations() {
        pauseLayer(layer)
    }
    
    func resumeAnimations() {
        resumeLayer(layer)
    }
    
    
    func pauseLayer(_ layer: CALayer?) {
        let pausedTime: CFTimeInterval? = layer?.convertTime(CACurrentMediaTime(), from: nil)
        layer?.speed = 0.0
        layer?.timeOffset = pausedTime ?? 0
    }
    
    func resumeLayer(_ layer: CALayer?) {
        let pausedTime: CFTimeInterval? = layer?.timeOffset
        layer?.speed = 1.0
        layer?.timeOffset = 0.0
        layer?.beginTime = 0.0
        let timeSincePause: CFTimeInterval = (layer?.convertTime(CACurrentMediaTime(), from: nil) ?? 0) - (pausedTime ?? 0)
        layer?.beginTime = timeSincePause
    }
    
}

protocol AudioPlayerViewControllerDelegate {
    func setCurrentAudio()
}

//This returns song length
func calculateTimeFromNSTimeInterval(_ duration:TimeInterval) ->(minute:String, second:String){
    // let hour_   = abs(Int(duration)/3600)
    let minute_ = abs(Int((duration/60).truncatingRemainder(dividingBy: 60)))
    let second_ = abs(Int(duration.truncatingRemainder(dividingBy: 60)))
    
    // var hour = hour_ > 9 ? "\(hour_)" : "0\(hour_)"
    let minute = minute_ > 9 ? "\(minute_)" : "0\(minute_)"
    let second = second_ > 9 ? "\(second_)" : "0\(second_)"
    return (minute,second)
}

class AudioPlayerViewController: UIViewController, AVAudioPlayerDelegate, UICollectionViewDelegate, UICollectionViewDataSource {
    var _AudioPlayerViewControllerDelegate : AudioPlayerViewControllerDelegate?
    
    static var shared : AudioPlayerViewController? = nil
    //    var audioPlayer:AVAudioPlayer! = nil
    lazy var currentAudio = ""
    lazy var currentAudioPath : URL! = nil
    lazy var currentAudioAlbum : String? = nil
    lazy var currentAudioArtist : String? = nil
    lazy var currentAudioImage : UIImage? = nil
    //    var audioList:NSArray!
    lazy var currentAudioIndex : Int = 0
    lazy var toggle = true
    lazy var effectToggle = true
    lazy var totalLengthOfAudio = ""
    lazy var isTableViewOnscreen = false
    lazy var shuffleState = false
    lazy var repeatState = false
    lazy var shuffleArray = [Int]()
    @IBOutlet weak var bannerView: GADBannerView!
    @IBOutlet weak var heightConstraintBannerView: NSLayoutConstraint!
    
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var artistNameLabel: UILabel!
    @IBOutlet weak var albumNameLabel: UILabel!
    @IBOutlet var songNameLabel : UILabel!
    
    @IBOutlet weak var containerSoundWave: UIView!
    let waveform = ASWaveformPlayerView()
    @IBOutlet var progressTimerLabel : UILabel!
    @IBOutlet var playerProgressSlider : UISlider!
    @IBOutlet var totalLengthOfAudioLabel : UILabel!
    @IBOutlet weak var shuffleButton: UIButton!
    @IBOutlet weak var repeatButton: UIButton!
    
    @IBOutlet weak var previousImageView: UIImageView!
    @IBOutlet weak var playImageView: UIImageView!
    @IBOutlet weak var nextImageView: UIImageView!
    @IBOutlet weak var repeatImageView: UIImageView!
    @IBOutlet weak var shuffleImageView: UIImageView!
    
    @IBOutlet weak var headerView: UIImageView!
    @IBOutlet weak var distView: UIView!
    @IBOutlet weak var toolView: UIView!
    
    
    @IBOutlet weak var btnLicense: UIButton!
    
    @IBAction func showLicense(_ sender: UIButton) {
        if !sender.isHidden
        {
            //            showAlertDialog(title: "License 4.0", subtitle: "", cancelTitle: "Ok", cancelHandler: nil)
            openUrl("http://creativecommons.org/licenses/by/4.0/")
        }
    }
    
    
    @IBOutlet weak var volumeSlider: UISlider!
    let datePicker = UIDatePicker()
    
    let playImage = UIImage(named: "song_iconplay")
    let pauseImage = UIImage(named: "song_iconpause")
    let imBigSong = UIImage(named: "song_imgdisc")
    let imSongLock = UIImage(named: "song_imgdisc")
    
    let repeatImage0 = UIImage(named: "song_iconreplay0")
    let repeatImage1 = UIImage(named: "song_iconreplay1")
    
    let shuffleImage0 = UIImage(named: "song_iconrandom0")
    let shuffleImage1 = UIImage(named: "song_iconrandom1")
    
    @IBOutlet weak var lbTimer: UILabel!
    var sleepTimer : Timer? = nil
    var currentSleepTimer : TimeInterval = 0
    
    @IBAction func btnCloseAd(_ sender: UIButton) {

    }
    
    
    //MARK: CollectionView
    private var collectionViewLayout: HorizontalViewFlowLayout!
    private var pageWidth: CGFloat {
        return self.collectionViewLayout.itemSize.width + self.collectionViewLayout.minimumLineSpacing
    }
    
    private var contentOffset: CGFloat {
        return self.collectionView.contentOffset.x + self.collectionView.contentInset.left
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return audioList!.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "audioPlayerCell", for: indexPath) as! AudioPlayerCollectionViewCell
        //        cell.imageSong.image = imBigSong
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView.isDragging || collectionView.isDecelerating || collectionView.isTracking {
            return
        }
        
        let selectedPage = indexPath.row
        
        if selectedPage == self.pageControl.currentPage {
            NSLog("Did select center item")
        }
        else {
            self.scrollToPage(page: selectedPage, animated: true)
        }
    }
    
    @IBAction func pageControlValueChange(_ sender: Any) {
        self.scrollToPage(page: self.pageControl.currentPage, animated: true)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        self.pageControl.currentPage = Int(self.contentOffset / self.pageWidth)
        currentTableAudioIndex = self.pageControl.currentPage
        //        initAudioPlayer()
        playAudioFromPlaylist()
    }
    
    private func scrollToPage(page: Int, animated: Bool) {
        if(page < 0 || page >= audioList!.count)
        {
            return
        }
        let pageOffset = CGFloat(page) * self.pageWidth - self.collectionView.contentInset.left
        self.collectionView.setContentOffset(CGPoint.init(x: pageOffset,y: 0), animated: animated)
        self.pageControl.currentPage = page
        
    }
    
    //MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        AudioPlayerViewController.shared = self
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        
        bannerView = createAndLoadBanner(bannerView, self,heightConstraintBannerView)
        //        NotificationCenter.default.addObserver(self, selector: #selector(self.volumeDidChange(notification:)), name: NSNotification.Name(rawValue: "AVSystemController_SystemVolumeDidChangeNotification"), object: nil)
        
        initWaveForm()
        
        distView.translatesAutoresizingMaskIntoConstraints = false
        
        distView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        distView.topAnchor.constraint(equalTo: headerView.bottomAnchor).isActive = true
        distView.bottomAnchor.constraint(equalTo: toolView.topAnchor).isActive = true
        distView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        
        distView.layoutIfNeeded()
        
        let screenWidth = UIScreen.main.bounds.width * 0.5
        self.collectionViewLayout = HorizontalViewFlowLayout.configureLayout(collectionView: self.collectionView, itemSize: CGSize.init(width : screenWidth, height : screenWidth), minimumLineSpacing: 0)
        
        
        readFromPlist()
        initAudioPlayer()
        visualEffectViewEqua.isHidden = true
        initEqua()
        
        //        updater = CADisplayLink(target: self, selector: #selector(updateUI))
        //        updater?.add(to: .current, forMode: RunLoop.Mode.default)
        //        updater?.isPaused = true
        
        updater = Timer.scheduledTimer(timeInterval: 1, target: self,   selector: (#selector(updateUI)), userInfo: nil, repeats: true)
        RunLoop.current.add(updater!, forMode:  RunLoop.Mode.common)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        btnLicense.isHidden = true
        UserDefaults.standard.set(0, forKey: defaultsKeys.APP_MYTUNES)
        if let tabItems = tabBarController?.tabBar.items {
            // In this case we want to modify the badge number of the third tab:
            let tabItem = tabItems[2]
            tabItem.badgeValue = nil
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        Timer.scheduledTimer(withTimeInterval: 0.01, repeats: false) { (timer) in
            timer.invalidate()
            var heightSize = self.collectionView.frame.height - 50
            var widthSize = self.collectionView.frame.width - 50
            let defaults = UserDefaults.standard
            if defaults.string(forKey: defaultsKeys.APP_REMOVE_ADS) != nil
            {
                heightSize = self.collectionView.frame.height - 50
                widthSize = self.collectionView.frame.width - 50
            }
            else
            {
                heightSize = self.collectionView.frame.height
                widthSize = self.collectionView.frame.width
            }
            
            if heightSize > widthSize
            {
                self.collectionViewLayout = HorizontalViewFlowLayout.configureLayout(collectionView: self.collectionView, itemSize: CGSize.init(width : widthSize, height : widthSize), minimumLineSpacing: 0)
            }
            else
            {
                
                self.collectionViewLayout = HorizontalViewFlowLayout.configureLayout(collectionView: self.collectionView, itemSize: CGSize.init(width : heightSize, height : heightSize), minimumLineSpacing: 0)
            }
            
            self.readFromPlist()
            
            self.collectionView.reloadData()
            self.pageControl.numberOfPages = audioList!.count
            
            if(isClickFromPlayList)
            {
                isClickFromPlayList = false
                self.playAudioFromPlaylist()
            }
            else
            {
                self.currentAudioIndex = currentTableAudioIndex
                //this sets last listened trach number as current
                if(audioList != nil)
                {
                    if audioList!.count > 0
                    {
                        isRotation = false
                        if self.player.engine == nil
                        {
                            self.prepareAudio()
                            //                        retrievePlayerProgressSliderValue()
                        }
                        else
                        {
                            //                        setCurrentAudioPath()
                        }
                    }
                    else
                    {
                        if self.player.engine != nil
                        {
                            self.stopAudiplayer()
                        }
                    }
                }
            }
            
            self.pageControl.currentPage = self.currentAudioIndex
            self.scrollToPage(page: self.pageControl.currentPage, animated: false)
        }
    }
    
    // MARK: AVAudio properties
    lazy var engine = AVAudioEngine()
    lazy var player = AVAudioPlayerNode()
    lazy var rateEffect = AVAudioUnitTimePitch()
    
    var audioFile: AVAudioFile? {
        didSet {
            if let audioFile = audioFile {
                audioLengthSamples = audioFile.length
                audioFormat = audioFile.processingFormat
                audioSampleRate = Float(audioFormat?.sampleRate ?? 44100)
                audioLengthSeconds = Float(audioLengthSamples) / audioSampleRate
                //                audioBuffer = AVAudioPCMBuffer(pcmFormat: audioFormat!, frameCapacity: AVAudioFrameCount(audioLengthSamples))
                //                do
                //                {
                //                    try audioFile.read(into: audioBuffer!)
                //                }
                //                catch
                //                {
                //                    print("xxx audioFile \(error)")
                //                }
                //                audioFile.readIntoBuffer(audioBuffer, error: nil)
            }
        }
    }
    var audioFileURL: URL? {
        didSet {
            if let audioFileURL = audioFileURL {
                audioFile = try? AVAudioFile(forReading: audioFileURL)
            }
        }
    }
    //    lazy var audioBuffer: AVAudioPCMBuffer? = nil
    
    // MARK: other properties
    lazy var audioFormat: AVAudioFormat? = nil
    lazy var audioSampleRate: Float = 0
    lazy var audioLengthSeconds: Float = 0
    lazy var audioLengthSamples: AVAudioFramePosition = 0
    lazy var needsFileScheduled = true
    
    lazy var updater: Timer? = nil
    var currentFrame: AVAudioFramePosition {
        guard let lastRenderTime = player.lastRenderTime,
            let playerTime = player.playerTime(forNodeTime: lastRenderTime) else {
                return 0
        }
        
        return playerTime.sampleTime
    }
    lazy var seekFrame: AVAudioFramePosition = 0
    lazy var currentPosition: AVAudioFramePosition = 0
    let pauseImageHeight: Float = 26.0
    let minDb: Float = -80.0
    
    lazy var EQNode: AVAudioUnitEQ? = nil
    
    enum TimeConstant {
        static let secsPerMin = 60
        static let secsPerHour = TimeConstant.secsPerMin * 60
    }
    
    
    @IBAction func plus10Tapped(_ sender: UIButton) {
        guard let _ = player.engine else { return }
        seek(to: 10.0)
    }
    
    @IBAction func minus10Tapped(_ sender: UIButton) {
        guard let _ = player.engine else { return }
        needsFileScheduled = false
        seek(to: -10.0)
    }
    
    @objc func updateUI() {
        //        print("xxx \(playerProgressSlider.value)")
        if player.engine == nil || !player.isPlaying
        {
            return
        }
        currentPosition = currentFrame + seekFrame
        currentPosition = max(currentPosition, 0)
        currentPosition = min(currentPosition, audioLengthSamples)
        
        playerProgressSlider.value = Float(currentPosition) / Float(audioSampleRate)
        waveform.updatePlotWith(percentagePlayed: Double(currentPosition) / Double(audioLengthSamples))
        let time = Float(currentPosition) / Float(audioSampleRate)
        progressTimerLabel.text = formatted(time: time)
        totalLengthOfAudioLabel.text = formatted(time: audioLengthSeconds)
        
        //        UserDefaults.standard.set(playerProgressSlider.value , forKey: "playerProgressSliderValue")
        if currentPosition >= audioLengthSamples {
            player.stop()
            //            updater?.isPaused = true
            disconnectVolumeTap()
            finishPlayingAudio()
        }
    }
    
    func formatted(time: Float) -> String {
        var secs = Int(ceil(time))
        var hours = 0
        var mins = 0
        
        if secs > TimeConstant.secsPerHour {
            hours = secs / TimeConstant.secsPerHour
            secs -= hours * TimeConstant.secsPerHour
        }
        
        if secs > TimeConstant.secsPerMin {
            mins = secs / TimeConstant.secsPerMin
            secs -= mins * TimeConstant.secsPerMin
        }
        
        var formattedString = ""
        if hours > 0 {
            formattedString = "\(String(format: "%02d", hours)):"
        }
        formattedString += "\(String(format: "%02d", mins)):\(String(format: "%02d", secs))"
        return formattedString
    }
    
    func setupAudio() {
        stopAudiplayer()
        audioFileURL = currentAudioPath
        
        engine.attach(player)
        engine.attach(EQNode!)
        
        engine.connect(player, to: EQNode!, format: audioFormat)
        engine.connect(EQNode!, to: engine.mainMixerNode, format: audioFormat)
        
        engine.prepare()
    }
    
    
    func scheduleAudioFile() {
        //        guard let audioBuffer = audioBuffer else { return }
        
        seekFrame = 0
        //        player.scheduleBuffer(audioBuffer, at: nil) { [weak self] in
        //            self?.needsFileScheduled = true
        //        }
        player.scheduleFile(audioFile!, at: nil) {
            self.needsFileScheduled = true
        }
    }
    
    func connectVolumeTap() {
        let format = engine.mainMixerNode.outputFormat(forBus: 0)
        engine.mainMixerNode.installTap(onBus: 0, bufferSize: 1024, format: format) { buffer, when in
            
        }
    }
    
    func disconnectVolumeTap() {
        engine.mainMixerNode.removeTap(onBus: 0)
    }
    
    func seek(to time: Float) {
        guard let audioFile = audioFile else {
            return
        }
        
        //        let timers = time / audioSampleRate
        seekFrame = AVAudioFramePosition(time * audioSampleRate)
        seekFrame = max(seekFrame, 0)
        seekFrame = min(seekFrame, audioLengthSamples)
        currentPosition = seekFrame
        
        player.engine!.stop()
        player.stop()
        
        if currentPosition < audioLengthSamples {
            playImageView.image = pauseImage
            updateUI()
            needsFileScheduled = false
            
            player.scheduleSegment(audioFile, startingFrame: seekFrame, frameCount: AVAudioFrameCount(audioLengthSamples - seekFrame), at: nil) { [weak self] in
                self?.needsFileScheduled = true
            }
            
            if !player.isPlaying {
                do
                {
                    try player.engine!.start()
                }
                catch
                {
                    print("xxx \(error)")
                }
                player.play()
            }
        }
        else
        {
            finishPlayingAudio()
        }
        
        showMediaInfo()
    }
    
    func readFromPlist(){
        if isPlayingFromPlaylist
        {
//            self.title = audioPlayList![indexPlaylistSelected].name
            audioList = audioPlayList![indexPlaylistSelected].listSong
        }
        else
        {
//            self.title = "Music Player"
            do{
                let fileStep = try loadDataJson(NAME_LIST_JSON) as Data
                let vendorArray = try JSONDecoder().decode([DataListJson].self, from: fileStep) as [DataListJson]
                audioList = vendorArray
            }catch{
                
            }
        }
    }
    
    func playAudioFromPlaylist(){
        currentAudioIndex = currentTableAudioIndex
        prepareAudio()
        play(self)
    }
    
    func initAudioPlayer()
    {
        assingSliderUI()
        setRepeatAndShuffle()
        retrieveSavedTrackNumber()
        initAudioPlayerBgAndLockSc()
    }
    
    func assingSliderUI () {
        let minImage = UIImage(named: "slider-track-fill")
        let maxImage = UIImage(named: "slider-track")
        let thumb = UIImage(named: "thumb")
        
        playerProgressSlider.setMinimumTrackImage(minImage, for: UIControl.State())
        playerProgressSlider.setMaximumTrackImage(maxImage, for: UIControl.State())
        playerProgressSlider.setThumbImage(thumb, for: UIControl.State())
    }
    
    func retrieveSavedTrackNumber(){
        if let currentAudioIndex_ = UserDefaults.standard.object(forKey: "currentAudioIndex") as? Int{
            if(currentAudioIndex_ < audioList!.count){
                currentAudioIndex = currentAudioIndex_
            }else{
                currentAudioIndex = 0
            }
        }else{
            currentAudioIndex = 0
        }
    }
    
    func setRepeatAndShuffle(){
        shuffleState = UserDefaults.standard.bool(forKey: "shuffleState")
        repeatState = UserDefaults.standard.bool(forKey: "repeatState")
        if shuffleState == true {
            shuffleButton.isSelected = true
            shuffleImageView.image = shuffleImage1
        } else {
            shuffleButton.isSelected = false
            shuffleImageView.image = shuffleImage0
        }
        
        if repeatState == true {
            repeatButton.isSelected = true
            repeatImageView.image = repeatImage1
        }else{
            repeatButton.isSelected = false
            repeatImageView.image = repeatImage0
        }
        
    }
    
    func retrievePlayerProgressSliderValue(){
        let playerProgressSliderValue =  UserDefaults.standard.float(forKey: "playerProgressSliderValue")
        if playerProgressSliderValue != 0 {
            playerProgressSlider.value  = playerProgressSliderValue
            seek(to: playerProgressSliderValue)
        }else{
            playerProgressSlider.value = 0.0
            seek(to: playerProgressSliderValue)
            progressTimerLabel.text = "00:00"
        }
    }
    
    // MARK:- AVAudioPlayer Delegate's Callback method
    fileprivate func finishPlayingAudio() {
        print("xxx finishAudio")
        if shuffleState == false && repeatState == false
        {
            // do nothing
            if currentAudioIndex < audioList!.count - 1
            {
                currentAudioIndex += 1
                self.scrollToPage(page: currentAudioIndex, animated: true)
                prepareAudio()
                playAudio()
            }
            else
            {
                pauseAudioPlayer()
                //                currentAudioIndex = 0
                prepareAudio()
            }
        }
        else if shuffleState == false && repeatState == true
        {
            //repeat same song
            prepareAudio()
            playAudio()
            
        }
        else if shuffleState == true && repeatState == false
        {
            //shuffle songs but do not repeat at the end
            //Shuffle Logic : Create an array and put current song into the array then when next song come randomly choose song from available song and check against the array it is in the array try until you find one if the array and number of songs are same then stop playing as all songs are already played.
            shuffleArray.append(currentAudioIndex)
            if shuffleArray.count >= audioList!.count {
                playImageView.image = playImage
                return
                
            }
            
            var randomIndex = 0
            var newIndex = false
            while newIndex == false {
                randomIndex =  Int(arc4random_uniform(UInt32(audioList!.count)))
                if shuffleArray.contains(randomIndex) {
                    newIndex = false
                }else{
                    newIndex = true
                }
            }
            currentAudioIndex = randomIndex
            self.scrollToPage(page: currentAudioIndex, animated: true)
            prepareAudio()
            playAudio()
            
        }
        else if shuffleState == true && repeatState == true
        {
            //shuffle song endlessly
            shuffleArray.append(currentAudioIndex)
            if shuffleArray.count >= audioList!.count {
                shuffleArray.removeAll()
            }
            
            var randomIndex = 0
            var newIndex = false
            while newIndex == false {
                randomIndex =  Int(arc4random_uniform(UInt32(audioList!.count)))
                if shuffleArray.contains(randomIndex) {
                    newIndex = false
                }else{
                    newIndex = true
                }
            }
            currentAudioIndex = randomIndex
            self.scrollToPage(page: currentAudioIndex, animated: true)
            prepareAudio()
            playAudio()
        }
    }
    
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool){
        if isRotation{
            isRotation = false
        }
        
        if flag == true
        {
            finishPlayingAudio()
            
        }
    }
    
    func saveCurrentTrackNumber()
    {
        UserDefaults.standard.set(currentAudioIndex, forKey:"currentAudioIndex")
        UserDefaults.standard.synchronize()
    }
    
    func prepareAudio(){
        isRotation = false
        setCurrentAudioPath()
        setupAudio()
        configAudio()
    }
    
    func initWaveForm()
    {
        waveform.normalColor = .lightGray
        waveform.progressColor = .orange
        //with high sampleCount passed to init method to avoid artifacts set this to false
        waveform.allowSpacing = false
        
        containerSoundWave.addSubview(waveform)
        //ASWaveformPlayerView supports both manual and AutoLayout
        waveform.translatesAutoresizingMaskIntoConstraints = false
        
        waveform.leftAnchor.constraint(equalTo: containerSoundWave.leftAnchor).isActive = true
        waveform.rightAnchor.constraint(equalTo: containerSoundWave.rightAnchor).isActive = true
        waveform.topAnchor.constraint(equalTo: containerSoundWave.topAnchor).isActive = true
        waveform.bottomAnchor.constraint(equalTo: playerProgressSlider.topAnchor, constant: -5).isActive = true
        waveform.layoutIfNeeded()
    }
    
    //Sets audio file URL
    func setCurrentAudioPath()
    {
        currentAudioAlbum = ""
        currentAudioArtist = ""
        currentAudioImage = nil
        currentAudio = readSongNameFromPlist(currentAudioIndex)
        currentAudioPath = getSongPath(songName: currentAudio)
        audioPlayingPath = currentAudioPath.path
        
        let keyNameFile = currentAudio.replacingOccurrences(of: " ", with: "").replacingOccurrences(of: ".", with: "")
        if UserDefaults.standard.string(forKey: keyNameFile) == nil
        {
            btnLicense.isHidden = true
        }
        else
        {
            btnLicense.isHidden = false
        }
        
        do {
            try waveform.setAudio(audioURL: currentAudioPath, // URL to local a audio file
                sampleCount: 100, // higher numbers make waveform more detailed
                amplificationFactor: 0.5) // constant that affects height of each 'bar' in waveform
        } catch {
            //handle error thrown
            print(error.localizedDescription)
        }
        
        let playerItem = AVPlayerItem.init(url: currentAudioPath)
        let metadataList = playerItem.asset.commonMetadata
        for item in metadataList {
            if item.commonKey!.rawValue == "title" {
                currentAudioAlbum = item.value as? String
            }
            if item.commonKey!.rawValue  == "artist" {
                currentAudioArtist = item.value as? String
            }
            if item.commonKey!.rawValue  == "artwork" {
                if let audioImage = UIImage(data: (item.value as! NSData) as Data) {
                    currentAudioImage = audioImage
                }
            }
        }
    }
    
    // Prepare audio for playing
    fileprivate func configAudio()
    {
        currentPosition = 0
        playerProgressSlider.maximumValue = audioLengthSeconds
        playerProgressSlider.minimumValue = 0.0
        playerProgressSlider.value = 0.0
        updateLabels()
        showMediaInfo()
        progressTimerLabel.text = "00:00"
    }
    
    func updateLabels(){
        songNameLabel.text = currentAudio
        currentAudioImage = imBigSong
        
        if let artist = currentAudioArtist{
            artistNameLabel.text = artist
        }else{
            currentAudioArtist = ""
            artistNameLabel.text = ""
        }
        if let album = currentAudioAlbum{
            albumNameLabel.text = album
        }else{
            currentAudioAlbum = ""
            albumNameLabel.text = ""
        }
        
    }
    
    //MARK:- Player Controls Methods
    func playAudio()
    {
        playImageView.image = pauseImage
        if !isRotation{
            isRotation = true
        }
        
        if currentPosition >= audioLengthSamples {
            updateUI()
        }
        
        if  !player.isPlaying{
            //            updater?.isPaused = false
            connectVolumeTap()
            if needsFileScheduled {
                needsFileScheduled = false
                scheduleAudioFile()
            }
            if(!player.engine!.isRunning)
            {
                do
                {
                    try player.engine?.start()
                }
                catch
                {
                    
                }
            }
            player.play()
        }
        
        saveCurrentTrackNumber()
        self._AudioPlayerViewControllerDelegate?.setCurrentAudio()
    }
    
    func playNextAudio()
    {
        currentAudioIndex += 1
        if currentAudioIndex>audioList!.count-1{
            currentAudioIndex -= 1
            return
        }
        
        if player.isPlaying
        {
            prepareAudio()
            playAudio()
        }
        else
        {
            prepareAudio()
        }
        
    }
    
    func playPreviousAudio(){
        currentAudioIndex -= 1
        if currentAudioIndex<0{
            currentAudioIndex += 1
            return
        }
        
        if player.isPlaying{
            prepareAudio()
            playAudio()
        }else{
            prepareAudio()
        }
        
    }
    
    func stopAudiplayer()
    {
        guard let _ = player.engine else
        {
            print("xxx player.engine nil")
            return
        }
        disconnectVolumeTap()
        //        updater?.isPaused = true
        player.stop()
        engine.stop()
        engine.detach(player)
        engine.detach(EQNode!)
        player.reset()
        engine.reset()
        
        player = AVAudioPlayerNode()
    }
    
    func pauseAudioPlayer(){
        guard let _ = player.engine else
        {
            return
        }
        
        playImageView.image = playImage
        disconnectVolumeTap()
        //        updater?.isPaused = true
        player.pause()
        player.engine?.pause()
    }
    //MARK:-
    
    //Read plist file and creates an array of dictionary
    func readSongNameFromPlist(_ indexNumber: Int) -> String {
        return audioList![indexNumber].name
    }
    
    @objc func volumeDidChange(notification: NSNotification) {
        //print("VOLUME CHANGING", AVAudioSession.sharedInstance().outputVolume)
        
        let volume = notification.userInfo!["AVSystemController_AudioVolumeNotificationParameter"] as! Float
        volumeSlider.value = volume
    }
    
    func assingSliderVolume () {
        let minImage = UIImage(named: "slider-track-fill")
        let maxImage = UIImage(named: "slider-track")
        
        volumeSlider.setMinimumTrackImage(minImage, for: UIControl.State())
        volumeSlider.setMaximumTrackImage(maxImage, for: UIControl.State())
        //        MPVolumeView.setVolume(audioPlayer.volume)
        volumeSlider.value = AVAudioSession.sharedInstance().outputVolume
    }
    
    //Mark - IBAction
    @IBAction func changeVolumeSlider(_ sender: Any) {
        if(audioPlayer == nil){ return }
        let selectedValue = Float(volumeSlider.value)
        //        audioPlayer.volume = selectedValue
        MPVolumeView.setVolume(selectedValue)
    }
    
    @IBAction func play(_ sender : AnyObject) {
        guard let _ = player.engine else { return }
        //        needsFileScheduled = false
        
        if shuffleState == true {
            shuffleArray.removeAll()
        }
        
        if player.isPlaying{
            pauseAudioPlayer()
        }else{
            playAudio()
        }
    }
    
    @IBAction func next(_ sender : AnyObject) {
        guard let _ = player.engine else { return }
        //        needsFileScheduled = false
        
        playNextAudio()
        self.scrollToPage(page: self.pageControl.currentPage + 1, animated: true)
    }
    
    @IBAction func previous(_ sender : AnyObject) {
        guard let _ = player.engine else { return }
        //        needsFileScheduled = false
        
        playPreviousAudio()
        self.scrollToPage(page: self.pageControl.currentPage - 1, animated: true)
    }
    
    @IBAction func changeAudioLocationSlider(_ sender : UISlider)
    {
        guard let _ = player.engine else { return }
        //        needsFileScheduled = false
        let changeValue = sender.value
        seek(to: changeValue)
    }
    @IBAction func changeValueSlider(_ sender: UISlider) {
        let time = sender.value
        seekFrame = AVAudioFramePosition(time * audioSampleRate)
        seekFrame = max(seekFrame, 0)
        seekFrame = min(seekFrame, audioLengthSamples)
        currentPosition = seekFrame
        currentPosition = currentFrame + seekFrame
        currentPosition = max(currentPosition, 0)
        currentPosition = min(currentPosition, audioLengthSamples)
        
        waveform.updatePlotWith(percentagePlayed: Double(currentPosition) / Double(audioLengthSamples))
    }
    
    @IBAction func shuffleButtonTapped(_ sender: UIButton) {
        shuffleArray.removeAll()
        if sender.isSelected == true {
            shuffleImageView.image = shuffleImage0
            sender.isSelected = false
            shuffleState = false
            UserDefaults.standard.set(false, forKey: "shuffleState")
        } else {
            shuffleImageView.image = shuffleImage1
            sender.isSelected = true
            shuffleState = true
            UserDefaults.standard.set(true, forKey: "shuffleState")
        }
    }
    
    @IBAction func repeatButtonTapped(_ sender: UIButton) {
        if sender.isSelected == true {
            repeatImageView.image = repeatImage0
            sender.isSelected = false
            repeatState = false
            UserDefaults.standard.set(false, forKey: "repeatState")
        } else {
            repeatImageView.image = repeatImage1
            sender.isSelected = true
            repeatState = true
            UserDefaults.standard.set(true, forKey: "repeatState")
        }
    }
    
    @IBAction func showEqualization(_ sender: Any)
    {
        visualEffectViewEqua.isHidden = false
    }
    
    //Mark - Sleep Timer
    @IBAction func showActionSheetTimer(sender: AnyObject) {
        let alert = UIAlertController(title: "Timer", message: "Please Select an Option", preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "5 mins", style: .default , handler:{ (UIAlertAction)in
            self.startSleepTimer(duration: 5)
        }))
        
        alert.addAction(UIAlertAction(title: "15 mins", style: .default , handler:{ (UIAlertAction)in
            self.startSleepTimer(duration: 15)
        }))
        
        alert.addAction(UIAlertAction(title: "30 mins", style: .default , handler:{ (UIAlertAction)in
            self.startSleepTimer(duration: 30)
        }))
        
        alert.addAction(UIAlertAction(title: "60 mins", style: .default, handler:{ (UIAlertAction)in
            self.startSleepTimer(duration: 60)
        }))
        
        let duration = UserDefaults.standard.value(forKey: defaultsKeys.APP_SLEEP_TIMER) as? Int
        if duration != nil
        {
            alert.addAction(UIAlertAction(title: "Stop Timer", style: .destructive, handler:{ (UIAlertAction)in
                self.stopSleepTimer()
            }))
        }
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler:{ (UIAlertAction)in
            alert.dismiss(animated: false, completion: nil)
        }))
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.pad){
            if let currentPopoverpresentioncontroller = alert.popoverPresentationController{
                currentPopoverpresentioncontroller.sourceView = self.view
                currentPopoverpresentioncontroller.sourceRect = self.view.bounds
                currentPopoverpresentioncontroller.permittedArrowDirections = UIPopoverArrowDirection(rawValue: 0)
                self.present(alert, animated: true, completion: nil)
            }
        }else{
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func stopSleepTimer()
    {
        lbTimer.text = ""
        lbTimer.isHidden = true
        if(sleepTimer != nil)
        {
            sleepTimer!.invalidate()
            sleepTimer = nil
        }
        
        UserDefaults.standard.removeObject(forKey: defaultsKeys.APP_SLEEP_TIMER)
    }
    
    func startSleepTimer(duration : Int)
    {
        let checkTimer = UserDefaults.standard.value(forKey: defaultsKeys.APP_SLEEP_TIMER) as? TimeInterval
        if checkTimer == nil
        {
            
        }
        else
        {
            stopSleepTimer()
        }
        
        lbTimer.isHidden = false
        self.currentSleepTimer = TimeInterval(duration * 60)
        
        UserDefaults.standard.set(self.currentSleepTimer , forKey: defaultsKeys.APP_SLEEP_TIMER)
        
        sleepTimer = Timer.scheduledTimer(timeInterval: 1, target: self,   selector: (#selector(updateSleepTimer)), userInfo: nil, repeats: true)
        
        RunLoop.current.add(sleepTimer!, forMode: RunLoop.Mode.common)
    }
    
    @objc func updateSleepTimer()
    {
        print("xxx updateSleepTimer")
        currentSleepTimer -= 1
        let time = calculateTimeFromNSTimeInterval(currentSleepTimer)
        lbTimer.text  = "\(time.minute):\(time.second)"
        if currentSleepTimer <= 0
        {
            print("finish")
            stopSleepTimer()
            if player.engine != nil
            {
                play(self)
            }
        }
        
        //print("\(time.minute):\(time.second)")
    }
    
    //Mark - Equalization
    @IBOutlet weak var visualEffectViewEqua: UIVisualEffectView!
    @IBOutlet weak var switchMusicOn: UISwitch!
    @IBOutlet weak var switchBypass: UISwitch!
    @IBOutlet var freqsSlides: [UISlider]!
    
    let frequencies: [Int] = [32, 63, 125, 250, 500, 1000, 2000, 4000, 8000, 16000]
    var preSets: [[Float]] = [
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0], // My setting
        [4, 6, 5, 0, 1, 3, 5, 4.5, 3.5, 0], // Dance
        [4, 3, 2, 2.5, -1.5, -1.5, 0, 1, 2, 3], // Jazz
        [5, 4, 3.5, 3, 1, 0, 0, 0, 0, 0] // Base Main
    ]
    
    func initEqua()
    {
        let thumb = UIImage(named: "thumb")
        for (_, slide) in freqsSlides.enumerated() {
            slide.minimumValue = -20
            slide.maximumValue = 20
            slide.transform = CGAffineTransform(rotationAngle: CGFloat(-Double.pi / 2))
            slide.setThumbImage(thumb, for: UIControl.State())
        }
        
        // initial Equalizer.
        EQNode = AVAudioUnitEQ(numberOfBands: frequencies.count)
        EQNode!.globalGain = 1
        for i in 0...(EQNode!.bands.count-1) {
            EQNode!.bands[i].frequency  = Float(frequencies[i])
            EQNode!.bands[i].gain       = 0
            EQNode!.bands[i].bypass     = false
            EQNode!.bands[i].filterType = .parametric
        }
    }
    
    @IBAction func turnOffEqua(_ sender: Any) {
        visualEffectViewEqua.isHidden = true
    }
    
    // UISwich
    @IBAction func switchValueChanged(_ sender: Any) {
        if let sender = sender as? UISwitch
        {
            if sender == switchBypass
            {
                setBypass(sender.isOn)
            }
        }
    }
    
    // UISlider
    @IBAction func sliderValueChanged(_ sender: Any) {
        if let slider = sender as? UISlider {
            print("slider of index:", slider.tag, "is changed to", slider.value)
            
            // Update equalizer values
            var preSet = getEquailizerOptions()
            preSet[slider.tag] = slider.value
            setEquailizerOptions(gains: preSet)
        }
    }
    
    // UISegmentedControl
    fileprivate func updateSliderEqua() {
        // Update UISliders.
        let preSet = getEquailizerOptions()
        for (index, slide) in freqsSlides.enumerated() {
            slide.value = preSet[index]
        }
    }
    
    @IBAction func segmentedControlValueChanged(_ sender: Any) {
        if let segmentedControl = sender as? UISegmentedControl {
            print("segmentedControl is selected: ", segmentedControl.selectedSegmentIndex)
            
            // Update Equalizer.
            let index = segmentedControl.selectedSegmentIndex
            setEquailizerOptions(gains: preSets[index])
            
            updateSliderEqua()
        }
    }
    
    public func setBypass(_ isOn: Bool) {
        for i in 0...(EQNode!.bands.count-1) {
            EQNode!.bands[i].bypass = isOn
        }
    }
    
    public func setEquailizerOptions(gains: [Float]) {
        guard let EQNode = EQNode else {
            return
        }
        for i in 0...(EQNode.bands.count-1) {
            EQNode.bands[i].gain = gains[i]
        }
    }
    
    public func getEquailizerOptions() -> [Float] {
        guard let EQNode = EQNode else {
            return []
        }
        return EQNode.bands.map { $0.gain }
    }
}

//MARK:- Lockscreen Media Control
extension AudioPlayerViewController
{
    // This shows media info on lock screen - used currently and perform controls
    func showMediaInfo(){
        let time = Float(currentPosition) / Float(audioSampleRate)
        let artWork = MPMediaItemArtwork.init(image: imSongLock!)
        
        MPNowPlayingInfoCenter.default().nowPlayingInfo = [
            MPMediaItemPropertyTitle : currentAudio,
            MPMediaItemPropertyArtist : currentAudioArtist!,
            MPMediaItemPropertyArtwork : artWork,
            MPNowPlayingInfoPropertyElapsedPlaybackTime : time,
            MPMediaItemPropertyPlaybackDuration : audioLengthSeconds,
            MPNowPlayingInfoPropertyPlaybackRate : 1.0
            //            MPMediaItemPropertyAlbumTitle : currentAudioAlbum
        ]
    }
    
    func initAudioPlayerBgAndLockSc() {
        do {
            //keep alive audio at background
            if #available(iOS 10.0, *) {
                try AVAudioSession.sharedInstance().setCategory(.playback, mode: .default, options: [])
            } else {
                // Fallback on earlier versions
                try AudioSessionHelper.setAudioSession()
            }
            try AVAudioSession.sharedInstance().setActive(true)
        } catch _ {
        }
        
        //        UIApplication.shared.beginReceivingRemoteControlEvents()
        //
        //        //LockScreen Media control registry
        //        if UIApplication.shared.responds(to: #selector(UIApplication.beginReceivingRemoteControlEvents))
        //        {
        //            UIApplication.shared.beginReceivingRemoteControlEvents()
        //            UIApplication.shared.beginBackgroundTask(expirationHandler: { () -> Void in
        //            })
        //        }
        
        let commandCenter = MPRemoteCommandCenter.shared()
        if #available(iOS 9.1, *) {
            commandCenter.changePlaybackPositionCommand.isEnabled = true
            commandCenter.changePlaybackPositionCommand.addTarget(self, action:#selector(changePlaybackPositionCommand(_:)))
            commandCenter.playCommand.isEnabled = true
            commandCenter.playCommand.addTarget(self, action: #selector(playCommand(_:)))
            commandCenter.pauseCommand.isEnabled = true
            commandCenter.pauseCommand.addTarget(self, action: #selector(pauseCommand(_:)))
            commandCenter.nextTrackCommand.isEnabled = true
            commandCenter.nextTrackCommand.addTarget(self, action: #selector(nextCommand(_:)))
            commandCenter.previousTrackCommand.isEnabled = true
            commandCenter.previousTrackCommand.addTarget(self, action: #selector(previousCommand(_:)))
        } else {
            // Fallback on earlier versions
        }
    }
    
    @objc func changePlaybackPositionCommand(_ event:MPChangePlaybackPositionCommandEvent) -> MPRemoteCommandHandlerStatus{
        let time = event.positionTime
        seek(to: Float(time))
        //use time to update your track time
        return MPRemoteCommandHandlerStatus.success;
    }
    
    @objc func playCommand(_ event:MPRemoteCommandEvent) -> MPRemoteCommandHandlerStatus{
        play(self)
        //use time to update your track time
        return MPRemoteCommandHandlerStatus.success;
    }
    
    @objc func pauseCommand(_ event:MPRemoteCommandEvent) -> MPRemoteCommandHandlerStatus{
        play(self)
        //use time to update your track time
        return MPRemoteCommandHandlerStatus.success;
    }
    
    @objc func nextCommand(_ event:MPRemoteCommandEvent) -> MPRemoteCommandHandlerStatus{
        next(self)
        //use time to update your track time
        return MPRemoteCommandHandlerStatus.success;
    }
    
    @objc func previousCommand(_ event:MPRemoteCommandEvent) -> MPRemoteCommandHandlerStatus{
        previous(self)
        //use time to update your track time
        return MPRemoteCommandHandlerStatus.success;
    }
}
