//
//  PlaylistTableViewCell.swift
//  Music Player
//
//  Created by NhonGa on 14/08/2018.
//  Copyright © 2018 NhonGa. All rights reserved.
//

import UIKit

class ListPlayingTableViewCell: UITableViewCell {

    @IBOutlet weak var ic_song_selected: UIImageView!
    @IBOutlet weak var tx_song_name: UILabel!
    @IBOutlet weak var tx_song_artist: UILabel!
    @IBOutlet weak var tx_song_duration: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
