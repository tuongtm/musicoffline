//
//  PlaylistViewController.swift
//  Music Player
//
//  Created by NhonGa on 10/08/2018.
//  Copyright © 2018 NhonGa. All rights reserved.
//

import UIKit
import AVFoundation
import MediaPlayer
import GoogleMobileAds

var isPlayingFromPlaylist : Bool = false
var indexSongsSelected : Int = 0
var indexPlaylistSelected : Int = 0
var audioPlayList : [DataPlayListJson]? = [DataPlayListJson]()
var audioList : [DataListJson]? = [DataListJson]()
var audioPlayer : AVAudioPlayer! = nil
var audioPlayingPath : String = ""
var currentTableAudioIndex : Int = 0
var isClickFromPlayList : Bool = false
var countAdsPlayList : Int = 0

class ListPlayingViewController: UIViewController,UITableViewDelegate, UITableViewDataSource, GADInterstitialDelegate, UISearchBarDelegate {
    static var shared : ListPlayingViewController? = nil
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchbar: UISearchBar!
   
    @IBOutlet weak var parentViewNameDate: UIView!
    var currentItemSelected : Int = 0
    var loading_1: UIImage!
    var loading_2: UIImage!
    var loading_3: UIImage!
    var loading_4: UIImage!
    var images: [UIImage]!
    var animatedImage: UIImageView!
    
    var filterAudioList : [DataListJson]!
    var tableAudioList : [DataListJson]!
    var isSearching : Bool = false
    var resultPredicate : NSPredicate!
    
    @IBOutlet weak var bannerView: GADBannerView!
    @IBOutlet weak var heightConstraintBannerView: NSLayoutConstraint!
    
    @IBAction func btnCloseAd(_ sender: UIButton) {

    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        ListPlayingViewController.shared = self
        self.title = "List Songs"
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.searchbar.delegate = self
        if(fullAds != nil)
        {
            fullAds.delegate = self
        }
        loading_1 = UIImage(named: "loading1")
        loading_2 = UIImage(named: "loading2")
        loading_3 = UIImage(named: "loading3")
        loading_4 = UIImage(named: "loading4")
        images = [loading_1, loading_2, loading_3, loading_4]
        animatedImage = UIImageView.init()
        filterAudioList = audioList
        isSearching = false
//        self.navigationController?.navigationItem.rightBarButtonItem = self.editButtonItem
        
        bannerView = createAndLoadBanner(bannerView, self, heightConstraintBannerView)
        NotificationCenter.default.addObserver(self, selector: #selector(ShowAdsFull), name: NSNotification.Name(rawValue: "ShowAdsFull\(PositionAdsFull[1])"), object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
         setCurrentAudio()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if !isPlayingFromPlaylist
        {
            do
            {
                try savePropertyList(NAME_LIST_JSON, audioList!)
            }
            catch
            {
                
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func closeAdsFull() -> Void{
        isClickFromPlayList = true
        NotificationCenter.default.removeObserver(self, name: .CLICK_LIST_PLAYING, object: nil)
        self.navigationController?.popViewController(animated: true)
    }
    
    func interstitialDidDismissScreen(_ ad: GADInterstitial) {
        createAndLoadInterstitial()
        closeAdsFull()
    }
    
    func setCurrentAudio() {
        retrieveSavedTrackNumber()
        readFromPlist()
    }
    
    func retrieveSavedTrackNumber(){
        if let currentAudioIndex_ = UserDefaults.standard.object(forKey: "currentAudioIndex") as? Int{
            guard let listData = audioList else{
                currentTableAudioIndex = 0
                return
            }
            
            if(currentAudioIndex_ < listData.count){
                currentTableAudioIndex = currentAudioIndex_
            }else{
                currentTableAudioIndex = 0
            }
        }else{
            currentTableAudioIndex = 0
        }
    }
    
    func readFromPlist()
    {
        if isPlayingFromPlaylist
        {
            audioList = audioPlayList![indexPlaylistSelected].listSong
            parentViewNameDate.isHidden = true
        }
        else
        {
            parentViewNameDate.isHidden = false
            do{
                let fileStep = try loadDataJson(NAME_LIST_JSON) as Data
                let vendorArray = try JSONDecoder().decode([DataListJson].self, from: fileStep) as [DataListJson]
                audioList = vendorArray
            }catch{
                
            }
            
            audioList = audioList!.sorted(by: { $0.name < $1.name})
            filterAudioList = filterAudioList.sorted(by: { $0.name < $1.name})
            self.tableView.reloadData()
        }
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if(isSearching)
        {
            return filterAudioList.count
        }
        return audioList!.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let row = indexPath.row
        var itemListPlaying : DataListJson = self.tableAudioList[row]
        let songName = itemListPlaying.name
        
        for i in 0...(audioList!.count - 1) {
            itemListPlaying = audioList![i]
            let songNameClicked = itemListPlaying.name
            if songNameClicked == songName
            {
                currentTableAudioIndex = i
                break
            }
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(ShowAdsFull), name: .CLICK_LIST_PLAYING, object: nil)
        if(fullAds != nil)
        {
            fullAds.delegate = self
        }
        else
        {
            createAndLoadInterstitial()
        }
        
        countAdsPlayList+=1
        if(countAdsPlayList > 3){
            countAdsPlayList = 0
            showActivityIndicatoryCountDown(isRV: false,pos: .CLICK_LIST_PLAYING){ (str) in
                if str == "removeads"
                {
                    self.closeAdsFull()
                }
            }
        }else{
            closeAdsFull()
        }
        
    }
    
    @objc func ShowAdsFull()
    {
        DispatchQueue.main.async
            {
                if !showAdsInterstitial(self)
                {
                    self.closeAdsFull()
                }
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        let childViewController = segue.destination as! AudioPlayerViewController
//        childViewController._AudioPlayerViewControllerDelegate = self
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        let row = indexPath.row
        if editingStyle == .delete {
            let refreshAlert = UIAlertController(title: "", message: "Are you sure?", preferredStyle: UIAlertController.Style.alert)
            
            refreshAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
                do{
                    let fileStep = try loadDataJson(NAME_LIST_JSON) as Data
                    var vendorArray = try JSONDecoder().decode([DataListJson].self, from: fileStep) as [DataListJson]
                    
                    var i = 0
                    for dataInPlayList in audioPlayList! {
                        var dataListSongInPlayList = dataInPlayList.listSong
                        dataListSongInPlayList.removeAll(where: { (dataPlayListSearching) -> Bool in
                            return dataPlayListSearching.name == vendorArray[row].name
                        })
                        audioPlayList![i].listSong = dataListSongInPlayList
                        i+=1
                    }
                    
                    try savePlayListToJson(NAME_PLAYLIST_JSON, audioPlayList!)
                    
                    try FileManager.default.removeItem(at: getSongPath(songName: vendorArray[row].name))
                    vendorArray.remove(at: row)
                    try savePropertyList(NAME_LIST_JSON, vendorArray)
                    
                    self.setCurrentAudio()
                    self.tableView.reloadData()
                }catch{
                    
                }
            }))
            
            refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action: UIAlertAction!) in
                print("Handle Cancel Logic here")
            }))
            
            present(refreshAlert, animated: true, completion: nil)
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "playlistCell", for: indexPath)as! ListPlayingTableViewCell
        let row = indexPath.row
        // Configure the cell...
        if(isSearching)
        {
            tableAudioList = filterAudioList
        }
        else
        {
            tableAudioList = audioList
        }
        let itemListPlaying : DataListJson = tableAudioList[row]
        let songName = itemListPlaying.name
        let songPath = getSongPath(songName: songName)
        cell.tx_song_name?.text = songName
        cell.tx_song_duration?.text = ""
        cell.tx_song_artist?.text = ""
//        cell.ic_song_selected = animatedImage
        
        let playerItem = AVPlayerItem.init(url: songPath)
        let metadataList = playerItem.asset.commonMetadata
        let time = calculateTimeFromNSTimeInterval(playerItem.asset.duration.seconds)
         cell.tx_song_duration?.text = "\(time.minute):\(time.second)"
        for item in metadataList {
//            if item.commonKey!.rawValue == "title" {
//
//            }
            if item.commonKey!.rawValue  == "artist" {
                cell.tx_song_artist?.text = item.value as? String
            }
//            if item.commonKey!.rawValue  == "artwork" {
//                if let audioImage = UIImage(data: (item.value as! NSData) as Data) {
////                    let audioArtwork = MPMediaItemArtwork(image: audioImage)
////                    cell.ic_song_selected.image = audioImage
//                }
//            }
        }
        
        if audioPlayingPath == songPath.path
        {
            cell.ic_song_selected.animationImages = images
            cell.ic_song_selected.animationDuration = 0.5
            cell.ic_song_selected.startAnimating()
            cell.ic_song_selected.isHidden = false
            currentTableAudioIndex = row
        }
        else
        {
            cell.ic_song_selected.isHidden = true
        }
        
        return cell
    }
 
    @IBAction func sortListPlaying(_ sender: UISegmentedControl) {
        let idSelect : Int = sender.selectedSegmentIndex
        
        if idSelect == 0
        {
            audioList = audioList!.sorted(by: { $0.name < $1.name})
            filterAudioList = filterAudioList.sorted(by: { $0.name < $1.name})
        }
        else
        {
            audioList = audioList!.sorted(by: { $0.createdate < $1.createdate})
            filterAudioList = filterAudioList.sorted(by: { $0.createdate < $1.createdate})
        }
        tableView.reloadData()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.isEmpty
        {
            isSearching = false
            view.endEditing(true)
            self.tableView.reloadData()
        }
        else
        {
            isSearching = true
            resultPredicate = NSPredicate(format: "name contains[c] %@", searchText)
            self.filterAudioList = audioList!.filter {
                $0.name.range(of: searchText, options: .caseInsensitive) != nil
            }
            self.tableView.reloadData()
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int){
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
