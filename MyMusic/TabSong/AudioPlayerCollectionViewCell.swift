//
//  AudioPlayerCollectionViewCell.swift
//  Tubidy
//
//  Created by NhonGa on 30/10/2018.
//  Copyright © 2018 ThreeWolves. All rights reserved.
//

import UIKit

class AudioPlayerCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imageSong: UIImageView!
    override var isSelected: Bool{
        didSet{
            if self.isSelected
            {
                //self.imageSong.transform = CGAffineTransform(scaleX: 1.1, y: 1.1)
            }
            else
            {
                 //self.imageSong.transform = CGAffineTransform(scaleX: 0.9, y: 0.9)//CGAffineTransform.identity
            }
        }
    }
}
