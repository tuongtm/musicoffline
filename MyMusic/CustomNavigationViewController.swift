//
//  CustomNavigationViewController.swift
//  Tubidy
//
//  Created by NhonGa on 23/10/2018.
//  Copyright © 2018 ThreeWolves. All rights reserved.
//

import UIKit

class CustomNavigationViewController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
//        let gradient = CAGradientLayer()
//        var bounds = navigationBar.bounds
//        bounds.size.height += UIApplication.shared.statusBarFrame.size.height
//        gradient.frame = bounds
//        //            gradient.colors = [UIColor.red.cgColor, UIColor.blue.cgColor]
//
//        let firstColor : UIColor = UIColor(red: 56/255, green: 39/255, blue: 180/255, alpha: 1.0)
//        let secondColor : UIColor = UIColor(red: 108/255, green: 24/255, blue: 164/255, alpha: 1.0)
//        gradient.colors = [firstColor.cgColor ,secondColor.cgColor]
//
//        gradient.startPoint = CGPoint(x: 0, y: 0)
//        gradient.endPoint = CGPoint(x: 1, y: 0)

//        if let image = getImageFrom(gradientLayer: gradient) {
//              navigationBar.setBackgroundImage(image, for: UIBarMetrics.default)
//        }
        
        navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationBar.shadowImage = UIImage()
        navigationBar.isTranslucent = true
        view.backgroundColor = .clear
    }
    
    func getImageFrom(gradientLayer:CAGradientLayer) -> UIImage? {
        var gradientImage:UIImage?
        UIGraphicsBeginImageContext(gradientLayer.frame.size)
        if let context = UIGraphicsGetCurrentContext() {
            gradientLayer.render(in: context)
            gradientImage = UIGraphicsGetImageFromCurrentImageContext()?.resizableImage(withCapInsets: UIEdgeInsets.zero, resizingMode: .stretch)
        }
        UIGraphicsEndImageContext()
        return gradientImage
    }
    
    
    override func remoteControlReceived(with event: UIEvent?) {
        if event!.type == UIEvent.EventType.remoteControl{
            switch event!.subtype{
            case UIEvent.EventSubtype.remoteControlPlay:
                AudioPlayerViewController.shared!.play(AudioPlayerViewController.shared!)
            case UIEvent.EventSubtype.remoteControlPause:
                 AudioPlayerViewController.shared!.play( AudioPlayerViewController.shared!)
            case UIEvent.EventSubtype.remoteControlNextTrack:
                 AudioPlayerViewController.shared!.next( AudioPlayerViewController.shared!)
            case UIEvent.EventSubtype.remoteControlPreviousTrack:
                 AudioPlayerViewController.shared!.previous( AudioPlayerViewController.shared!)
            default:
                print("There is an issue with the control")
            }
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
