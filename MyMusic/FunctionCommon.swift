//
//  FunctionCommon.swift
//  Music Player
//
//  Created by NhonGa on 16/08/2018.
//  Copyright © 2018 NhonGa. All rights reserved.
//

import Foundation
import GoogleMobileAds

extension UIColor {
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    convenience init(rgb: Int) {
        self.init(
            red: (rgb >> 16) & 0xFF,
            green: (rgb >> 8) & 0xFF,
            blue: rgb & 0xFF
        )
    }
    
    convenience init(hexString: String, alpha: CGFloat = 1.0) {
        let hexString: String = hexString.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let scanner = Scanner(string: hexString)
        if hexString.hasPrefix("#")  {
            scanner.scanLocation = 1
        }
        var color: UInt32 = 0
        scanner.scanHexInt32(&color)
        let mask = 0x000000FF
        let r = Int(color >> 16) & mask
        let g = Int(color >> 8) & mask
        let b = Int(color) & mask
        let red = CGFloat(r) / 255.0
        let green = CGFloat(g) / 255.0
        let blue = CGFloat(b) / 255.0
        self.init(red:red, green:green, blue:blue, alpha:alpha)
    }
    func toHexString() -> String {
        var r:CGFloat = 0
        var g:CGFloat = 0
        var b:CGFloat = 0
        var a:CGFloat = 0
        getRed(&r, green: &g, blue: &b, alpha: &a)
        let rgb:Int = (Int)(r*255)<<16 | (Int)(g*255)<<8 | (Int)(b*255)<<0
        return String(format: "#%06x", rgb)
    }
}

extension UITextView {
    func hyperLink(originalText: String, hyperLink: String, urlString: String) {
        let style = NSMutableParagraphStyle()
        style.alignment = .left
        let attributedOriginalText = NSMutableAttributedString(string: originalText)
        let linkRange = attributedOriginalText.mutableString.range(of: hyperLink)
        let fullRange = NSMakeRange(0, attributedOriginalText.length)
        attributedOriginalText.addAttribute(NSAttributedString.Key.link, value: urlString, range: linkRange)
        attributedOriginalText.addAttribute(NSAttributedString.Key.paragraphStyle, value: style, range: fullRange)
        attributedOriginalText.addAttribute(NSAttributedString.Key.font, value: UIFont.systemFont(ofSize: 20), range: fullRange)
        attributedOriginalText.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.black, range: fullRange)
        self.linkTextAttributes = [
            NSAttributedString.Key.foregroundColor: UIColor.blue,
            NSAttributedString.Key.underlineStyle: NSUnderlineStyle.single.rawValue,
        ]
        self.attributedText = attributedOriginalText
    }
    
}
var countTimerShowAds : Int = 0
var timer : Timer? = nil
var PositionAdsFull = ["Tutorial", "ListPlaying", "CustomTabbar"]

extension Notification.Name
{
    static let APP_HOME_FIRST_SHOW_ADS_FULL = Notification.Name("APP_HOME_FIRST_SHOW_ADS_FULL")
    static let CLOSE_TUTORIAL = Notification.Name("CLOSE_TUTORIAL")
    static let VIDEO_DOWNLOAD_LIST_DATA_RT = Notification.Name("VIDEO_DOWNLOAD_LIST_DATA_RT")
    static let CLICK_TABBAR = Notification.Name("CLICK_TABBAR")
    static let CLICK_LIST_PLAYING = Notification.Name("CLICK_LIST_PLAYING")
    static let CLOSE_SUBS = Notification.Name("CLOSE_SUBS")
}

extension UIViewController {
    func showTutorial(_ screenName : String) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let tutvc = storyboard.instantiateViewController(withIdentifier: screenName)
        tutvc.modalPresentationStyle = .fullScreen
        self.present(tutvc, animated: true, completion: nil)
    }
    
    func hideActivityIndicatory()
    {
        if let viewWithTag = self.view.viewWithTag(100) {
            viewWithTag.removeFromSuperview()
        }else{
            print("xxx No!")
        }
    }
    
    func showActivityIndicatory()
    {
        let containerView : UIView = UIView()
        containerView.frame = view.frame
        containerView.center = view.center
        containerView.backgroundColor = UIColor.init(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.3)
        containerView.tag = 100
        
        let loadingView: UIView = UIView()
        loadingView.frame = CGRect.init(x: 0, y: 0, width: 80, height: 80)
        loadingView.center = view.center
        loadingView.backgroundColor = UIColor.init(red: 68/255, green: 68/255, blue: 68/255, alpha: 0.7)
        loadingView.clipsToBounds = true
        loadingView.layer.cornerRadius = 10
        
        let actInd: UIActivityIndicatorView = UIActivityIndicatorView()
        actInd.frame = CGRect.init(x: 0, y: 0, width: 40, height: 40)
        actInd.style =
            UIActivityIndicatorView.Style.whiteLarge
        actInd.center = CGPoint.init(x: loadingView.frame.size.width / 2, y: loadingView.frame.size.height / 2)
        loadingView.addSubview(actInd)
        containerView.addSubview(loadingView)
        view.addSubview(containerView)
        actInd.startAnimating()
    }
    
    func showActivityIndicatoryTitle() -> (UILabel, UIProgressView)
    {
        let containerView : UIView = UIView()
        containerView.frame = view.frame
        containerView.center = view.center
        containerView.backgroundColor = UIColor.init(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.3)
        containerView.tag = 100
        
        let loadingView: UIView = UIView()
        loadingView.frame = CGRect.init(x: 0, y: 0, width: 150, height: 150)
        loadingView.center = view.center
        loadingView.backgroundColor = UIColor.init(red: 68/255, green: 68/255, blue: 68/255, alpha: 0.7)
        loadingView.clipsToBounds = true
        loadingView.layer.cornerRadius = 10
        
        
        let textDes = UILabel.init(frame: CGRect(x : 0, y : 0, width : 150, height: 50))
        textDes.textColor = UIColor.white
        textDes.font = UIFont(name:"SegoeUI", size: 15.0)
        textDes.textAlignment = .center
        textDes.numberOfLines = 0
        textDes.center = CGPoint.init(x: loadingView.frame.size.width / 2, y: loadingView.frame.size.height / 2 + 45)
        loadingView.addSubview(textDes)
        
        let actInd: UIActivityIndicatorView = UIActivityIndicatorView()
        actInd.frame = CGRect.init(x: 0, y: 0, width: 40, height: 40)
        actInd.style =
            UIActivityIndicatorView.Style.whiteLarge
        actInd.center = CGPoint.init(x: loadingView.frame.size.width / 2, y: loadingView.frame.size.height / 2)
        loadingView.addSubview(actInd)
        
        let progressbar: UIProgressView = UIProgressView()
        progressbar.frame = CGRect.init(x: 0, y: 0, width: loadingView.frame.size.width - 10, height: 2)
        progressbar.trackTintColor = .clear
        progressbar.progressTintColor = UIColor.init(hexString: "#00BDFF")
        progressbar.center = CGPoint.init(x: loadingView.frame.size.width / 2, y: loadingView.frame.size.height - 1)
        loadingView.addSubview(progressbar)
        
        containerView.addSubview(loadingView)
        view.addSubview(containerView)
        actInd.startAnimating()
        
        return (textDes, progressbar)
    }
    
    func showActivityIndicatoryTitle(title : String)
    {
        let containerView : UIView = UIView()
        containerView.frame = view.frame
        containerView.center = view.center
        containerView.backgroundColor = UIColor.init(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.3)
        containerView.tag = 100
        
        let loadingView: UIView = UIView()
        loadingView.frame = CGRect.init(x: 0, y: 0, width: 150, height: 150)
        loadingView.center = view.center
        loadingView.backgroundColor = UIColor.init(red: 68/255, green: 68/255, blue: 68/255, alpha: 0.7)
        loadingView.clipsToBounds = true
        loadingView.layer.cornerRadius = 10
        
        
        let textDes = UILabel.init(frame: CGRect(x : 0, y : 0, width : 150, height: 50))
        textDes.textColor = UIColor.white
        textDes.font = UIFont(name:"SegoeUI", size: 15.0)
        textDes.textAlignment = .center
        textDes.numberOfLines = 0
        textDes.center = CGPoint.init(x: loadingView.frame.size.width / 2, y: loadingView.frame.size.height / 2 + 45)
        textDes.text = "\(title)..."
        loadingView.addSubview(textDes)
        
        let actInd: UIActivityIndicatorView = UIActivityIndicatorView()
        actInd.frame = CGRect.init(x: 0, y: 0, width: 40, height: 40)
        actInd.style =
            UIActivityIndicatorView.Style.whiteLarge
        actInd.center = CGPoint.init(x: loadingView.frame.size.width / 2, y: loadingView.frame.size.height / 2)
        loadingView.addSubview(actInd)
        containerView.addSubview(loadingView)
        
        view.addSubview(containerView)
        actInd.startAnimating()
    }
    
    func showActivityIndicatoryCountDown(isRV : Bool, pos : Notification.Name,showCompletion completion: (String) -> Void)
    {
        if !isRV
        {
            let storage = UserDefaults.standard
            if storage.string(forKey: defaultsKeys.APP_REMOVE_ADS) != nil
            {
                completion("removeads")
                return
            }
        }
        else
        {
            completion("loadVideo")
        }
        
        countTimerShowAds = 1
        let containerView : UIView = UIView()
        containerView.frame = view.frame
        containerView.center = view.center
        containerView.backgroundColor = UIColor.init(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.3)
        
        let loadingView: UIView = UIView()
        loadingView.frame = CGRect.init(x: 0, y: 0, width: 150, height: 150)
        loadingView.center = view.center
        loadingView.backgroundColor = UIColor.init(red: 68/255, green: 68/255, blue: 68/255, alpha: 0.7)
        loadingView.clipsToBounds = true
        loadingView.layer.cornerRadius = 10
        
        
        let textDes = UILabel.init(frame: CGRect(x : 0, y : 0, width : 150, height: 50))
        textDes.textColor = UIColor.white
        textDes.font = UIFont(name:"SegoeUI", size: 15.0)
        textDes.textAlignment = .center
        textDes.numberOfLines = 0
        textDes.center = CGPoint.init(x: loadingView.frame.size.width / 2, y: loadingView.frame.size.height / 2 + 45)
        textDes.text = "Loading Ads\n\(countTimerShowAds)..."
        loadingView.addSubview(textDes)
        
        let actInd: UIActivityIndicatorView = UIActivityIndicatorView()
        actInd.frame = CGRect.init(x: 0, y: 0, width: 40, height: 40)
        actInd.style =
            UIActivityIndicatorView.Style.whiteLarge
        actInd.center = CGPoint.init(x: loadingView.frame.size.width / 2, y: loadingView.frame.size.height / 2)
        loadingView.addSubview(actInd)
        containerView.addSubview(loadingView)
        
        view.addSubview(containerView)
        let array : [Any] = [textDes, containerView, pos]
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateTimer), userInfo: array, repeats: true)
        
        actInd.startAnimating()
    }
    
    @objc func updateTimer(timer: Timer)
    {
        let array : [Any] = timer.userInfo as! [Any]
        countTimerShowAds -= 1
        let txLabel = array[0] as! UILabel
        txLabel.text = "Loading Ads\n\(countTimerShowAds)..."
        if countTimerShowAds <= 0
        {
            timer.invalidate()
            let containerView = array[1] as! UIView
            containerView.removeFromSuperview()
            let posAdsFull = array[2] as! Notification.Name
            NotificationCenter.default.post(name: posAdsFull, object: nil)
            
        }
    }
    
    func showAlertDialog(title:String? = nil,
                         subtitle:String? = nil,
                         cancelTitle:String? = "Cancel",
                         cancelHandler: ((UIAlertAction) -> Swift.Void)? = nil){
        
        let alert = UIAlertController(title: title, message: subtitle, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: cancelTitle, style: .cancel, handler: cancelHandler))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func showConfirmDialog(title:String? = nil,
                           subtitle:String? = nil,
                           actionTitle:String? = "Add",
                           cancelTitle:String? = "Cancel",
                           cancelHandler: ((UIAlertAction) -> Void)? = nil,
                           actionHandler: ((UIAlertAction) -> Void)? = nil) {
        
        let alert = UIAlertController(title: title, message: subtitle, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: actionTitle, style: .default, handler: actionHandler))
        alert.addAction(UIAlertAction(title: cancelTitle, style: .cancel, handler: cancelHandler))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func showInputDialog(title:String? = nil,
                         subtitle:String? = nil,
                         actionTitle:String? = "Add",
                         cancelTitle:String? = "Cancel",
                         text:String = "",
                         inputPlaceholder:String? = nil,
                         inputKeyboardType:UIKeyboardType = UIKeyboardType.default,
                         cancelHandler: ((UIAlertAction) -> Swift.Void)? = nil,
                         actionHandler: ((_ text: String?) -> Void)? = nil) {
        
        let alert = UIAlertController(title: title, message: subtitle, preferredStyle: .alert)
        
        alert.addTextField { (textField:UITextField) in
            textField.text = text
            textField.placeholder = inputPlaceholder
            textField.keyboardType = inputKeyboardType
        }
        alert.addAction(UIAlertAction(title: actionTitle, style: .destructive, handler: { (action:UIAlertAction) in
            guard let textField =  alert.textFields?.first else {
                actionHandler?(nil)
                return
            }
            actionHandler?(textField.text)
        }))
        alert.addAction(UIAlertAction(title: cancelTitle, style: .cancel, handler: cancelHandler))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}


extension UIView {
    func animShow(){
        UIView.animate(withDuration: 2, delay: 0, options: [.curveEaseIn],
                       animations: {
                        self.center.y -= self.bounds.height
                        self.layoutIfNeeded()
        }, completion: nil)
        self.isHidden = false
    }
    func animHide(){
        UIView.animate(withDuration: 2, delay: 0, options: [.curveLinear],
                       animations: {
                        self.center.y += self.bounds.height
                        self.layoutIfNeeded()
                        
        },  completion: {(_ completed: Bool) -> Void in
            self.isHidden = true
        })
    }
    
    func findViewController() -> UIViewController? {
        if let nextResponder = self.next as? UIViewController {
            return nextResponder
        } else if let nextResponder = self.next as? UIView {
            return nextResponder.findViewController()
        } else {
            return nil
        }
    }
    /**
     Set x Position
     
     :param: x CGFloat
     by DaRk-_-D0G
     */
    func setX(_ x:CGFloat) {
        var frame:CGRect = self.frame
        frame.origin.x = x
        self.frame = frame
    }
    /**
     Set y Position
     
     :param: y CGFloat
     by DaRk-_-D0G
     */
    func setY(_ y:CGFloat) {
        var frame:CGRect = self.frame
        frame.origin.y = y
        self.frame = frame
    }
    /**
     Set Width
     
     :param: width CGFloat
     by DaRk-_-D0G
     */
    func setWidth(_ width:CGFloat) {
        var frame:CGRect = self.frame
        frame.size.width = width
        self.frame = frame
    }
    /**
     Set Height
     
     :param: height CGFloat
     by DaRk-_-D0G
     */
    func setHeight(_ height:CGFloat) {
        var frame:CGRect = self.frame
        frame.size.height = height
        self.frame = frame
    }
}

extension String {
    init?(htmlEncodedString: String) {
        
        guard let data = htmlEncodedString.data(using: .utf8) else {
            return nil
        }
        
        let options: [NSAttributedString.DocumentReadingOptionKey: Any] = [
            NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html,
            NSAttributedString.DocumentReadingOptionKey.characterEncoding: String.Encoding.utf8.rawValue
        ]
        
        guard let attributedString = try? NSAttributedString(data: data, options: options, documentAttributes: nil) else {
            return nil
        }
        
        self.init(attributedString.string)
    }
    
    func encodeUrl() -> String?
    {
        return self.addingPercentEncoding( withAllowedCharacters: NSCharacterSet.urlQueryAllowed)
    }
    func decodeUrl() -> String?
    {
        return self.removingPercentEncoding
    }
    func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        
        return ceil(boundingBox.height)
    }
    
    func width(withConstraintedHeight height: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        
        return ceil(boundingBox.width)
    }
}

func highlightTextDes(_ contentDes: String, _ isBold : Bool, _ size : CGFloat) -> NSAttributedString {
    let attributedString = NSMutableAttributedString(string: contentDes)
    let range = NSMakeRange(0, attributedString.length)
    if(isBold)
    {
        attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont.boldSystemFont(ofSize: size), range: range)
    }
    else
    {
        attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont.systemFont(ofSize: size), range: range)
    }
    return attributedString
}

func highlightTextDes(_ contentDes: String, _ highLightTexts : [String], _ isUnderline : Bool, _ isBold : Bool, _ size : CGFloat) -> NSAttributedString {
    let attributedString = NSMutableAttributedString(string: contentDes)
    var checkUnderline = isUnderline
    for highLightText in highLightTexts {
        let range = (contentDes as NSString).range(of: highLightText)
        if(isBold)
        {
            attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont.boldSystemFont(ofSize: size), range: range)
        }
        else
        {
            attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont.italicSystemFont(ofSize: size), range: range)
        }
        if(checkUnderline)
        {
            attributedString.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: range)
            checkUnderline = false
        }
    }
    return attributedString
}

func createAndLoadBanner(_ bannerView : GADBannerView,_ controller : UIViewController,_ heightConstraintBannerView : NSLayoutConstraint) -> GADBannerView{
    let storage = UserDefaults.standard
    if storage.string(forKey: defaultsKeys.APP_REMOVE_ADS) != nil
    {
        hideBanner(bannerView, heightConstraintBannerView)
    }
    else
    {
        if Constants.BANNER_ID == ""
        {
            heightConstraintBannerView.constant = 0
        }
        else
        {
            bannerView.adUnitID = Constants.BANNER_ID
            bannerView.rootViewController = controller
            bannerView.isAutoloadEnabled = true
            let request = GADRequest()
            bannerView.load(request)
        }
    }
    return bannerView
}
func adView(bannerView: GADBannerView!,
            didFailToReceiveAdWithError error: GADRequestError!) {
    print("adView:didFailToReceiveAdWithError: \(error.localizedDescription)")
}
func hideBanner(_ bannerView : GADBannerView, _ heightConstraintBannerView : NSLayoutConstraint)
{
    heightConstraintBannerView.constant = 0
    bannerView.layoutIfNeeded()
    bannerView.isHidden = true
}

func showBanner(_ bannerView : GADBannerView, _ heightConstraintBannerView : NSLayoutConstraint)
{
    heightConstraintBannerView.constant = 80
    bannerView.layoutIfNeeded()
    bannerView.isHidden = false
}


var fullAds : GADInterstitial!
func createAndLoadInterstitial() -> Void {
    let storage = UserDefaults.standard
    if storage.string(forKey: defaultsKeys.APP_REMOVE_ADS) == nil
    {
        fullAds = GADInterstitial(adUnitID: Constants.FULL_ID)
        let request = GADRequest()
        fullAds.load(request)
    }
}

func showAdsInterstitial(_ controller : UIViewController) -> Bool
{
    let storage = UserDefaults.standard
    if storage.string(forKey: defaultsKeys.APP_REMOVE_ADS) != nil
    {
        return false
    }
    
    if(fullAds == nil)
    {
        createAndLoadInterstitial()
        return false
    }
    
    if !fullAds.isReady
    {
        createAndLoadInterstitial()
        return false
    }
    
    fullAds.present(fromRootViewController: controller)
    return true
}

func getSongPath(songName : String)->URL{
    let documentDirectoryURL =  try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
    return documentDirectoryURL.appendingPathComponent(songName)
}

func savePlayListToJson(_ nameJson : String, _ dataArrayObject : [DataPlayListJson]) throws
{
    // Transform array into data and save it into file
    do {
        //        let data = try JSONSerialization.data(withJSONObject: dataArrayObject, options: [])
        let data =  try JSONEncoder().encode(dataArrayObject)
        try data.write(to: plistURL(nameJson), options: [])
    } catch {
        print("xxx saveJson: \(error)")
    }
}

func savePropertyList(_ nameJson : String, _ dataArrayObject : [DataListJson]) throws
{
    // Transform array into data and save it into file
    do {
//        let data = try JSONSerialization.data(withJSONObject: dataArrayObject, options: [])
        let data =  try JSONEncoder().encode(dataArrayObject)
        try data.write(to: plistURL(nameJson), options: [])
    } catch {
        print("xxx saveJson: \(error)")
    }
}


func loadPropertyList(_ nameJson : String) throws -> [[String : String]]
{
    do {
        let data = try Data(contentsOf: plistURL(nameJson), options: [])
        guard let personArray = try JSONSerialization.jsonObject(with: data, options: []) as? [[String: String]]
        else
        {
            return [[String : String]]()
        }
        return personArray
    } catch {
        print(error)
    }
    
    return [[String : String]]()
}

func plistURL(_ nameJson : String) -> URL {
    let documentDirectoryURL =  try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
    return documentDirectoryURL.appendingPathComponent(nameJson)
}

func saveDataJsonStr(_ nameJson : String, _ plist: [String]) throws
{
    // Transform array into data and save it into file
    do {
        let data = try JSONSerialization.data(withJSONObject: plist, options: [])
        try data.write(to: plistURL(nameJson), options: [])
    } catch {
        print(error)
    }
}

func loadDataJson(_ nameJson : String) throws -> Data
{
    do {
        let data = try Data(contentsOf: URL(fileURLWithPath: plistURL(nameJson).path), options: [])
        return data
    } catch {
        
        print("errorMess :  \(error)")
    }
    return Data()
}

func openUrl(_ urlString:String) {
    let url = URL(string: urlString)!
    if #available(iOS 10.0, *) {
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
    } else {
        UIApplication.shared.openURL(url)
    }
}

func getDocumentsDirectory() -> URL {
    let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
    return paths[0]
}
