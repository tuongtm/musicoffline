//
//  SongsTableViewCell.swift
//  Music1
//
//  Created by NhonGa on 11/05/2019.
//  Copyright © 2019 ThreeWolves. All rights reserved.
//

import UIKit

class SongsTableViewCell: UITableViewCell {
    @IBOutlet weak var tx_title: UILabel!
    @IBOutlet weak var tx_artist: UILabel!
    
    @IBOutlet weak var btn_AddToPlaylist: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
