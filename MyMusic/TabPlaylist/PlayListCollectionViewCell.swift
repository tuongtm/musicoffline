//
//  PlayListCollectionViewCell.swift
//  Music1
//
//  Created by NhonGa on 11/05/2019.
//  Copyright © 2019 ThreeWolves. All rights reserved.
//

import UIKit

class PlayListCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var namePlaylist: UILabel!
    @IBOutlet weak var imIcon: UIImageView!
    @IBOutlet weak var btnAddPlaylist: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        imIcon.layer.borderWidth = 1
        imIcon.layer.masksToBounds = false
        imIcon.layer.borderColor = UIColor.black.cgColor
        imIcon.layer.cornerRadius = imIcon.frame.height/2
        imIcon.clipsToBounds = true
    }
    
}
