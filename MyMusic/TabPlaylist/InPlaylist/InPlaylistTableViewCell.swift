//
//  InPlaylistTableViewCell.swift
//  Music1
//
//  Created by NhonGa on 11/05/2019.
//  Copyright © 2019 ThreeWolves. All rights reserved.
//

import UIKit

class InPlaylistTableViewCell: UITableViewCell {
    @IBOutlet weak var txTitle: UILabel!
    @IBOutlet weak var txArtist: UILabel!
    @IBOutlet weak var btnDel: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
