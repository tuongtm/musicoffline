//
//  InPlaylistViewController.swift
//  Music1
//
//  Created by NhonGa on 11/05/2019.
//  Copyright © 2019 ThreeWolves. All rights reserved.
//

import UIKit
import GoogleMobileAds

class InPlaylistViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    static var shared : InPlaylistViewController? = nil
    @IBOutlet weak var im_icon: UIImageView!
    @IBOutlet weak var lb_title: UILabel!
    @IBOutlet weak var lb_total_songs: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var bannerView: GADBannerView!
    @IBOutlet weak var heightConstraintBannerView: NSLayoutConstraint!
    
    @IBAction func btnCloseAd(_ sender: UIButton) {

    }
    
    var dataPlaylistSelect : DataPlayListJson? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        InPlaylistViewController.shared = self
        dataPlaylistSelect = audioPlayList![indexPlaylistSelected]
        // Do any additional setup after loading the view.
        self.title = "Playlist"
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorColor = UIColor.clear
        
        self.tableView.allowsMultipleSelection = true
        self.tableView.allowsMultipleSelectionDuringEditing = true
        let longpress = UILongPressGestureRecognizer(target: self, action: #selector(longPressGestureRecognized(gestureRecognizer:)))
        self.tableView.addGestureRecognizer(longpress)
    
        im_icon.layer.borderWidth = 1
        im_icon.layer.masksToBounds = false
        im_icon.layer.borderColor = UIColor.black.cgColor
        im_icon.layer.cornerRadius = im_icon.frame.height/2
        im_icon.clipsToBounds = true
        
        reloadDataView()
        bannerView = createAndLoadBanner(bannerView, self, heightConstraintBannerView)
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        reloadDataView()
        tableView.reloadData()
    }
    
    fileprivate func reloadDataView() {
        lb_title.text = dataPlaylistSelect!.name
        lb_total_songs.text = "\(String(describing: dataPlaylistSelect!.listSong.count)) songs"
        im_icon.image = UIImage.init(named: "songbig")
        if dataPlaylistSelect!.listSong.count == 0
        {
            tableView.isHidden = true
        }
        else
        {
            let dataShow = dataPlaylistSelect!.listSong[0]
            let songPath = getSongPath(songName: dataShow.name)
            let playerItem = AVPlayerItem.init(url: songPath)
            let metadataList = playerItem.asset.commonMetadata
            for item in metadataList
            {
                if item.commonKey!.rawValue  == "artwork"
                {
                    if let audioImage = UIImage(data: (item.value as! NSData) as Data)
                    {
                        im_icon.image = audioImage
                    }
                    else
                    {
                        im_icon.image = UIImage(named: "songbig")
                    }
                    
                    break
                }
            }
        }
    }
    
    @objc func longPressGestureRecognized(gestureRecognizer: UIGestureRecognizer) {
        
        let longpress = gestureRecognizer as! UILongPressGestureRecognizer
        let state = longpress.state
        let locationInView = longpress.location(in: self.tableView)
        var indexPath = self.tableView.indexPathForRow(at: locationInView)
        
        switch state {
        case .began:
            if indexPath != nil {
                Path.initialIndexPath = indexPath
                let cell = self.tableView.cellForRow(at: indexPath!) as! InPlaylistTableViewCell
                My.cellSnapShot = snapshopOfCell(inputView: cell)
                var center = cell.center
                My.cellSnapShot?.center = center
                My.cellSnapShot?.alpha = 0.0
                self.tableView.addSubview(My.cellSnapShot!)
                
                UIView.animate(withDuration: 0.25, animations: {
                    center.y = locationInView.y
                    My.cellSnapShot?.center = center
                    My.cellSnapShot?.transform = CGAffineTransform(scaleX: 1.05, y: 1.05)
                    My.cellSnapShot?.alpha = 0.98
                    cell.alpha = 0.0
                }, completion: { (finished) -> Void in
                    if finished {
                        cell.isHidden = true
                    }
                })
            }
            
        case .changed:
            var center = My.cellSnapShot?.center
            center?.y = locationInView.y
            My.cellSnapShot?.center = center!
            if ((indexPath != nil) && (indexPath != Path.initialIndexPath)) {
                
                do
                {
                    audioPlayList![indexPlaylistSelected].listSong.swapAt((indexPath?.row)!, (Path.initialIndexPath?.row)!)
                    try savePlayListToJson(NAME_PLAYLIST_JSON, audioPlayList!)
                    self.dataPlaylistSelect = audioPlayList![indexPlaylistSelected]
                }
                catch
                {
                    
                }
                //swap(&self.wayPoints[(indexPath?.row)!], &self.wayPoints[(Path.initialIndexPath?.row)!])
                self.tableView.moveRow(at: Path.initialIndexPath!, to: indexPath!)
                DispatchQueue.main.async {
                    self.reloadDataView()
                }
                Path.initialIndexPath = indexPath
            }
            
        default:
            let cell = self.tableView.cellForRow(at: Path.initialIndexPath!) as! InPlaylistTableViewCell
            cell.isHidden = false
            cell.alpha = 0.0
            UIView.animate(withDuration: 0.25, animations: {
                My.cellSnapShot?.center = cell.center
                My.cellSnapShot?.transform = .identity
                My.cellSnapShot?.alpha = 0.0
                cell.alpha = 1.0
            }, completion: { (finished) -> Void in
                if finished {
                    Path.initialIndexPath = nil
                    My.cellSnapShot?.removeFromSuperview()
                    My.cellSnapShot = nil
                }
            })
        }
    }
    
    func snapshopOfCell(inputView: UIView) -> UIView {
        
        UIGraphicsBeginImageContextWithOptions(inputView.bounds.size, false, 0.0)
        inputView.layer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        let cellSnapshot : UIView = UIImageView(image: image)
        cellSnapshot.layer.masksToBounds = false
        cellSnapshot.layer.cornerRadius = 0.0
        cellSnapshot.layer.shadowOffset = CGSize(width: -5.0, height: 0.0)
        cellSnapshot.layer.shadowRadius = 5.0
        cellSnapshot.layer.shadowOpacity = 0.4
        return cellSnapshot
    }
    
    struct My {
        static var cellSnapShot: UIView? = nil
    }
    
    struct Path {
        static var initialIndexPath: IndexPath? = nil
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return dataPlaylistSelect!.listSong.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let row = indexPath.row
        let cell = tableView.dequeueReusableCell(withIdentifier: "InPlaylistTableCell", for: indexPath) as! InPlaylistTableViewCell
        let dataShow = dataPlaylistSelect!.listSong[row]
        cell.txTitle.text = dataShow.name
        cell.txArtist.text = "Unknow"
        let songPath = getSongPath(songName: dataShow.name)
        let playerItem = AVPlayerItem.init(url: songPath)
        let metadataList = playerItem.asset.commonMetadata
        for item in metadataList
        {
            if item.commonKey!.rawValue  == "artist"
            {
                cell.txArtist.text = item.value as? String
                break
            }
        }
        cell.btnDel.tag = row
        cell.btnDel.addTarget(self, action: #selector(deleteSongsInPlaylist), for: .touchUpInside)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let row = indexPath.row
        showPlaying(row)
    }
    
    @objc func deleteSongsInPlaylist(_ sender: UIButton)
    {
        showConfirmDialog(title: "Are you sure?", subtitle: "", actionTitle: "Ok", cancelTitle: "Cancel", cancelHandler: nil) { (str) in
            do
            {
                if audioPlayList![indexPlaylistSelected].listSong.count == 0
                {
                    return
                }
                
                audioPlayList![indexPlaylistSelected].listSong.remove(at: sender.tag)
                try savePlayListToJson(NAME_PLAYLIST_JSON, audioPlayList!)
                DispatchQueue.main.async
                    {
                        self.dataPlaylistSelect = audioPlayList![indexPlaylistSelected]
                        self.reloadDataView()
                        self.tableView.reloadData()
                }
            }
            catch
            {
                print("xxx \(error)")
            }
        }
    }
    
    @IBAction func deletePlaylist(_ sender: Any)
    {
        do
        {
            audioPlayList!.remove(at: indexPlaylistSelected)
            try savePlayListToJson(NAME_PLAYLIST_JSON, audioPlayList!)
            DispatchQueue.main.async
            {
                self.navigationController?.popViewController(animated: true)
            }
        }
        catch
        {
            
        }
    }
    @IBAction func editPlaylist(_ sender: Any)
    {
        showInputDialog(title: "Playlist Name",
                        subtitle: "Please enter the new playlist's name below.",
                        actionTitle: "Save",
                        cancelTitle: "Cancel",
                        text: dataPlaylistSelect!.name,
                        inputPlaceholder: "",
                        inputKeyboardType: .default)
        {
            (input:String?) in
            if let namePlaylist = input
            {
                if namePlaylist != ""
                {
                    do{
                        audioPlayList![indexPlaylistSelected].name = namePlaylist
                        try savePlayListToJson(NAME_PLAYLIST_JSON, audioPlayList!)
                        DispatchQueue.main.async
                        {
                            self.dataPlaylistSelect = audioPlayList![indexPlaylistSelected]
                            self.lb_title.text = self.dataPlaylistSelect!.name
                        }
                    }
                    catch{
                        
                    }
                }
            }
        }
    }
    
    @IBAction func playSongsInPlayList(_ sender: Any)
    {
        showPlaying(0)
    }
    
    @objc func showPlaying(_ currentSongs : Int)
    {
        if dataPlaylistSelect?.listSong.count == 0
        {
            showAlertDialog(title: "No Song to play",subtitle: "Please add song in order to play!",cancelTitle: "Ok")
            return
        }
        isPlayingFromPlaylist = true
        isClickFromPlayList = true
        currentTableAudioIndex = currentSongs
        self.tabBarController?.selectedIndex = 1
    }
}
