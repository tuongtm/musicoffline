//
//  AddSongToPlaylistTableViewCell.swift
//  Music1
//
//  Created by NhonGa on 12/05/2019.
//  Copyright © 2019 ThreeWolves. All rights reserved.
//

import UIKit

class AddSongToPlaylistTableViewCell: UITableViewCell {
    @IBOutlet weak var imIcon: UIImageView!
    @IBOutlet weak var txTitle: UILabel!
    @IBOutlet weak var txArtist: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        imIcon.layer.borderWidth = 1
        imIcon.layer.masksToBounds = false
        imIcon.layer.borderColor = UIColor.black.cgColor
        imIcon.layer.cornerRadius = imIcon.frame.height/2
        imIcon.clipsToBounds = true
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.selectionStyle = .default
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.accessoryType = selected ? .checkmark : .none
        // Configure the view for the selected state
    }

}
