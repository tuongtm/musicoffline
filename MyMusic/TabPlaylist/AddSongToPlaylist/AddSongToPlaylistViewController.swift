//
//  AddSongToPlaylistViewController.swift
//  Music1
//
//  Created by NhonGa on 12/05/2019.
//  Copyright © 2019 ThreeWolves. All rights reserved.
//

import UIKit
import GoogleMobileAds

class AddSongToPlaylistViewController: UIViewController, UITableViewDelegate, UITableViewDataSource
{
    static var shared : AddSongToPlaylistViewController? = nil
    @IBOutlet weak var tableview: UITableView!
    var songPathSelected : String = ""
    var listDataAddToList : [Int] = []
    @IBOutlet weak var bannerView: GADBannerView!
    @IBOutlet weak var heightConstraintBannerView: NSLayoutConstraint!
    
    @IBOutlet weak var tableviewBottomAnchor: NSLayoutConstraint!
    @IBAction func btnCloseAd(_ sender: UIButton) {

    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        AddSongToPlaylistViewController.shared = self
        self.tableview.delegate = self
        self.tableview.dataSource = self
        self.tableview.separatorColor = UIColor.clear
        self.tableview.allowsMultipleSelection = true
        self.tableview.allowsMultipleSelectionDuringEditing = true
        
        songPathSelected = audioList![indexSongsSelected].path
        
        navigationItem.rightBarButtonItem = UIBarButtonItem.init(barButtonSystemItem: .cancel, target: self, action: #selector(dismissSetting))
        navigationItem.rightBarButtonItem?.tintColor = UIColor.white
        
        bannerView = createAndLoadBanner(bannerView, self, heightConstraintBannerView)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let storage = UserDefaults.standard
        if storage.string(forKey: defaultsKeys.APP_REMOVE_ADS) != nil
        {
            tableviewBottomAnchor.constant = 0
            tableview.layoutIfNeeded()
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return audioPlayList!.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AddSongToPlaylistTableViewCell", for: indexPath) as! AddSongToPlaylistTableViewCell
        let row = indexPath.row
        let dataShow = audioPlayList![row]
        
        cell.txTitle.text = dataShow.name
        cell.txArtist.text = "\(dataShow.listSong.count) songs"
        cell.imIcon.image = UIImage.init(named: "songbig")
        
        let songPath = getSongPath(songName: dataShow.name)
        let playerItem = AVPlayerItem.init(url: songPath)
        let metadataList = playerItem.asset.commonMetadata
        for item in metadataList
        {
            if item.commonKey!.rawValue  == "artwork"
            {
                if let audioImage = UIImage(data: (item.value as! NSData) as Data)
                {
                     cell.imIcon.image = audioImage
                }
                else
                {
                     cell.imIcon.image = UIImage(named: "songbig")
                }
                break
            }
        }
        let haveSongInPL : Bool = dataShow.listSong.contains { (dataCheck) -> Bool in
            return dataCheck.path == songPathSelected
        }
        
        if haveSongInPL
        {
            cell.isUserInteractionEnabled = false
            cell.accessoryType = .checkmark
        }
        else
        {
            cell.accessoryType = .none
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        listDataAddToList.append(indexPath.row)
        
        if listDataAddToList.count == 1
        {
            navigationItem.rightBarButtonItem = UIBarButtonItem.init(barButtonSystemItem: .done, target: self, action: #selector(dismissSetting))
            navigationItem.rightBarButtonItem?.tintColor = UIColor.red
        }
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath)
    {
        listDataAddToList.removeAll { (number) -> Bool in
            return number == indexPath.row
        }
        
        if listDataAddToList.count == 0
        {
            navigationItem.rightBarButtonItem = UIBarButtonItem.init(barButtonSystemItem: .cancel, target: self, action: #selector(dismissSetting))
            navigationItem.rightBarButtonItem?.tintColor = UIColor.white
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 100
    }
    
    @IBAction func addPlaylists(_ sender: Any)
    {
        showInputPlaylist(sender)
    }

    @objc func dismissSetting()
    {
        if listDataAddToList.count > 0
        {
            listDataAddToList.forEach { (number) in
                do
                {
                    audioPlayList![number].listSong.append(audioList![indexSongsSelected])
                    try savePlayListToJson(NAME_PLAYLIST_JSON, audioPlayList!)
                }
                catch
                {
                    
                }
            }
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func showInputPlaylist(_ sender: Any) {
        showInputDialog(title: "Playlist Name",
                        subtitle: "Please enter the new playlist's name below.",
                        actionTitle: "Add",
                        cancelTitle: "Cancel",
                        inputPlaceholder: "New playlist's name",
                        inputKeyboardType: .default)
        {
            (input:String?) in
            if let namePlaylist = input
            {
                if namePlaylist != ""
                {
                    do{
                        audioPlayList?.append(DataPlayListJson.init(namePlaylist, []))
                        try savePlayListToJson(NAME_PLAYLIST_JSON, audioPlayList!)
                        DispatchQueue.main.async
                        {
                                self.tableview.reloadData()
                        }
                    }
                    catch{
                        
                    }
                }
            }
        }
    }
}
