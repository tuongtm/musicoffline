//
//  PlaylistViewController.swift
//  Tubidy
//
//  Created by NhonGa on 19/01/2019.
//  Copyright © 2019 ThreeWolves. All rights reserved.
//

import UIKit
import GoogleMobileAds

var dataAlbumArray : [String]? = nil

class PlaylistViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    static var shared : PlaylistViewController? = nil
    @IBOutlet weak var tablview: UITableView!
    @IBOutlet weak var bannerView: GADBannerView!
    @IBOutlet weak var heightConstraintBannerView: NSLayoutConstraint!
    
    @IBAction func btnCloseAd(_ sender: UIButton) {

    }
    
    struct headerSection
    {
        let icon_image : String
        let title : String
        let button1_image : String
        let button2_image : String
    }
    
    fileprivate let headerSectionTableView =
        [headerSection(icon_image: "icon_health", title: "Playlists", button1_image: "icon_menu_song", button2_image: "library_btnadd"),
         headerSection(icon_image: "icon_music", title: "Songs", button1_image: "", button2_image: "")]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        PlaylistViewController.shared = self
        tablview.delegate = self
        tablview.dataSource = self
//        self.title = "Library"
        
        bannerView = createAndLoadBanner(bannerView, self, heightConstraintBannerView)
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        UserDefaults.standard.set(0, forKey: defaultsKeys.APP_MYTUNES)
        if let tabItems = tabBarController?.tabBar.items {
            // In this case we want to modify the badge number of the third tab:
            let tabItem = tabItems[2]
            tabItem.badgeValue = nil
        }
        
        do
        {
            let fileStep = try loadDataJson(NAME_PLAYLIST_JSON) as Data
            let vendorArray = try JSONDecoder().decode([DataPlayListJson].self, from: fileStep) as [DataPlayListJson]
            audioPlayList = vendorArray
            
            let fileStep1 = try loadDataJson(NAME_LIST_JSON) as Data
            let vendorArray1 = try JSONDecoder().decode([DataListJson].self, from: fileStep1) as [DataListJson]
            audioList = vendorArray1
        }
        catch
        {
            
        }
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        tablview.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch (section) {
        case 0:
            return 1
        case 1:
            return audioList!.count
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let row = indexPath.row
        let section = indexPath.section
        if section == 0
        {
            let cell = Bundle.main.loadNibNamed("PlaylistTableViewCell", owner: self, options: nil)?.first as! PlaylistTableViewCell
            cell.listPlaylistJson = audioPlayList!
            return cell
        }
        else
        {
            let cell = Bundle.main.loadNibNamed("SongsTableViewCell", owner: self, options: nil)?.first as! SongsTableViewCell
            
            guard let listSongs = audioList
                else
            {
                return cell
            }
            
            if listSongs.count == 0
            {
                return cell
            }
            
            let data : DataListJson = listSongs[row]
            cell.tx_title.text = data.name
            cell.tx_artist.text = "Unknow"
            
            let songPath = getSongPath(songName: data.name)
            let playerItem = AVPlayerItem.init(url: songPath)
            let metadataList = playerItem.asset.commonMetadata
            for item in metadataList
            {
                if item.commonKey!.rawValue  == "artist" {
                    cell.tx_artist.text = item.value as? String
                }
            }
            cell.btn_AddToPlaylist.tag = row
            cell.btn_AddToPlaylist.addTarget(self, action: #selector(showPlaylist), for: .touchUpInside)
            return cell
        }
    }
    
    @objc func showPlaylist(_ sender : UIButton)
    {
        indexSongsSelected = sender.tag
        performSegue(withIdentifier: "playlist", sender: self)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        let section = indexPath.section
        return section == 0 ? 100 : 70;
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectRow = indexPath.row
        let section = indexPath.section
        if section == 1
        {
            //click nhac tren table list
            isPlayingFromPlaylist = false
            isClickFromPlayList = true
            currentTableAudioIndex = selectRow
            self.tabBarController?.selectedIndex = 1
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return headerSectionTableView.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let vw = UIView.init(frame: CGRect.init(x: 0, y: 0, width: view.frame.width, height: 30))
        vw.backgroundColor = UIColor(hexString: "#F9E2D6")
        
        let imageIcon = UIImageView.init(frame: CGRect(x : 10, y : 5, width : 20, height: 20))
        imageIcon.image = UIImage.init(named: headerSectionTableView[section].icon_image)
        vw.addSubview(imageIcon)
        
        let textTitle = UILabel.init(frame: CGRect(x : imageIcon.frame.maxX + 10, y : 0, width : 100, height: vw.frame.height))
        textTitle.textColor = UIColor(hexString: "#F8AD37")
        textTitle.font = UIFont(name:"SegoeUI-Bold", size: 17)
        textTitle.text = headerSectionTableView[section].title
        vw.addSubview(textTitle)
        
        //        let btn1 = UIButton.init(frame: CGRect(x : vw.frame.width - 50, y : 15, width : 20, height: 20))
        //        btn1.setImage(UIImage.init(named: headerSectionTableView[section].button1_image), for: .normal)
        //        vw.addSubview(btn1)
        
        let btn2 = UIButton.init(frame: CGRect(x : vw.frame.width - 40, y : 0, width : 30, height: 30))
        if headerSectionTableView[section].button2_image != ""
        {
            btn2.setImage(UIImage.init(named: headerSectionTableView[section].button2_image), for: .normal)
            vw.addSubview(btn2)
        }
        
        
        if section == 0
        {
            //            btn1.addTarget(self, action: #selector(showListPlayList), for: .touchUpInside)
            btn2.addTarget(self, action: #selector(showInputPlaylist), for: .touchUpInside)
        }
        else
        {
            
        }
        
        return vw
    }
    
    @objc func showInputPlaylist(_ sender: Any) {
        showInputDialog(title: "Playlist Name",
                        subtitle: "Please enter the new playlist's name below.",
                        actionTitle: "Add",
                        cancelTitle: "Cancel",
                        inputPlaceholder: "New playlist's name",
                        inputKeyboardType: .default)
        {
            (input:String?) in
            if let namePlaylist = input
            {
                if namePlaylist != ""
                {
                    do{
                        audioPlayList?.append(DataPlayListJson.init(namePlaylist, []))
                        try savePlayListToJson(NAME_PLAYLIST_JSON, audioPlayList!)
                        DispatchQueue.main.async
                            {
                                self.tablview.reloadData()
                        }
                    }
                    catch{
                        
                    }
                }
            }
        }
    }
    
    fileprivate func whitespaceString(font: UIFont = UIFont.systemFont(ofSize: 15), width: CGFloat) -> String {
        let kPadding: CGFloat = 20
        let mutable = NSMutableString(string: "")
        let attribute = [NSAttributedString.Key.font: font]
        while mutable.size(withAttributes: attribute).width < width - (2 * kPadding) {
            mutable.append(" ")
        }
        return mutable as String
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        let row = indexPath.row
        if editingStyle == .delete {
            let refreshAlert = UIAlertController(title: "", message: "Are you sure?", preferredStyle: UIAlertController.Style.alert)
            
            refreshAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
                do{
                    let fileStep = try loadDataJson(NAME_LIST_JSON) as Data
                    var vendorArray = try JSONDecoder().decode([DataListJson].self, from: fileStep) as [DataListJson]
                    
                    var i = 0
                    for dataInPlayList in audioPlayList! {
                        var dataListSongInPlayList = dataInPlayList.listSong
                        dataListSongInPlayList.removeAll(where: { (dataPlayListSearching) -> Bool in
                            return dataPlayListSearching.name == vendorArray[row].name
                        })
                        audioPlayList![i].listSong = dataListSongInPlayList
                        i+=1
                    }
                    
                    try savePlayListToJson(NAME_PLAYLIST_JSON, audioPlayList!)
                    
                    try FileManager.default.removeItem(at: getSongPath(songName: vendorArray[row].name))
                    vendorArray.remove(at: row)
                    try savePropertyList(NAME_LIST_JSON, vendorArray)
                    audioList = vendorArray
                    //                    self.setCurrentAudio()
                    self.tablview.reloadData()
                }catch{
                    
                }
            }))
            
            refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action: UIAlertAction!) in
                print("Handle Cancel Logic here")
            }))
            
            self.present(refreshAlert, animated: true, completion: nil)
        }
    }
    
    @objc func showListPlayList()
    {
        
    }
}


extension UIView
{
    func image() -> UIImage {
        UIGraphicsBeginImageContextWithOptions(bounds.size, isOpaque, 0)
        guard let context = UIGraphicsGetCurrentContext() else {
            return UIImage()
        }
        layer.render(in: context)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }
}
