//
//  PlaylistTableViewCell.swift
//  Tubidy
//
//  Created by NhonGa on 19/01/2019.
//  Copyright © 2019 ThreeWolves. All rights reserved.
//

import UIKit

class PlaylistTableViewCell: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource {
    @IBOutlet weak var collectionView: UICollectionView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(UINib.init(nibName: "PlayListCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "PlayListCollectionCell")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    var listPlaylistJson:[DataPlayListJson] = [] {
        didSet {
            collectionView.reloadData()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if listPlaylistJson.count == 0
        {
            return 1
        }
        return listPlaylistJson.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let row = indexPath.row
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PlayListCollectionCell", for: indexPath) as! PlayListCollectionViewCell
        if listPlaylistJson.count == 0
        {
            cell.btnAddPlaylist.isHidden = false
            cell.imIcon.isHidden = true
            cell.namePlaylist.isHidden = true
            
            cell.btnAddPlaylist.addTarget(self, action: #selector(addPlaylist), for: .touchUpInside)
        }
        else
        {
            let dataPlaylist = listPlaylistJson[row]
            cell.btnAddPlaylist.isHidden = true
            cell.imIcon.isHidden = false
            cell.namePlaylist.isHidden = false
            cell.namePlaylist.text = dataPlaylist.name
            
            if dataPlaylist.listSong.count == 0
            {
                cell.imIcon.image = UIImage(named: "songbig")
            }
            else
            {
                let songPath = getSongPath(songName: dataPlaylist.listSong[0].name)
                let playerItem = AVPlayerItem.init(url: songPath)
                let metadataList = playerItem.asset.commonMetadata
                cell.imIcon.image = UIImage(named: "songbig")
                for item in metadataList {
                    if item.commonKey!.rawValue  == "artwork" {
                        if let audioImage = UIImage(data: item.value as! Data)
                        {
                            cell.imIcon.image = audioImage
                            break
                        }
                    }
                }
            }
        }
        
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        if listPlaylistJson.count != 0
        {
            indexPlaylistSelected = indexPath.row
            PlaylistViewController.shared!.performSegue(withIdentifier: "segue", sender: self)
        }
    }
    
    @objc func addPlaylist(_ sender: Any) {
        PlaylistViewController.shared!.showInputDialog(title: "Playlist Name",
                                                       subtitle: "Please enter the new playlist's name below.",
                                                       actionTitle: "Add",
                                                       cancelTitle: "Cancel",
                                                       inputPlaceholder: "New playlist's name",
                                                       inputKeyboardType: .default)
        {
            (input:String?) in
            if let namePlaylist = input
            {
                if namePlaylist != ""
                {
                    do{
                        audioPlayList?.append(DataPlayListJson.init(namePlaylist, []))
                        try savePlayListToJson(NAME_PLAYLIST_JSON, audioPlayList!)
                        DispatchQueue.main.async
                        {
                            PlaylistViewController.shared!.tablview.reloadData()
                        }
                    }
                    catch{
                        
                    }
                }
            }
        }

    }
    
    //Action
    @objc func tapDetected(sender: UIGestureRecognizer) {
        PlaylistViewController.shared!.performSegue(withIdentifier: "segue", sender: self)
    }
}
